//
//  MySessionCell.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 27/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class MySessionCell: UITableViewCell {
    
    @IBOutlet weak var prntView: UIView!
    @IBOutlet weak var programName: UILabel!
    @IBOutlet weak var workingDays: UILabel!
    @IBOutlet weak var startEndFrom: UILabel!
    @IBOutlet weak var tutionType: UILabel!
    @IBOutlet weak var bookings: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setData(data: HomeMonthModl.Payload) {
        self.programName.text = data.programName
        self.bookings.text = "\(data.noOfStudents ?? 0) Students"
        
        if let getDate = data.startFrom, let getTime = data.endTo {
            if let cDate = self.stringToDate(dateStr: getDate, withFormat: "HH:mm:ss"), let cTime = self.stringToDate(dateStr: getTime, withFormat: "HH:mm:ss") {
                if let strDate = self.convDate(date: cDate, format: "HH:mm"), let strTime = self.convDate(date: cTime, format: "HH:mm")  {
                    self.startEndFrom.text = "\(strDate) - \(strTime)"
                }
            }
        } else {
            self.startEndFrom.text = ""
        }
        
        //self.startEndFrom.text = "\(data.startFrom ?? "") - \(data.endTo ?? "")"
        if let getDate = data.startingFrom {
            if let cDate = self.stringToDate(dateStr: getDate) {
               self.workingDays.text = self.convDate(date: cDate, format: "MMM dd yyyy")
            }
        } else {
            self.workingDays.text = ""
        }
        //self.workingDays.text = data.workingDays
        self.tutionType.text = data.tuitionType
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
