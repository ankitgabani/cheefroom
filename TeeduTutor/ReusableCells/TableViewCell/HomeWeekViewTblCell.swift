//
//  HomeWeekViewTblCell.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 26/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class HomeWeekViewTblCell: UITableViewCell {
    @IBOutlet weak var imgChange: UIImageView!
    
    @IBOutlet weak var prntView: UIView!
    @IBOutlet weak var programName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var totalStudents: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgChange.image = imgChange.image?.withRenderingMode(.alwaysTemplate)
        imgChange.tintColor = UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1)

        // Initialization code
    }
    
    func setData(data: HomeWeekModl.Payload) {
        self.programName.text = "\(data.programName ?? "") \(data.payloadClass ?? "")"//data.programName
        if let getDate = data.date, let getTime = data.startFrom {
            if let cDate = self.stringToDate(dateStr: getDate), let cTime = self.stringToDate(dateStr: getTime, withFormat: "HH:mm:ss") {
                if let strDate = self.convDate(date: cDate, format: "dd MMM"), let strTime = self.convDate(date: cTime, format: "HH:mm")  {
                    self.date.text = "\(strDate)\n\(strTime)"
                }
            }
        } else {
            self.date.text = ""
        }
        //self.date.text = self.convDate(date: T##Date)//"\(data.date ?? "")\n\(data.startFrom ?? "")"
        if let noOfStudents = data.noOfStudents {
            self.totalStudents.text = "\(noOfStudents) Students"
        }
        if let imageName = data.image {
            RestApi.shared.getImage(url: BASE_URL+imageName) { (image) in
                self.img.image = image ?? #imageLiteral(resourceName: "Home-Section")
            }
        } else {
            self.img.image = #imageLiteral(resourceName: "Home-Section")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
