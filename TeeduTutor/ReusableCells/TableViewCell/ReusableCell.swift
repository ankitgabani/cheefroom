//
//  ReusableCell.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class ReusableCell: UITableViewCell {
    
    @IBOutlet weak var prntView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
