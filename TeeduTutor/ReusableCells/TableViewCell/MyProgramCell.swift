//
//  MyProgramCell.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 26/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SDWebImage

class MyProgramCell: UITableViewCell {

    @IBOutlet weak var prntView: UIView!
    @IBOutlet weak var programName: UILabel!
    @IBOutlet weak var workingDays: UILabel!
    @IBOutlet weak var startEndTime: UILabel!
    @IBOutlet weak var img: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(data: ProgramListModl.Payload) {
        self.programName.text = "\(data.programName ?? "") \(data.payloadClass ?? "")"
        if let imageName = data.image {
      
            var strImage = BASE_URL+imageName
            strImage = strImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: strImage)
            
            self.img.sd_setImage(with: url, for: .normal, placeholderImage: UIImage(named: "avatar"), completed: nil)
//            RestApi.shared.getImage(url: BASE_URL+imageName) { (image) in
//                self.img.setImage(image, for: .normal)
//            }
        } else {
            self.img.setImage(#imageLiteral(resourceName: "avatar"), for: .normal)
        }
        
        if let getStart = data.startFrom, let getEnd = data.endTo {
            if let cDate = self.stringToDate(dateStr: getStart, withFormat: "HH:mm:ss"), let cTime = self.stringToDate(dateStr: getEnd, withFormat: "HH:mm:ss") {
                if let strDate = self.convDate(date: cDate, format: "HH:mm"), let strTime = self.convDate(date: cTime, format: "HH:mm")  {
                    self.startEndTime.text = "\(strDate) - \(strTime)"
                }
            }
        } else {
            self.startEndTime.text = ""
        }
        
        self.workingDays.text = data.workingDays!
        //self.startEndTime.text = "\(data.startFrom ?? "") - \(data.endTo ?? "")"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
