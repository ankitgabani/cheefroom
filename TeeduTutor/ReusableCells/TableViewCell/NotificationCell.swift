//
//  NotificationCell.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 30/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var showDate: UILabel!
    @IBOutlet weak var parentView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: NotificationModl.Payload) {
        self.name.text = data.message
        
        if data.isRead == "0" {
            self.parentView.backgroundColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1.0)
        } else {
            self.parentView.backgroundColor = .clear
        }
        
        if let imageName = data.profileImage {
            RestApi.shared.getImage(url: BASE_URL+imageName) { (image) in
                self.img.image = image ?? #imageLiteral(resourceName: "avatar")
            }
        } else {
            self.img.image = #imageLiteral(resourceName: "avatar")
        }
        self.showDate.text = data.createdAt
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
