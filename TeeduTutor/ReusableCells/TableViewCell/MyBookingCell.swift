//
//  MyBookingCell.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 27/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol BookingActions {
    func getBooking(id: Int?, cancel: Bool?)
}

class MyBookingCell: UITableViewCell {
    
    @IBOutlet weak var prntView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var programClassName: UILabel!
    @IBOutlet weak var workingDaysStartEndTime: UILabel!
    @IBOutlet weak var noOfSession: UILabel!
    @IBOutlet weak var startSessionDate: UILabel!
    @IBOutlet weak var endSessionDate: UILabel!
    
    var bookingID: Int?
    
    var delegate: BookingActions?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: BookingslistModl.Payload) {
        self.name.text = data.fullName
        if let imageName = data.profileImage {
            RestApi.shared.getImage(url: BASE_URL+imageName) { (image) in
                self.img.image = image ?? #imageLiteral(resourceName: "iStock-19")
            }
        } else {
            self.img.image = #imageLiteral(resourceName: "avatar")
        }
        self.noOfSession.text = "\(data.noOfSession ?? 0) Sessions"
        self.programClassName.text = "\(data.programName ?? "") - \(data.payloadClass ?? "")"
        
        if let getDate = data.startFrom, let getTime = data.endTo {
            if let cDate = self.stringToDate(dateStr: getDate, withFormat: "HH:mm:ss"), let cTime = self.stringToDate(dateStr: getTime, withFormat: "HH:mm:ss") {
                if let strDate = self.convDate(date: cDate, format: "HH:mm"), let strTime = self.convDate(date: cTime, format: "HH:mm")  {
                    self.workingDaysStartEndTime.text = "\(data.workingDays ?? "") \(strDate) - \(strTime)"
                }
            }
        } else {
            self.workingDaysStartEndTime.text = ""
        }
        
        if let getDate = data.startSession {
            if let cDate = self.stringToDate(dateStr: getDate) {
                self.startSessionDate.text = "Starting From \(self.convDate(date: cDate, format: "MMM dd yyyy") ?? "")"
            }
        } else {
            self.startSessionDate.text = ""
        }
        
        if let getEDate = data.endSession {
            if let cEDate = self.stringToDate(dateStr: getEDate) {
                self.endSessionDate.text = "Ending on \(self.convDate(date: cEDate, format: "MMM dd yyyy") ?? "")"
            }
        } else {
            self.endSessionDate.text = ""
        }
        //self.startSessionDate.text = "Starting From \(data.startSession ?? "")"
        //self.endSessionDate.text = "Ending on \(data.endSession ?? "")"
        self.bookingID = data.bookingID
    }
    
    
    @IBAction func successBtnClicked(_ sender: UIButton) {
        self.delegate?.getBooking(id: self.bookingID, cancel: false)
    }
    
    @IBAction func declineBtnClicked(_ sender: UIButton) {
        self.delegate?.getBooking(id: self.bookingID, cancel: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
