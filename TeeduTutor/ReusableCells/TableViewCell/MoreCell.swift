//
//  MoreCell.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 27/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class MoreCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var reviewCount: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var notifyView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
