//
//  StudentListCell.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 26/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class StudentListCell: UITableViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    var number: String?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: StudentListModl.Payload) {
        self.userName.text = data.fullName
        if let img = data.profileImage {
            RestApi.shared.getImage(url: img) { (image) in
                if let image = image {
                    self.img.image = image
                } else {
                    self.img.image = #imageLiteral(resourceName: "iStock-5")
                }
            }
        }
        if let mobile = data.mobile {
            number = mobile
        }
    }
    
    @IBAction func callUser(_ sender: UIButton) {
        if let number = number {
            dialNumber(number: number)
        }
        
    }
    
    func dialNumber(number : String) {

     if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
          if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
           } else {
               UIApplication.shared.openURL(url)
           }
       } else {
                // add error message here
       }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
