//
//  HomeTodaySectionCell.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 26/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class HomeTodaySectionCell: UICollectionViewCell {
    @IBOutlet weak var prntView: UIView!
    @IBOutlet weak var programName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var numberOfStudents: UILabel!
    @IBOutlet weak var className: UILabel!
    
    func setData(data: HomeTodayModl.Payload) {
        self.programName.text = data.programName
        if let getDate = data.date, let getTime = data.startFrom {
            if let cDate = self.stringToDate(dateStr: getDate), let cTime = self.stringToDate(dateStr: getTime, withFormat: "HH:mm:ss") {
                if let strDate = self.convDate(date: cDate, format: "dd MMM"), let strTime = self.convDate(date: cTime, format: "HH:mm")  {
                    self.date.text = "\(strDate) | \(strTime)"
                }
            }
        } else {
            self.date.text = ""
        }
        
        self.numberOfStudents.text = "\(data.NoOfStudents ?? 0) Students"
        self.className.text = data.payloadClass
        if let imageName = data.image {
            RestApi.shared.getImage(url: BASE_URL+imageName) { (image) in
                self.img.image = image ?? #imageLiteral(resourceName: "Home-Section")
            }
        } else {
            self.img.image = #imageLiteral(resourceName: "Home-Section")
        } //BASE_URL+data.image
    }
}
