//
//  CuisineCell.swift
//  TeeduTutor
//
//  Created by Navjot Singh on 22/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class CuisineCell: UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var image: UIImageView!
    
}
