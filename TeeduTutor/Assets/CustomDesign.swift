//
//  CustomDesign.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class CustomDesign: NSObject {

}


extension UIButton {
    func addTextSpacing(spacing: CGFloat){
       let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text!)!)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSRange(location: 0, length: (self.titleLabel?.text!.count)!))
       self.setAttributedTitle(attributedString, for: .normal)
   }
}

@IBDesignable class setCharacterSpaceLbl: UILabel {

    @IBInspectable public var spacing: CGFloat = 0.0 {
        didSet {
            applyKerning()
        }
    }

    override var text: String? {
        didSet {
            applyKerning()
        }
    }

    private func applyKerning() {
        let stringValue = self.text ?? ""
        let attrString = NSMutableAttributedString(string: stringValue)
        attrString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
}

@IBDesignable class setGradientButton: UIButton {
    
    @IBInspectable var firstColr: UIColor = ProjectColor.lightGreen
    @IBInspectable var secColr: UIColor = ProjectColor.darkGreen
    @IBInspectable var titleColr: UIColor = UIColor.white
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        //self.setTitleColor(titleColr, for: .normal)
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.colors = [firstColr.cgColor, secColr.cgColor]
        self.layer.insertSublayer(gradient, at: 0)
        path.fill()
        path.close()
    }
}
