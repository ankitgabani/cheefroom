//
//  Constant.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

func BASE_URL(child: String) -> String {
    return "http://api.chefroom.in/api/"+child
}
let BASE_URL = "http://api.chefroom.in/"

let BASE_URL_New = "http://api.chefroom.in/api/"

//let BASE_URL = "http://api.chefroom.in/storage/app/"

class Constant: NSObject {

    static let APPNAME = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
}


class savedKeys: NSObject {
    static let username = "username"
    static let mobile = "mobile"
    static let device_token = "device_token"
    static let api_token = "api_token"
    static let userID = "userID"
    static let profile_image = "profile_image"
    static let email = "email"
    static let status = "status"
    static let lat = "lat"
    static let long = "long"
    static let isNotification = "isNotification"
}


class ProjectUrl: NSObject {
    
    static let updateNumberOtp = BASE_URL(child: "updateNumberOtp")
    static let verifyupdatenumberOtp = BASE_URL(child: "verifyupdatenumber_otp")
    static let registerTutor = BASE_URL(child: "registerTutor")
    static let getCitiesData = BASE_URL(child: "getCitiesData")
    static let sendOtp = BASE_URL(child: "sendOtp")
    static let verify_otp = BASE_URL(child: "verify_otp")
    static let getplacewheretoteach = BASE_URL(child: "getplacewheretoteach")
    static let getClassesData = BASE_URL(child: "getClassesData")
    static let getTutorSubjectdata = BASE_URL(child: "getTutorSubjectdata")
    static let TutorBiodata = BASE_URL(child: "TutorBiodata")
    static let ProfileImage = BASE_URL(child: "profile_image")
    static let tutorWeekSessionsData = BASE_URL(child: "tutorWeekSessionsData")
    static let tutorTodaysSessionsdata = BASE_URL(child: "tutorTodaysSessionsdata")
    static let tutorMonthsSessionsdata = BASE_URL(child: "tutorMonthsSessionsdata")
    static let cancelBookingByTutor = BASE_URL(child: "CancelBookingByTutor")
    static let CancelProgramSession = BASE_URL(child: "Cancel_Program_Session")
    static let showStudentList = BASE_URL(child: "showStudentList")
    static let rescheduleTutorSession = BASE_URL(child: "RescheduleTutorSession")
    static let showProgramList = BASE_URL(child: "showProgramList")
    static let tutorSelectedSubjectAndClassdata = BASE_URL(child: "tutorSelectedSubjectAndClassdata")
    static let addTutorPrograms = BASE_URL(child: "addTutorPrograms")
    static let editTutorPrograms = BASE_URL(child: "editTutorPrograms")
    static let tutorBookingslist = BASE_URL(child: "tutorBookingslist")
    static let notificationStatus = BASE_URL(child: "notificationStatus")
    static let acceptedBookingByTutor = BASE_URL(child: "AcceptedBookingByTutor")
    static let tutorMysessionsList = BASE_URL(child: "tutorMysessionsList")
    static let tutorShowProfile = BASE_URL(child: "tutorShowProfile")
    static let updateTutorProfile = BASE_URL(child: "update_tutor_profile")
    static let kyc_Documents = BASE_URL(child: "kyc_Documents")
    static let Notifications = BASE_URL(child: "Notifications")
    static let TermsandCoditions = BASE_URL(child: "TermsandCoditions")
    static let FAQ = BASE_URL(child: "FAQ")
    static let PrivacyPolicy = BASE_URL(child: "PrivacyPolicy")
    static let tutorFeedbacks = BASE_URL(child: "tutorFeedbacks")
    
    static let addBankDetails = BASE_URL(child: "add_bank_details")
    static let addSessionParameters = BASE_URL(child: "add_session_parameters")
}

class ProjectColor: NSObject {
    static let darkGreen = UIColor(red: 114/255, green: 177/255, blue: 32/255, alpha: 1.0) //72b120
    static let lightGreen = UIColor(red: 228/255, green: 226/255, blue: 76/255, alpha: 1.0) //e4e24c
}

class ProjectFont: NSObject {
    static func setFont(size: CGFloat) -> UIFont {
         return UIFont(name: "Quicksand-Regular", size: size)!
    }
}

class ProjectImages: NSObject {
    static let tutorial1 = UIImage(named: "BG.png")
    static let tutorial2 = UIImage(named: "BG-1.png")
    static let tutorial3 = UIImage(named: "blend-1.png")
}
