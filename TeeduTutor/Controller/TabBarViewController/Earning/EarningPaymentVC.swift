//
//  EarningPaymentVC.swift
//  TeeduTutor
//
//  Created by Ankit on 24/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SDWebImage
class EarningPaymentVC: BaseViewController {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtAccountNo: UITextField!
    
    @IBOutlet weak var txtIFC: UITextField!
        
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var btnImgVerify: UIButton!
    
    @IBOutlet weak var btnNOti: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
       btnSave.layer.cornerRadius=10
        
        if let name = UserDefaults.standard.value(forKey: "name") as? String {
            txtName.text = name
            self.btnSave.isEnabled = false
            self.txtAccountNo.isEnabled = false
            self.txtIFC.isEnabled = false
            self.txtBankName.isEnabled = false
            self.txtName.isEnabled = false
        }
        
        if let bank_name = UserDefaults.standard.value(forKey: "bank_name") as? String {
            txtBankName.text = bank_name
        }
        
        if let account_no = UserDefaults.standard.value(forKey: "account_no") as? String {
            txtAccountNo.text = account_no
        }
        
        if let ifsc = UserDefaults.standard.value(forKey: "ifsc") as? String {
            txtIFC.text = ifsc
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         setNavigation()
    }
    
    
    private func setNavigation() {
         
         self.hideHideNavigationBar()
         
         if let imageUrl = self.getData(key: savedKeys.profile_image) {
            let url = URL(string: imageUrl)
            self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            
//             RestApi.shared.getImage(url: imageUrl ) { (image) in
//                 if let image = image {
//                     self.imgProfile.image = image
//                 } else {
//                     self.imgProfile.image = #imageLiteral(resourceName: "avatar")
//                 }
//             }
         }
         if let imageVerified = self.getData(key: savedKeys.status) {
             if imageVerified == "VERIFIED" {
                 btnImgVerify.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
             } else {
                 btnImgVerify.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
             }
         }
     }
    
    @IBAction func btnVerifyClicked(_ sender: Any) {
    }
    
    @IBAction func btnNotification(_ sender: Any) {
    }
    
    @IBAction func btnSaveClick(_ sender: Any) {
        
        if self.isValidatedLogin() {
            callAddBackDetailsAPI()
        }
        
    }
    
    func isValidatedLogin() -> Bool {
        if txtName.text == "" {
            self.view.makeToast("Please enter name on the account", duration: 0.3, position: .center)
            return false
        } else if txtBankName.text == "" {
            self.view.makeToast("Please enter bank name", duration: 0.3, position: .center)
            return false
        } else if txtAccountNo.text == "" {
            self.view.makeToast("Please enter account number", duration: 0.3, position: .center)
            return false
        } else if txtIFC.text == "" {
            self.view.makeToast("Please enter IFSC code", duration: 0.3, position: .center)
            return false
        }
        return true
    }
    
    func callAddBackDetailsAPI() {
        
        self.showLoader()

        let param = ["name": txtName.text!,
                     "bank_name": txtBankName.text!,
                     "account_no": txtAccountNo.text!,
                     "ifsc": txtIFC.text!] as [String: Any]
        
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        
        RestApi.shared.requiredPOST(url: ProjectUrl.addBankDetails, param: param, header: header).responseAddBankDetailsModl { (response) in
            self.hideLoader()
            if let response = response.result.value {

                print(response)
                
               self.btnSave.isEnabled = false
                self.txtAccountNo.isEnabled = false
                self.txtIFC.isEnabled = false
                self.txtBankName.isEnabled = false
                self.txtName.isEnabled = false
                
                UserDefaults.standard.setValue(self.txtName.text!, forKey: "name")
                UserDefaults.standard.setValue(self.txtBankName.text!, forKey: "bank_name")
                UserDefaults.standard.setValue(self.txtAccountNo.text!, forKey: "account_no")
                UserDefaults.standard.setValue(self.txtIFC.text!, forKey: "ifsc")
                UserDefaults.standard.synchronize()

                self.showAlertMessage(titleStr: "", messageStr: response.message)

            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)

            }
            
        }
        
    }
}
