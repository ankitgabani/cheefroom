//
//  ThingNeedViewController.swift
//  TeeduTutor
//
//  Created by Ankit on 25/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class ThingNeedViewController: BaseViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnFinish: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var tblview: UITableView!
    
    @IBOutlet weak var btnVerify: UIButton!
    
    var arrHeader = ["Ingredients List","Utensils Required"]
    
    var arrIngredientsList : [Int] = []
    var arrUtensilsList : [Int] = []
    
    var arrStrIngredientsList : [String] = []
    var arrStrUtensilsList : [String] = []
    
    var dicIngredientList = NSMutableDictionary()
    var dicUtensilList = NSMutableDictionary()
    
    var objProgramListArr: ProgramListModl.Payload?
    
    var isFromEdit = false
    
    var isFir11 = false
    var isFir22 = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigation()
        
        if isFromEdit == true {
            lblTitle.text = "Edit a"
            btnFinish.setTitle("Save Changes", for: .normal)
            setUpEdit()
        } else {
            
            arrIngredientsList.append(1)
            arrUtensilsList.append(1)
            
            lblTitle.text = "Add a"
            btnFinish.setTitle("Finish", for: .normal)
        }
        
        tblview.delegate = self
        tblview.dataSource = self
        tblview.register(UINib(nibName: "AddSessionTYextCell", bundle: nil), forCellReuseIdentifier: "AddSessionTYextCell")
        
        
        // Do any additional setup after loading the view.
    }
    
    func setUpEdit() {
        
        let objIngredients = objProgramListArr?.ingredients_list
        let objUtensils = objProgramListArr?.utensils_required
        
        if let fullName = objIngredients {
            let arrIngredientsDe = fullName.components(separatedBy: ",")
            print(arrIngredientsDe)
            
            for index in 0..<arrIngredientsDe.count
            {
                arrIngredientsList.append(1)
                self.dicIngredientList.setObject(arrIngredientsDe[index], forKey: index as NSCopying)
            }
        }
        
        if let fullName1 = objUtensils {
            let arrUtensilsDe = fullName1.components(separatedBy: ",")
            print(arrUtensilsDe)
            
            for index in 0..<arrUtensilsDe.count
            {
                arrUtensilsList.append(1)
                self.dicUtensilList.setObject(arrUtensilsDe[index], forKey: index as NSCopying)
            }
        }
        
        
        self.tblview.reloadData()
    }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgProfile.image = image
                } else {
                    self.imgProfile.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                btnVerify.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                btnVerify.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnViridy(_ sender: Any) {
    }
    
    @IBAction func btnFinished(_ sender: Any) {
        self.showLoader()
        
        self.tblview.reloadData()
        
        perform(#selector(startAnimLogo), with: nil, afterDelay: 0.5)
        
    }
    
    @objc func startAnimLogo() {
        
        let dicIngra = dicIngredientList.allValues
        let dicUtensils = dicUtensilList.allValues
        
        let set = NSSet(array: dicIngra as [Any])
        let strings1 = set.allObjects as? [String] // or as!
        let finalStr = strings1?.joined(separator: ",")
        
        let set1 = NSSet(array: dicUtensils as [Any])
        let strings2 = set1.allObjects as? [String] // or as!
        let finalStr2 = strings2?.joined(separator: ",")
        
        AppData.sharedInstance.ingredients_list = finalStr
        AppData.sharedInstance.utensils_required = finalStr2
        
        if isFromEdit == true {
            callEditSessionAPI()
        } else {
            callAddSessionAPI()
        }
        
    }
    
    func callEditSessionAPI() {
        //self.showLoader()
        let sessionName = AppData.sharedInstance.sessionName ?? ""
        let cuisine = AppData.sharedInstance.cuisine ?? ""
        let fee = AppData.sharedInstance.fee ?? ""
        let languages = AppData.sharedInstance.languages ?? ""
        let suitable_for = AppData.sharedInstance.suitable_for ?? ""
        let session_type = AppData.sharedInstance.session_type ?? ""
        let hashtag = AppData.sharedInstance.hashtag ?? ""
        let selectedImage = AppData.sharedInstance.image
        
        let session_frequency = AppData.sharedInstance.session_frequency ?? ""
        let working_days = AppData.sharedInstance.working_days ?? ""
        let schedule_start_date = AppData.sharedInstance.schedule_start_date ?? ""
        let schedule_end_date = AppData.sharedInstance.schedule_end_date ?? ""
        let start_from = AppData.sharedInstance.start_from ?? ""
        let end_to = AppData.sharedInstance.end_to ?? ""
        
        let descriptions = AppData.sharedInstance.descriptions ?? ""
        let instruction_video = AppData.sharedInstance.instruction_video ?? ""
        
        let ingredients_list = AppData.sharedInstance.ingredients_list ?? ""
        let utensils_required = AppData.sharedInstance.utensils_required ?? ""
        let file_name = AppData.sharedInstance.fileName ?? ""
        
        let param = ["session_name": sessionName,"cuisine": cuisine,"fee": fee, "languages": languages,"suitable_for": suitable_for,"session_type": session_type,"hashtag": hashtag,"session_frequency" :session_frequency,"working_days":working_days,"schedule_start_date":schedule_start_date,"schedule_end_date":schedule_end_date,"start_from":start_from,"end_to":end_to,"description":descriptions,"instruction_video":instruction_video,"ingredients_list":ingredients_list,"utensils_required":utensils_required,"id": objProgramListArr?.id ?? ""]
        
        print(param)
        APIClient.sharedInstance.postImageToServer(self.getData(key: savedKeys.api_token)!, "edit_session", file_name, image: selectedImage, parameters: param) { (response, error, statusCode) in
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            if error == nil {
                self.hideLoader()
                if statusCode == 200
                {
                    let code = response?.value(forKey: "code") as? Int
                    let message = response?.value(forKey: "message") as? String
                    print("upload")
                    
                    if code == 1 {
                        
                        print("upload")
                        
                        let redViewController = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                        redViewController.selectedIndex = 1
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = redViewController
                        
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                        vc.selectedIndex = 1
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        
                        let alert = UIAlertController(title: "ChefRoom", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                    
                }else
                {
                    print("not upload")
                    print("STATUS CODE \(String(describing: statusCode))")
                    
                    
                }
            }
            else{
                self.hideLoader()
                print("not upload")
                // self.hideHUD()
                
            }
        }
        
    }
    
    
    func callAddSessionAPI() {
        //  self.showLoader()
        let sessionName = AppData.sharedInstance.sessionName ?? ""
        let cuisine = AppData.sharedInstance.cuisine ?? ""
        let fee = AppData.sharedInstance.fee ?? ""
        let languages = AppData.sharedInstance.languages ?? ""
        let suitable_for = AppData.sharedInstance.suitable_for ?? ""
        let session_type = AppData.sharedInstance.session_type ?? ""
        let hashtag = AppData.sharedInstance.hashtag ?? ""
        let selectedImage = AppData.sharedInstance.image
        
        let session_frequency = AppData.sharedInstance.session_frequency ?? ""
        let working_days = AppData.sharedInstance.working_days ?? ""
        let schedule_start_date = AppData.sharedInstance.schedule_start_date ?? ""
        let schedule_end_date = AppData.sharedInstance.schedule_end_date ?? ""
        let start_from = AppData.sharedInstance.start_from ?? ""
        let end_to = AppData.sharedInstance.end_to ?? ""
        
        let descriptions = AppData.sharedInstance.descriptions ?? ""
        let instruction_video = AppData.sharedInstance.instruction_video ?? ""
        
        let ingredients_list = AppData.sharedInstance.ingredients_list ?? ""
        let utensils_required = AppData.sharedInstance.utensils_required ?? ""
        let file_name = AppData.sharedInstance.fileName ?? ""
        
        
        let param = ["session_name": sessionName,"cuisine": cuisine,"fee": fee, "languages": languages,"suitable_for": suitable_for,"session_type": session_type,"hashtag": "#\(hashtag)","session_frequency" :session_frequency,"working_days":working_days,"schedule_start_date":schedule_start_date,"schedule_end_date":schedule_end_date,"start_from":start_from,"end_to":end_to,"description":descriptions,"instruction_video":instruction_video,"ingredients_list":ingredients_list,"utensils_required":utensils_required]
        
        print(param)
        APIClient.sharedInstance.postImageToServer(self.getData(key: savedKeys.api_token)!, "add_session", file_name, image: selectedImage, parameters: param) { (response, error, statusCode) in
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            if error == nil {
                self.hideLoader()
                if statusCode == 200
                {
                    let code = response?.value(forKey: "code") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if code == 1 {
                        
                        print("upload")
                        
                        let redViewController = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                        redViewController.selectedIndex = 1
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = redViewController
                        
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                        vc.selectedIndex = 1
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        
                        let alert = UIAlertController(title: "ChefRoom", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }else
                {
                    print("not upload")
                    print("STATUS CODE \(String(describing: statusCode))")
                }
            }
            else{
                self.hideLoader()
                print("not upload")
                // self.hideHUD()
                
            }
        }
        
    }
    
}

extension ThingNeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return arrIngredientsList.count
        } else {
            return arrUtensilsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblview.dequeueReusableCell(withIdentifier: "AddSessionTYextCell") as! AddSessionTYextCell
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
                if isFir11 == true {
                    cell.txtSession.placeholder = "Add here |"
                    cell.txtSession.isEnabled = true
                    cell.txtSession.delegate = self
                    cell.txtSession.text = self.dicIngredientList.object(forKey: indexPath.row) as? String
                    
                    //                    let searchToSearch = cell.txtSession.text!
                    //
                    //                    if searchToSearch != "" {
                    //                        self.arrStrIngredientsList.append(cell.txtSession.text!)
                    //                    }
                    
                } else {
                    cell.txtSession.placeholder = "Please add all the ingredients with quantity"
                    cell.txtSession.text = self.dicIngredientList.object(forKey: indexPath.row) as? String
                    
                    cell.txtSession.delegate = self
                    cell.txtSession.isEnabled = false
                }
                
            } else {
                cell.txtSession.placeholder = "Add here |"
                cell.txtSession.isEnabled = true
                cell.txtSession.delegate = self
                
                cell.txtSession.text = self.dicIngredientList.object(forKey: indexPath.row) as? String
                
                //                let searchToSearch = cell.txtSession.text!
                //
                //                if searchToSearch != "" {
                //                    self.arrStrIngredientsList.append(cell.txtSession.text!)
                //                }
            }
            
        } else {
            
            if indexPath.row == 0 {
                
                if isFir22 == true {
                    cell.txtSession.placeholder = "Add here |"
                    cell.txtSession.isEnabled = true
                    cell.txtSession.delegate = self
                    
                    //                    let searchToSearch = cell.txtSession.text!
                    //
                    //                    if searchToSearch != "" {
                    //                        self.arrStrUtensilsList.append(cell.txtSession.text!)
                    //                    }
                    
                    cell.txtSession.text = self.dicUtensilList.object(forKey: indexPath.row) as? String
                    
                    
                } else {
                    cell.txtSession.placeholder = "E.g. Type of Pan"
                    cell.txtSession.isEnabled = false
                    cell.txtSession.text = self.dicUtensilList.object(forKey: indexPath.row) as? String
                    
                }
                
            } else {
                cell.txtSession.placeholder = "Add here |"
                cell.txtSession.isEnabled = true
                cell.txtSession.delegate = self
                
                cell.txtSession.text = self.dicUtensilList.object(forKey: indexPath.row) as? String
                
                
                //                let searchToSearch = cell.txtSession.text!
                //
                //                if searchToSearch != "" {
                //                    self.arrStrUtensilsList.append(cell.txtSession.text!)
                //                }
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("SessionHederView", owner: self, options: [:])?.first as! SessionHederView
        
        headerView.lblName.text = arrHeader[section]
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = Bundle.main.loadNibNamed("SessionFooterView", owner: self, options: [:])?.first as! SessionFooterView
        
        if section == 0 {
            
            footerView.btnAddSession.removeTarget(self, action: #selector(clickedAddSession), for: .touchUpInside)
            footerView.btnAddSession.addTarget(self, action: #selector(clickedAddSession1), for: .touchUpInside)
            
        } else {
            
            footerView.btnAddSession.removeTarget(self, action: #selector(clickedAddSession1), for: .touchUpInside)
            footerView.btnAddSession.addTarget(self, action: #selector(clickedAddSession), for: .touchUpInside)
            
        }
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    @objc func clickedAddSession1() {
        
        if isFir11 == false {
            isFir11 = true
            self.tblview.reloadData()
            
        } else {
            // arrIngredientsList + [1]
            arrIngredientsList.append(1)
            self.tblview.reloadData()
        }
        
    }
    
    @objc func clickedAddSession() {
        
        if isFir22 == false {
            isFir22 = true
            self.tblview.reloadData()
            
        } else {
            // arrUtensilsList + [1]
            arrUtensilsList.append(1)
            
            self.tblview.reloadData()
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let cell = textField.superview?.superview?.superview?.superview as! AddSessionTYextCell
        
        let indexPath = tblview.indexPath(for: cell)
        
        if indexPath?.section == 0
        {
            self.dicIngredientList.setObject(textField.text!, forKey: indexPath?.row as! NSCopying)
        }
        else{
            self.dicUtensilList.setObject(textField.text!, forKey: indexPath?.row as! NSCopying)
            
        }
        
        self.tblview.reloadData()
    }
    
}
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}
