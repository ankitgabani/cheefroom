//
//  SetupScheduleVC.swift
//  TeeduTutor
//
//  Created by Ankit on 25/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class SetupScheduleVC: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var viewTarget: UIView!
    @IBOutlet weak var lblSession: UILabel!
    
    @IBOutlet weak var imgMon: UIImageView!
    @IBOutlet weak var imhTue: UIImageView!
    @IBOutlet weak var imgWed: UIImageView!
    @IBOutlet weak var imgFrd: UIImageView!
    @IBOutlet weak var imgSun: UIImageView!
    @IBOutlet weak var imgSat: UIImageView!
    @IBOutlet weak var imgTur: UIImageView!
    
    @IBOutlet weak var lblStarDate: UILabel!
    
    @IBOutlet weak var lblEndDate: UILabel!
    
    @IBOutlet weak var lnlFromTime: UILabel!
    @IBOutlet weak var lblToTime: UILabel!
    
    
    let datePicker = UIDatePicker()
    
    var objschedule_start_date: String?
    var objschedule_end_date: String?
    let dropDownSessionType = DropDown()
    
    var objProgramListArr: ProgramListModl.Payload?
    
    var isFromEdit = false
    
    var arrDaySelection = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit == true {
            lblTitle.text = "Edit a"
            setUpEdit()
        } else {
            lblTitle.text = "Add a"
        }
        
        self.hidesBottomBarWhenPushed = true
        setNavigation()
        
        setDropDownSessionType()
        // Do any additional setup after loading the view.
    }
    
    func setUpEdit() {
        print(objProgramListArr?.workingDays ?? "")
        
        let getWarkingDays = objProgramListArr?.workingDays ?? ""
        
        if getWarkingDays.contains("Mon") {
            imgMon.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Mon")
        }
        if getWarkingDays.contains("Tue") {
            imhTue.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Tue")
        }
        
        if getWarkingDays.contains("Wed") {
            imgWed.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Wed")
        }
        
        if getWarkingDays.contains("Thu") {
            imgTur.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Thu")
        }
        
        if getWarkingDays.contains("Fri") {
            imgFrd.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Fri")
        }
        
        if getWarkingDays.contains("Sat") {
            imgSat.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Sat")
        }
        
        if getWarkingDays.contains("Sun") {
            imgSun.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Sun")
        }
        
        objschedule_start_date = objProgramListArr?.schedule_start_date ?? ""
        objschedule_end_date = objProgramListArr?.schedule_end_date ?? ""
        
        var dateFromString = ""
        
        if let strStart = objProgramListArr?.schedule_start_date {
            
            let arrSplit1 = strStart.components(separatedBy: "-")
            let strYear = arrSplit1[0] as! String
            let strMonth = arrSplit1[1] as! String
            let strDay = arrSplit1[2] as! String
            
            let monthNumber = (Int)(strMonth)
            if monthNumber != 0 {
                let fmt = DateFormatter()
                //fmt.dateFormat = "MMM"
                fmt.dateStyle = .medium
                let strMonthName = fmt.monthSymbols[monthNumber! - 1]
                dateFromString = String.init(format: "%@ %@ %@", strMonthName,strDay,strYear)
            }
        }
        
        lblStarDate.text = dateFromString
        
        var dateFromString1 = ""
        if let strStart1 = objProgramListArr?.schedule_end_date {
            
            let arrSplit2 = strStart1.components(separatedBy: "-")
            let strYear1 = arrSplit2[0] as! String
            let strMonth1 = arrSplit2[1] as! String
            let strDay1 = arrSplit2[2] as! String
            
            let monthNumber1 = (Int)(strMonth1)
            if monthNumber1 != 0 {
                let fmt1 = DateFormatter()
                //fmt.dateFormat = "MMM"
                fmt1.dateStyle = .medium
                let strMonthName1 = fmt1.monthSymbols[monthNumber1! - 1]
                dateFromString1 = String.init(format: "%@ %@ %@", strMonthName1,strDay1,strYear1)
                
            }
            
        }
        lblEndDate.text = dateFromString1
        
        
        lblSession.text = objProgramListArr?.session_frequency ?? ""
        lnlFromTime.text = objProgramListArr?.startFrom ?? ""
        lblToTime.text = objProgramListArr?.endTo ?? ""
    }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgProfile.image = image
                } else {
                    self.imgProfile.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                btnVerify.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                btnVerify.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    func setDropDownSessionType() {
        dropDownSessionType.dataSource = ["Only Once", "Daily", "Weekly"]
        dropDownSessionType.anchorView = viewTarget
        dropDownSessionType.direction = .any
        dropDownSessionType.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblSession.text = item
        }
        dropDownSessionType.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDownSessionType.topOffset = CGPoint(x: 0, y: -viewTarget.bounds.height)
        dropDownSessionType.dismissMode = .onTap
        dropDownSessionType.textColor = UIColor.darkGray
        dropDownSessionType.backgroundColor = UIColor.white
        dropDownSessionType.selectionBackgroundColor = UIColor.clear
        dropDownSessionType.reloadAllComponents()
    }
    
    @IBAction func btnChooseSeccion(_ sender: Any) {
        dropDownSessionType.show()
    }
    
    @IBAction func btnVerifyProfile(_ sender: Any) {
        
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedMon(_ sender: Any) {
        
        if imgMon.image == UIImage(named: "Group 2335") {
            imgMon.image = UIImage(named: "Group -3")
            arrDaySelection.remove("Mon")
        } else {
            imgMon.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Mon")
        }
    }
    
    @IBAction func clickedTue(_ sender: Any) {
        
        if imhTue.image == UIImage(named: "Group 2335") {
            imhTue.image = UIImage(named: "Group -3")
            arrDaySelection.remove("Tue")
        } else {
            imhTue.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Tue")
        }
    }
    
    @IBAction func clickedWed(_ sender: Any) {
        if imgWed.image == UIImage(named: "Group 2335") {
            imgWed.image = UIImage(named: "Group -3")
            arrDaySelection.remove("Wed")
        } else {
            imgWed.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Wed")
        }
    }
    
    @IBAction func clickedTur(_ sender: Any) {
        if imgTur.image == UIImage(named: "Group 2335") {
            imgTur.image = UIImage(named: "Group -3")
            arrDaySelection.remove("Thu")
        } else {
            imgTur.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Thu")
        }
    }
    
    @IBAction func clickedFrd(_ sender: Any) {
        if imgFrd.image == UIImage(named: "Group 2335") {
            imgFrd.image = UIImage(named: "Group -3")
            arrDaySelection.remove("Fri")
        } else {
            imgFrd.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Fri")
        }
    }
    
    @IBAction func clickedSat(_ sender: Any) {
        if imgSat.image == UIImage(named: "Group 2335") {
            imgSat.image = UIImage(named: "Group -3")
            arrDaySelection.remove("Sat")
        } else {
            imgSat.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Sat")
        }
    }
    
    @IBAction func clickedSun(_ sender: Any) {
        if imgSun.image == UIImage(named: "Group 2335") {
            imgSun.image = UIImage(named: "Group -3")
            arrDaySelection.remove("Sun")
        } else {
            imgSun.image = UIImage(named: "Group 2335")
            arrDaySelection.add("Sun")
        }
    }
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
        if lblSession.text == nil {
            self.view.makeToast("Please select session frequency", duration: 0.3, position: .center)
            return false
        } else if lblStarDate.text == "" {
            self.view.makeToast("Please select schedule starts from", duration: 0.3, position: .center)
            return false
        } else if lblEndDate.text == "" {
            self.view.makeToast("Please select schedule ends on", duration: 0.3, position: .center)
            return false
        } else if lnlFromTime.text == "" {
            self.view.makeToast("Please select from time", duration: 0.3, position: .center)
            return false
        } else if lblToTime.text == "" {
            self.view.makeToast("Please select to time", duration: 0.3, position: .center)
            return false
        }
        return true
    }
    
    @IBAction func clickedStartDate(_ sender: UITextField) {
        
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        sender.inputAccessoryView = toolbar
        sender.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        objschedule_start_date = formatter1.string(from: datePicker.date)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd yyyy"
        lblStarDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        self.view.endEditing(true)
    }
    
    @IBAction func clickedFromTime(_ sender: UITextField) {
        datePicker.datePickerMode = .time
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerFrom));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        sender.inputAccessoryView = toolbar
        sender.inputView = datePicker
    }
    
    @objc func donedatePickerFrom(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        lnlFromTime.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @IBAction func clickedToTime(_ sender: UITextField) {
        datePicker.datePickerMode = .time
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerTo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        sender.inputAccessoryView = toolbar
        sender.inputView = datePicker
    }
    
    @objc func donedatePickerTo(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        lblToTime.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @IBAction func clickedEndDate(_ sender: UITextField) {
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerED));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        sender.inputAccessoryView = toolbar
        sender.inputView = datePicker
    }
    
    @objc func donedatePickerED(){
        //For date formate
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        objschedule_end_date = formatter1.string(from: datePicker.date)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd yyyy"
        lblEndDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        let set = NSSet(array: arrDaySelection as! [Any])
        print(set)
        let strings1 = set.allObjects as? [String] // or as!
        
        let finalStr = strings1?.joined(separator: ",")
        
        if self.isValidatedLogin() {
            
            AppData.sharedInstance.session_frequency = lblSession.text!
            AppData.sharedInstance.working_days = finalStr
            AppData.sharedInstance.schedule_start_date = objschedule_start_date
            AppData.sharedInstance.schedule_end_date = objschedule_end_date
            AppData.sharedInstance.start_from = lnlFromTime.text!
            AppData.sharedInstance.end_to = lblToTime.text!
            
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: MoreAboutSessionVC.self)) as! MoreAboutSessionVC
            vc.hidesBottomBarWhenPushed = true
            vc.isFromEdit = self.isFromEdit
            vc.objProgramListArr = self.objProgramListArr
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
