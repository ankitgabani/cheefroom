//
//  SessionInformationVC.swift
//  TeeduTutor
//
//  Created by Ankit on 25/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class SessionInformationVC: BaseViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imgSessionProfile: UIImageView!
    @IBOutlet weak var ViewLL: UIView!
    @IBOutlet weak var viewSS: UIView!
    
    //MARK:- IBOutlet
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblTitleAlertNmae: UILabel!
    @IBOutlet weak var collection: UICollectionView!
    
    @IBOutlet weak var collecTionSS: UICollectionView!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgProlfie: UIImageView!
    @IBOutlet weak var btnVerify: UIButton!
    
    @IBOutlet weak var btnSelectProfile: UIButton!
    @IBOutlet weak var txtEnterSessionName: UITextView!
    
    
    @IBOutlet weak var lblCuisine: UILabel!
    @IBOutlet weak var viewTargetCuisine: UIView!
    
    @IBOutlet weak var lblLanhuge: UILabel!
    @IBOutlet weak var viewTargetLangu: UIView!
    
    
    @IBOutlet weak var lblSuitble: UILabel!
    @IBOutlet weak var viewTargetSuitbale: UIView!
    
    @IBOutlet weak var lblSecctionType: UILabel!
    
    @IBOutlet weak var viewTargetSesstion: UIView!
    
    @IBOutlet weak var txtFess: UITextField!
    
    @IBOutlet weak var txtHashTag: UITextView!
    
    let dropDownSessionType = DropDown()
    let dropDownCuisines = DropDown()
    let dropDownLanguage = DropDown()
    let dropDownSuitablefor = DropDown()
    
    var arrCuisines: [String] = []
    var arrSkillset: [String] = []
    var arrLanguages: [String] = []
    var arrSuitablefor: [String] = []
    
    var arrCuisinesID: [String] = []
    
    
    var arrLangColl = NSMutableArray()
    var arrSuitableColl = NSMutableArray()
    var arrCuisinesCuisines: [NSMutableArray] = []
    
    @IBOutlet weak var lblTitle: UILabel!
    
    var objProgramListArr: ProgramListModl.Payload?
    
    var isFromEdit = false
    
    var isLangugee = false
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.isHidden = true
        collection.allowsMultipleSelection=true
        collection.register(UINib(nibName: "CuisineCell", bundle: nil), forCellWithReuseIdentifier: "CuisineCell")
        self.collection.register(UINib(nibName: String(describing: CuisineHeader.self), bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: CuisineHeader.self))
        collection.delegate = self
        collection.dataSource = self
        
        
        collecTionSS.allowsMultipleSelection=true
        collecTionSS.register(UINib(nibName: "CuisineCell", bundle: nil), forCellWithReuseIdentifier: "CuisineCell")
        self.collecTionSS.register(UINib(nibName: String(describing: CuisineHeader.self), bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: CuisineHeader.self))
        collecTionSS.delegate = self
        collecTionSS.dataSource = self
        
        if isFromEdit == true {
            lblTitle.text = "Edit a"
            setUpEdit()
        } else {
            lblTitle.text = "Add a"
        }
        
        DispatchQueue.main.async { [weak self] in
            self!.hidesBottomBarWhenPushed = true
        }
        
        setNavigation()
        
        btnSelectProfile.layer.cornerRadius = btnSelectProfile.frame.height / 2
        btnSelectProfile.clipsToBounds = true
        
        
        imgSessionProfile.layer.cornerRadius = imgSessionProfile.frame.height / 2
        imgSessionProfile.clipsToBounds = true

        getProgramListApi()
        setDropDownSessionType()
        
        txtFess.addTarget(self, action: #selector(SkillSetVC.textFieldDidChange(_:)),
                          for: UIControl.Event.editingChanged)
        // Do any additional setup after loading the view.
        
    }
    
    func setUpEdit() {
        
        if let imageUrl = objProgramListArr?.image {
            RestApi.shared.getImage(url: BASE_URL+imageUrl) { (image) in
                if let image = image {
                    //self.imgSessionProfile.image = image
                    self.btnSelectProfile.setImage(image, for: .normal)
                    AppData.sharedInstance.image = image
                }
            }
        } else {
            imgProlfie.image = #imageLiteral(resourceName: "avatar")
        }
        
        let filePathOld = objProgramListArr?.image ?? ""
        let parsed = filePathOld.replacingOccurrences(of: "uploads/app/public/program_images/", with: "")

        AppData.sharedInstance.fileName =  parsed
        
        txtEnterSessionName.text! = objProgramListArr?.session_name ?? ""
        lblCuisine.text! = objProgramListArr?.cuisine ?? ""
        txtFess.text! = objProgramListArr?.fee ?? ""
        lblLanhuge.text! = objProgramListArr?.languages ?? ""
        lblSuitble.text! = objProgramListArr?.suitable_for ?? ""
        lblSecctionType.text! = objProgramListArr?.session_type ?? ""
        txtHashTag.text! = objProgramListArr?.hashtag ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hidesBottomBarWhenPushed = true
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
    }
    
    func setDropDownCuisines() {
        dropDownCuisines.dataSource = arrCuisines
        dropDownCuisines.anchorView = viewTargetCuisine
        dropDownCuisines.direction = .any
        dropDownCuisines.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblCuisine.text = item
            
        }
        dropDownCuisines.bottomOffset = CGPoint(x: 0, y: viewTargetCuisine.bounds.height)
        dropDownCuisines.topOffset = CGPoint(x: 0, y: -viewTargetCuisine.bounds.height)
        dropDownCuisines.dismissMode = .onTap
        dropDownCuisines.textColor = UIColor.darkGray
        dropDownCuisines.backgroundColor = UIColor.white
        dropDownCuisines.selectionBackgroundColor = UIColor.clear
        dropDownCuisines.reloadAllComponents()
    }
    
    func setDropDownSuitablefor() {
        dropDownSuitablefor.dataSource = arrSuitablefor
        dropDownSuitablefor.anchorView = viewTargetSuitbale
        dropDownSuitablefor.direction = .any
        dropDownSuitablefor.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if self.lblSuitble.text != "Choose multiple" {
                
                if self.lblSuitble.text!.range(of: item) != nil {
                    print("exists")
                } else {
                    let addLan = "\(self.lblSuitble.text ?? ""), \(item)"
                    self.lblSuitble.text = addLan
                }
                
            } else {
                self.lblSuitble.text = item
            }
        }
        
        dropDownSuitablefor.bottomOffset = CGPoint(x: 0, y: viewTargetSuitbale.bounds.height)
        dropDownSuitablefor.topOffset = CGPoint(x: 0, y: -viewTargetSuitbale.bounds.height)
        dropDownSuitablefor.dismissMode = .onTap
        dropDownSuitablefor.textColor = UIColor.darkGray
        dropDownSuitablefor.backgroundColor = UIColor.white
        dropDownSuitablefor.selectionBackgroundColor = UIColor.clear
        dropDownSuitablefor.reloadAllComponents()
    }
    
    func setDropDownLanguages() {
        dropDownLanguage.dataSource = arrLanguages
        dropDownLanguage.anchorView = viewTargetLangu
        dropDownLanguage.direction = .any
        dropDownLanguage.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if self.lblLanhuge.text != "Choose multiple" {
                
                if self.lblLanhuge.text!.range(of: item) != nil {
                    print("exists")
                } else {
                    let addLan = "\(self.lblLanhuge.text ?? ""), \(item)"
                    self.lblLanhuge.text = addLan
                }
                
            } else {
                self.lblLanhuge.text = item
            }
            
        }
        dropDownLanguage.bottomOffset = CGPoint(x: 0, y: viewTargetLangu.bounds.height)
        dropDownLanguage.topOffset = CGPoint(x: 0, y: -viewTargetLangu.bounds.height)
        dropDownLanguage.dismissMode = .onTap
        dropDownLanguage.textColor = UIColor.darkGray
        dropDownLanguage.backgroundColor = UIColor.white
        dropDownLanguage.selectionBackgroundColor = UIColor.clear
        dropDownLanguage.reloadAllComponents()
    }
    
    func setDropDownSessionType() {
        dropDownSessionType.dataSource = ["One Participant", "Group"]
        dropDownSessionType.anchorView = viewTargetSesstion
        dropDownSessionType.direction = .any
        dropDownSessionType.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblSecctionType.text = item
        }
        dropDownSessionType.bottomOffset = CGPoint(x: 0, y: viewTargetSesstion.bounds.height)
        dropDownSessionType.topOffset = CGPoint(x: 0, y: -viewTargetSesstion.bounds.height)
        dropDownSessionType.dismissMode = .onTap
        dropDownSessionType.textColor = UIColor.darkGray
        dropDownSessionType.backgroundColor = UIColor.white
        dropDownSessionType.selectionBackgroundColor = UIColor.clear
        dropDownSessionType.reloadAllComponents()
    }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgProlfie.image = image
                } else {
                    self.imgProlfie.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                btnVerify.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                btnVerify.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    private func openActionSheet(){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openGallery()
    {
        let Imgpicker = UIImagePickerController()
        Imgpicker.delegate = self
        Imgpicker.sourceType = .photoLibrary
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    private func openCamera(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            let Imgpicker = UIImagePickerController()
            Imgpicker.delegate = self
            Imgpicker.sourceType = .camera
            Imgpicker.allowsEditing = true
            present(Imgpicker, animated: true, completion: nil)
            
        }
        else
        {
            self.showAlertMessage(titleStr: nil, messageStr: "You don't have camera")
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.editedImage] as? UIImage {
            btnSelectProfile.imageView?.contentMode = .scaleAspectFill
            imgSessionProfile.contentMode = .scaleAspectFill
            
            let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            let fileName = url!.lastPathComponent

            //imgSessionProfile.image = pickedImage
            btnSelectProfile.setImage(pickedImage, for: .normal)
            
            AppData.sharedInstance.fileName = fileName

            AppData.sharedInstance.image = pickedImage
        }
        
        picker.dismiss(animated:true,completion:nil)
    }
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {

        if btnSelectProfile.currentImage == nil {
            self.view.makeToast("Please upload a picture", duration: 0.3, position: .center)
             return false
        } else if txtEnterSessionName.text == "" {
            self.view.makeToast("Please enter session name", duration: 0.3, position: .center)
            return false
        } else if lblCuisine.text == "Choose one" {
            self.view.makeToast("Please select cuisine", duration: 0.3, position: .center)
            return false
        } else if lblLanhuge.text == "Choose multiple" {
            self.view.makeToast("Please choose language", duration: 0.3, position: .center)
            return false
        } else if lblSuitble.text == "Choose multiple" {
            self.view.makeToast("Please choose suitable for", duration: 0.3, position: .center)
            return false
        } else if lblSecctionType.text == "Choose one" {
            self.view.makeToast("Please enter session type", duration: 0.3, position: .center)
            return false
        } else if txtFess.text == "" {
            self.view.makeToast("Please enter fess", duration: 0.3, position: .center)
            return false
        } else if txtHashTag.text == "" {
            self.view.makeToast("Please enter hashtag", duration: 0.3, position: .center)
            return false
        }
        return true
    }
    
    //MARK:- Action Method
    @IBAction func btnChooseProile(_ sender: Any) {
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedChoosePhoto(_ sender: Any) {
        openActionSheet()
    }
    @IBAction func clickedChooseSesstion(_ sender: Any) {
        dropDownSessionType.show()
    }
    
    @IBAction func clickedChooseCuisie(_ sender: Any) {
        dropDownCuisines.show()
    }
    
    @IBAction func clickedChooseLange(_ sender: Any) {
        alertView.isHidden = false
        ViewLL.isHidden = false
        viewSS.isHidden = true
        
        //dropDownLanguage.show()
    }
    
    @IBAction func clickedSuitable(_ sender: Any) {
        alertView.isHidden = false
        ViewLL.isHidden = true
        viewSS.isHidden = false
        
        //dropDownSuitablefor.show()
    }
    @IBAction func clickedNext(_ sender: Any) {
        
        if self.isValidatedLogin() {
            AppData.sharedInstance.sessionName = txtEnterSessionName.text!
            AppData.sharedInstance.cuisine = lblCuisine.text!
            AppData.sharedInstance.fee = txtFess.text!
            AppData.sharedInstance.languages = lblLanhuge.text!
            AppData.sharedInstance.suitable_for = lblSuitble.text!
            AppData.sharedInstance.session_type = lblSecctionType.text!
            AppData.sharedInstance.hashtag = txtHashTag.text!
            
            
            
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SetupScheduleVC.self)) as! SetupScheduleVC
            vc.isFromEdit = self.isFromEdit
            vc.objProgramListArr = self.objProgramListArr
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    //MARK:- API call
    
    func getProgramListApi() {
        
        let param = ["": ""]
        self.showLoader()
        print(param)
        APIClient.sharedInstance.MakeAPICallWihAuthHeaderTokenQuery(self.getData(key: savedKeys.api_token)!, url: "add_session_parameters", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    self.hideLoader()
                    
                    if let responseUser = response {
                        
                        print(responseUser)
                        let dicResponse = responseUser.value(forKey: "payload") as? NSDictionary
                        
                        //  self.arrCuisines = dicResponse?.value(forKey: "cuisines") as? NSArray
                        //  self.arrLanguages = dicResponse?.value(forKey: "languages") as? NSArray
                        //  self.arrSuitablefor = dicResponse?.value(forKey: "suitable_for") as? NSArray
                        
                        let arrSTR = dicResponse?.value(forKey: "cuisines") as? NSArray
                        for index in arrSTR! {
                            let str11 = index as? NSDictionary
                            let str = str11?.value(forKey: "cuisine_title") as? String
                            let strID = str11?.value(forKey: "cuisine_id") as? String
                            self.arrCuisines.append(str ?? "")
                            self.arrCuisinesID.append(strID ?? "")
                        }
                        self.setDropDownCuisines()
                        
                        self.arrLanguages = dicResponse?.value(forKey: "languages") as! [String]
                        self.setDropDownLanguages()
                        
                        self.arrSuitablefor = dicResponse?.value(forKey: "suitable_for") as! [String]
                        self.setDropDownSuitablefor()
                        
                        self.collection.reloadData()
                        self.collecTionSS.reloadData()
                        
                    } else {
                        
                    }
                    
                } else {
                    
                    self.hideLoader()
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.hideLoader()
            }
        })
    }
    
    @IBAction func btnAddLanguOK(_ sender: Any) {
        alertView.isHidden = true
        
        let set = NSSet(array: arrLangColl as! [Any])
        print(set)
        let strings1 = set.allObjects as? [String] // or as!
        
        let finalStr = strings1?.joined(separator: ",")
        self.lblLanhuge.text = finalStr
        
        if self.lblLanhuge.text == "" {
            self.lblLanhuge.text = "Choose multiple"
        }
        
    }
    
    
    @IBAction func btnOKSuss(_ sender: Any) {
         alertView.isHidden = true
        let set = NSSet(array: arrSuitableColl as! [Any])
        print(set)
        let strings1 = set.allObjects as? [String] // or as!
        
        let finalStr = strings1?.joined(separator: ",")
        self.lblSuitble.text = finalStr
        
        if self.lblSuitble.text == "" {
            self.lblSuitble.text = "Choose multiple"
        }
    }
    
}

extension SessionInformationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collection {
            return arrLanguages.count
        } else {
            return arrSuitablefor.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 8)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CuisineCell.self), for: indexPath) as? CuisineCell else { return UICollectionViewCell() }
        
        if collectionView == collection {
            cell.name.text = arrLanguages[indexPath.row]
        } else {
            cell.name.text = arrSuitablefor[indexPath.row]
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! CuisineCell
        
        let name = cell.name.text!
        
        if collectionView == collection {
            if cell.image.image == UIImage(named: "check") {
                cell.image.image = UIImage(named: "Group -3")
                arrLangColl.remove(name)
            } else {
                cell.image.image = UIImage(named: "check")
                arrLangColl.add(name)
            }
        } else {
            if cell.image.image == UIImage(named: "check") {
                cell.image.image = UIImage(named: "Group -3")
                arrSuitableColl.remove(name)
            } else {
                cell.image.image = UIImage(named: "check")
                arrSuitableColl.add(name)
            }
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: 60)
    }
}

extension UITextView {
    
    private class PlaceholderLabel: UILabel { }
    
    private var placeholderLabel: PlaceholderLabel {
        if let label = subviews.compactMap( { $0 as? PlaceholderLabel }).first {
            return label
        } else {
            let label = PlaceholderLabel(frame: .zero)
            label.font = font
            label.textColor = UIColor(red: 127/255, green: 129/255, blue: 131/255, alpha: 1)
            addSubview(label)
            return label
        }
    }
    
    @IBInspectable
    var placeholder: String {
        get {
            return subviews.compactMap( { $0 as? PlaceholderLabel }).first?.text ?? ""
        }
        set {
            let placeholderLabel = self.placeholderLabel
            placeholderLabel.text = newValue
            placeholderLabel.numberOfLines = 0
            let width = frame.width - textContainer.lineFragmentPadding * 2
            let size = placeholderLabel.sizeThatFits(CGSize(width: width, height: .greatestFiniteMagnitude))
            placeholderLabel.frame.size.height = size.height
            placeholderLabel.frame.size.width = width
            placeholderLabel.frame.origin = CGPoint(x: textContainer.lineFragmentPadding, y: textContainerInset.top)
            
            textStorage.delegate = self
        }
    }
    
}

extension UITextView: NSTextStorageDelegate {
    
    public func textStorage(_ textStorage: NSTextStorage, didProcessEditing editedMask: NSTextStorage.EditActions, range editedRange: NSRange, changeInLength delta: Int) {
        if editedMask.contains(.editedCharacters) {
            placeholderLabel.isHidden = !text.isEmpty
        }
    }
    
}
