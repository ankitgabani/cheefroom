//
//  MoreAboutSessionVC.swift
//  TeeduTutor
//
//  Created by Ankit on 25/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class MoreAboutSessionVC: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtAboutSessio: UITextView!
    
    @IBOutlet weak var txtYoutubeLink: UITextField!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var btnVerifle: UIButton!
    
    var objProgramListArr: ProgramListModl.Payload?
    
    var isFromEdit = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit == true {
            lblTitle.text = "Edit a"
              setUpEdit()
        } else {
            lblTitle.text = "Add a"
        }
        
        setNavigation()
      
        self.hidesBottomBarWhenPushed = true
        // Do any additional setup after loading the view.
    }
    
    func setUpEdit() {
        
        txtAboutSessio.text = objProgramListArr?.descriptions
        txtYoutubeLink.text = objProgramListArr?.instruction_video
    }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgProfile.image = image
                } else {
                    self.imgProfile.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                btnVerifle.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                btnVerifle.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnVrifykle(_ sender: Any) {
    }
    
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
        if txtAboutSessio.text == "" {
            self.view.makeToast("Please enter about session details", duration: 0.3, position: .center)
            return false
        } else if txtYoutubeLink.text == "" {
            self.view.makeToast("Please enter instruction links", duration: 0.3, position: .center)
            return false
        }
        return true
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        if self.isValidatedLogin() {
            
            AppData.sharedInstance.descriptions = txtAboutSessio.text!
            AppData.sharedInstance.instruction_video = txtYoutubeLink.text!
            
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: ThingNeedViewController.self)) as! ThingNeedViewController
            vc.hidesBottomBarWhenPushed = true
            vc.isFromEdit = self.isFromEdit
            vc.objProgramListArr = self.objProgramListArr
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
