//
//  MySessionVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 27/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class MySessionVC: BaseViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgVerified: UIButton!
    @IBOutlet weak var notification: UIButton!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var alertView: UIView!
    
    var homeMonthResponse: HomeMonthViewModl?
    var homeCancelRespnse: CancelViewModl?
    
    var selectedDate: String?
    
    var calender: Calendar?
    var nextDay: Int = 0
    
    var monthsArr = [String]()
    
    var homeCancelID: String?
    var cancelTime: String?
    var cancelDate: String?
    
    var currentDate: Date = Date()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        monthsArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        
        calender = Calendar.current
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigation()
        //self.sessionListApi()
        
        self.alertView.isHidden = true
        
        tblView.delegate = self
        tblView.dataSource = self
        
        self.getMonthApi(param: nil)
        
        setCustomCalender(date: Date())
        
        if self.getData(key: savedKeys.isNotification) == "YES" {
            notification.setImage(#imageLiteral(resourceName: "notificationUnread"), for: .normal)
        } else {
            notification.setImage(#imageLiteral(resourceName: "notificationIcon"), for: .normal)
        }
        
        self.showTabBar()
    }
    
    func setCustomCalender(date: Date) {
        let month = Calendar.current.component(.month, from: date)
        let year = Calendar.current.component(.year, from: date)
        
        let now = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        
        monthLbl.text = "\(nameOfMonth) \(year)"
        
        let param = ["month_value": month, "year_value": year]
        self.getMonthApi(param: param)
    }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgView.image = image
                } else {
                    self.imgView.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                imgVerified.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                imgVerified.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    @IBAction func alertOK(_ sender: UIButton) {
        self.alertView.isHidden = true
        
        if let homeCancelID = homeCancelID, let cancelTime = cancelTime, let cancelDate = cancelDate {
            let param = ["program_id": homeCancelID, "date": cancelDate, "time": cancelTime] as [String : Any]
            cancelApi(param: param)
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.alertView.isHidden = true
    }
    
    @IBAction func previousBtn(_ sender: UIButton) {
        nextDay -= 1
        let selectedPreDate = Calendar.current.date(byAdding: .month, value: nextDay, to: Date())!
        setCustomCalender(date: selectedPreDate)
    }
    
    @IBAction func forwdBtn(_ sender: UIButton) {
        nextDay += 1
        let selectedPreDate = Calendar.current.date(byAdding: .month, value: nextDay, to: Date())!
        setCustomCalender(date: selectedPreDate)
    }
    
    @IBAction func notificationPush(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: NotificationVC.self)) as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MySessionVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeMonthResponse?.payload?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MySessionCell.self), for: indexPath) as! MySessionCell
        self.setShadowinHeader(headershadowView: [cell.prntView])
        cell.setData(data: (self.homeMonthResponse?.payload?[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let getDate = self.stringToDate(dateStr: (self.homeMonthResponse?.payload?[indexPath.row].date)!) {
            let cDate = self.convDate(date: Date())
            if let strDate = self.stringToDate(dateStr: cDate) {
                currentDate = strDate
            }
            if getDate >= currentDate {
                
                self.openActionSheet(id: self.homeMonthResponse?.payload?[indexPath.row].id,
                name: self.homeMonthResponse?.payload?[indexPath.row].payloadClass,
                image: self.homeMonthResponse?.payload?[indexPath.row].image,
                date: self.homeMonthResponse?.payload?[indexPath.row].date,
                title: self.homeMonthResponse?.payload?[indexPath.row].programName,
                oldDate: self.homeMonthResponse?.payload?[indexPath.row].date,
                startTime: self.homeMonthResponse?.payload?[indexPath.row].startFrom)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 10
    }
}


//MARK:- Open Action Sheet
extension MySessionVC {
    
    func openActionSheet(id: String?, name: String?, image: String?, date: String?, title: String?, oldDate: String?, startTime: String?) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Student’s List", style: .default, handler: { (_ ) in
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: StudentListVC.self)) as! StudentListVC
            vc.ID = id
            vc.navTitle = title
            vc.navteacherName = name
            vc.navprogamImage = image
            vc.navDate = date
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Reschedule Session", style: .default, handler: { (_ ) in
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: RescheduleVC.self)) as! RescheduleVC
            vc.ID = id
            vc.oldDate = oldDate
            
            vc.navTitle = title
            vc.navteacherName = name
            vc.navprogamImage = image
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel Session", style: .default, handler: { (_ ) in
            
            self.homeCancelID = id
            self.cancelDate = date
            self.cancelTime = startTime
            self.alertView.isHidden = false
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}


//hit api
extension MySessionVC {
    
    private func getMonthApi(param: [String: Any]?) {
        //self.dispatchGroup.enter()
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.tutorMonthsSessionsdata, param: param, header: header).responseHomeMonthModl { (response) in
            self.hideLoader()
            print("4. First")
            //self.dispatchGroup.leave()
            if var response = response.result.value {
                
                response.payload = response.payload?.sorted(by: { self.stringToDate(dateStr: $0.startFrom ?? "", withFormat: "HH:mm:ss")! < self.stringToDate(dateStr: $1.startFrom ?? "", withFormat: "HH:mm:ss")! })
                
                self.homeMonthResponse = HomeMonthViewModl(homeModel: response)
                if self.homeMonthResponse?.code == 1 {
                    
                    //self.homeMonthResponse?.payload = self.homeMonthResponse?.payload?.sorted(by: { self.stringToDate(dateStr: $0.startFrom ?? "", withFormat: "HH:mm:ss")! < self.stringToDate(dateStr: $1.startFrom ?? "", withFormat: "HH:mm:ss")! })
                    self.tblView.reloadData()
                    
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.homeMonthResponse?.message!)
                    self.tblView.reloadData()
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    private func cancelApi(param: [String: Any]?) {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        //let param = ["program_id": ID!, "date": self.convDate(date: Date(), format: "yyyy-MM-dd")] as [String : Any]
        RestApi.shared.requiredPOST(url: ProjectUrl.CancelProgramSession, param: param, header: header).responseCancelModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.homeCancelRespnse = CancelViewModl(cancel: response)
                if self.homeCancelRespnse?.code == 1 {
                    self.getMonthApi(param: nil)
//                    let action = UIAlertAction(title: "Ok", style: .default) { (_ ) in
//                        self.getWeekApi()
//                        self.getTodayApi()
//                        self.getMonthApi(param: nil)
//                    }
                    //self.showAlertMessage(titleStr: "TeeduTutor", messageStr: self.homeCancelRespnse?.message!, completion: action)
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.homeCancelRespnse?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
