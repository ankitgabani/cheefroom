//
//  MyProgramsVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 26/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class MyProgramsVC: BaseViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var plusBtn: UIButton!
    
    //@IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgVerified: UIButton!
    @IBOutlet weak var notification: UIButton!
    
    var programListArr: ProgramListViewModl?
    
    var arrEpty = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setShadowinHeader(shadowBtn: [plusBtn])
        // Do any additional setup after loading the view.
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigation()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        self.showTabBar()
        
        self.getProgramListApi()
        
        if self.getData(key: savedKeys.isNotification) == "YES" {
            notification.setImage(#imageLiteral(resourceName: "notificationUnread"), for: .normal)
        } else {
            notification.setImage(#imageLiteral(resourceName: "notificationIcon"), for: .normal)
        }
        
        //nameLbl.text = self.getData(key: savedKeys.username) ?? "User"
        
    }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            
            var strImage = imageUrl
            
            strImage = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

            let url = URL(string: strImage)

            print(url)
            
            DispatchQueue.main.async { [weak self] in
                self!.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            }
            
//            RestApi.shared.getImage(url: strImage) { (image) in
//                if let image = image {
//                    self.imgView.image = image
//                } else {
//                    self.imgView.image = #imageLiteral(resourceName: "avatar")
//                }
//            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                imgVerified.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                imgVerified.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    
    @IBAction func notificationPush(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: NotificationVC.self)) as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func plusBtnClicked(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SessionInformationVC.self)) as! SessionInformationVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MyProgramsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.programListArr?.payload?.count == 0
        {
            return 1

        } else {
            return self.programListArr?.payload?.count ?? 0

        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MyProgramCell.self), for: indexPath) as! MyProgramCell
        
        if self.programListArr?.payload?.count == 0
        {
            
            
        } else {
            cell.setData(data: (self.programListArr?.payload?[indexPath.row])!)
        }
        
        self.setShadowinHeader(headershadowView: [cell.prntView])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier:
            String(describing: SessionInformationVC.self)) as! SessionInformationVC
        vc.isFromEdit = true
        vc.objProgramListArr = self.programListArr?.payload?[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
//        let vc = self.storyBoardMain().instantiateViewController(withIdentifier:
//            String(describing: AddProgrameVC.self)) as! AddProgrameVC
//
//        if self.programListArr?.payload?.count == 0
//        {
//
//        } else {
//
//            vc.setEditData = (self.programListArr?.payload?[indexPath.row])!
//            let objprogramListArr = self.programListArr?.payload?[indexPath.row]
//            let strID = objprogramListArr?.id
//            vc.ID = Int(strID!)
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
}

//HIT API
extension MyProgramsVC {
    
    private func getProgramListApi() {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: ProjectUrl.showProgramList, param: nil, header: header).responseProgramListModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.programListArr = ProgramListViewModl(listModl: response)
                if self.programListArr?.code == 1 {
                    
                    print(response)
                    if self.programListArr?.payload?.count == nil || self.programListArr?.payload?.count == 0 {
                       // self.emptyView.isHidden = false
                    } else {
                       // self.emptyView.isHidden = true
                    }
                    self.tblView.reloadData()
                } else {
                    if self.programListArr?.payload?.count == nil || self.programListArr?.payload?.count == 0 {
                       // self.emptyView.isHidden = false
                    } else {
                       // self.emptyView.isHidden = true
                    }
                    //self.showAlertMessage(titleStr: nil, messageStr: self.programListArr?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
