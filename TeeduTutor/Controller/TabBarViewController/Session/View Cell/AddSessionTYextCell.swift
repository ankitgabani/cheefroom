//
//  AddSessionTYextCell.swift
//  TeeduTutor
//
//  Created by Ankit on on 25/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class AddSessionTYextCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewpart1: UIView!
    
    @IBOutlet weak var txtSession: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()

        
        mainView.layer.cornerRadius = 6
        mainView.layer.shadowColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 0.5).cgColor
        mainView.layer.shadowOffset = CGSize(width: 0, height: 3)
        mainView.layer.shadowOpacity = 0.8
        mainView.layer.shadowRadius = 3 //Here your control your blur

        viewpart1.clipsToBounds = true
        viewpart1.layer.cornerRadius = 6
        viewpart1.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
