//
//  TabBarVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 25/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {

//    let unselectedColor = UIColor(red: 252/255, green: 252/255, blue: 252/255, alpha: 1.0)
//    let selectedColor = UIColor(red: 43/255, green: 49/255, blue: 56/255, alpha: 1.0)
    
    let numberOfTabs: CGFloat = 4
    var tabBarHeight: CGFloat!
    
    var sendToReview: Bool?

    override func viewDidLoad() {
        super.viewDidLoad()

//        let numberOfItems = CGFloat(tabBar.items!.count)
//        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height + 40)
        //tabBar.layer.cornerRadius = 10
        //tabBar.selectionIndicatorImage = #imageLiteral(resourceName: "Untitled").resizableImage(withCapInsets: UIEdgeInsets(top: -40, left: 0, bottom: 0, right: 0))

        // remove default border
//        tabBar.frame.size.width = self.view.frame.width + 4
//        tabBar.frame.origin.x = -2
        
        tabBarHeight = self.tabBar.frame.height + 40
        updateSelectionIndicatorImage()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if sendToReview == true {
            sendToReview = nil
            let vc = self.storyboard?.instantiateViewController(withIdentifier: String(describing: ReviewsVC.self)) as! ReviewsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        updateSelectionIndicatorImage()
    }
    

    func updateSelectionIndicatorImage() {
        let width = tabBar.bounds.width
        var selectionImage = #imageLiteral(resourceName: "bottomTabSelected")
        let tabSize = CGSize(width: width/numberOfTabs, height: tabBarHeight)

        UIGraphicsBeginImageContext(tabSize)
        selectionImage.draw(in: CGRect(x: 0, y: 0, width: tabSize.width, height: tabSize.height))
        selectionImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        tabBar.selectionIndicatorImage = selectionImage
    }

}

extension UIImage {

   class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
    
    
    let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    color.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return image
   }
}
