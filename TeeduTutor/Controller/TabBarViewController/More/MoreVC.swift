//
//  MoreVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 27/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import MessageUI
import StoreKit

class MoreVC: BaseViewController {
    
    @IBOutlet weak var aletrView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewParentView: UIView!
    @IBOutlet weak var tblViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgVerified: UIButton!
    @IBOutlet weak var userName: UILabel!
    
    var reviewResponse: ReviewViewModl?
    var profileArr: ProfileViewModl?
    
    var arr = [TutorialModl]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self

        arr.append(TutorialModl(name: "Reviews", image: #imageLiteral(resourceName: "Path 3575")))
        arr.append(TutorialModl(name: "KYC Status", image: #imageLiteral(resourceName: "file")))
        arr.append(TutorialModl(name: "Support", image: #imageLiteral(resourceName: "supportiocn")))
        arr.append(TutorialModl(name: "Terms & Conditions", image: #imageLiteral(resourceName: "calendar")))
        arr.append(TutorialModl(name: "Frequently Asked Questions", image: #imageLiteral(resourceName: "Frequently")))
        arr.append(TutorialModl(name: "Privacy Policy", image: #imageLiteral(resourceName: "privacyicon")))
        arr.append(TutorialModl(name: "Invite Friends", image: #imageLiteral(resourceName: "invitefriend")))
        arr.append(TutorialModl(name: "Rate Our App", image: #imageLiteral(resourceName: "Rateourapp")))
        arr.append(TutorialModl(name: "Logout", image: #imageLiteral(resourceName: "logout")))
        
        self.setShadowinHeader(headershadowView: [tblViewParentView])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.aletrView.isHidden = true
        DispatchQueue.main.async {
           // self.tblViewHeight.constant = 490
            self.tblView.layoutIfNeeded()
        }
        
        self.reviewListApi()
        self.getProfileApi()
        self.setNavigation()

     }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        self.showTabBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgView.image = image
                } else {
                    self.imgView.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        
        if let username = self.getData(key: savedKeys.username) {
            userName.text = username
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                imgVerified.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                imgVerified.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    @IBAction func goToProfile(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: ProfileVC.self)) as! ProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func yesLogout(_ sender: Any) {
        self.removeData(key: savedKeys.username)
        self.removeData(key: savedKeys.mobile)
        //self.removeData(key: savedKeys.device_token)
        self.removeData(key: savedKeys.api_token)
        self.removeData(key: savedKeys.userID)
        self.removeData(key: savedKeys.profile_image)
        self.removeData(key: savedKeys.email)
        self.removeData(key: savedKeys.status)
        //            self.removeData(key: savedKeys.lat)
        //            self.removeData(key: savedKeys.long)
        
        
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
         self.aletrView.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func noLogiout(_ sender: Any) {
         self.aletrView.isHidden = true
    }
    
}

extension MoreVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MoreCell.self), for: indexPath) as! MoreCell
        
        if arr[indexPath.row].name == "Reviews" {
            if reviewResponse?.payload?.count == 0 || reviewResponse?.payload?.count == nil {
                cell.notifyView.isHidden = true
            } else {
                cell.reviewCount.text = "\(reviewResponse?.payload?.count ?? 0)"
                cell.notifyView.isHidden = false
            }
        } else {
            cell.notifyView.isHidden = true
        }
        cell.img.image = arr[indexPath.row].image
        
        cell.img.image = cell.img.image?.withRenderingMode(.alwaysTemplate)
        cell.img.tintColor = UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1)

        cell.name.text = arr[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arr[indexPath.row].name == "Reviews" {
            
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: ReviewsVC.self)) as! ReviewsVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if arr[indexPath.row].name == "KYC Status" {
            
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: DocsUploadVC.self)) as! DocsUploadVC
            vc.fromMore = true
            self.navigationController?.pushViewController(vc, animated: true)
            self.showNavigationBar(title: nil, setTransparent: true)
            
            
        } else if arr[indexPath.row].name == "Support" {
            
            openActionSheet()
            
        } else if arr[indexPath.row].name == "Terms & Conditions" {
            
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: AboutAppVC.self)) as! AboutAppVC
            vc.whatPage = "T&C"
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if arr[indexPath.row].name == "Frequently Asked Questions" {
            
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: AboutAppVC.self)) as! AboutAppVC
            vc.whatPage = "FAQ"
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if arr[indexPath.row].name == "Privacy Policy" {
            
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: AboutAppVC.self)) as! AboutAppVC
            vc.whatPage = "Privacy"
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if arr[indexPath.row].name == "Invite Friends" {
            
            let activityVC = UIActivityViewController(activityItems: ["http://teedu.in/"], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            present(activityVC, animated: true, completion: nil)
            activityVC.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in

                if completed  {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        } else if arr[indexPath.row].name == "Rate Our App" {
            AppSupport.rateApp()
        } else if arr[indexPath.row].name == "Logout" {
            
            self.aletrView.isHidden = false
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
   
}

extension MoreVC {
    private func reviewListApi() {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: ProjectUrl.tutorFeedbacks, param: nil, header: header).responseReviewModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.reviewResponse = ReviewViewModl(viewModl: response)
                self.tblView.reloadData()
            }  else {
                //self.showAlertMessage(titleStr: nil, messageStr: nil)
                self.tblView.reloadData()
            }
        }
    }
    
    private func getProfileApi() {
        //self.dispatchGroup.enter()
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        
        print(header)
        
         print(ProjectUrl.tutorShowProfile)
        RestApi.shared.requiredGET(url: ProjectUrl.tutorShowProfile, param: nil, header: header).responseProfileModl { (response) in
            self.hideLoader()
            print("2. First")
            //self.dispatchGroup.leave()
            if let response = response.result.value {
                self.profileArr = ProfileViewModl(viewModl: response)
                if self.profileArr?.code == 1 {
                    
                    if self.profileArr?.payload?.kycStatus?.isEmpty == true {
                        //self.kycView.isHidden = false
                    } else {
                        //self.kycView.isHidden = true
                        
                        self.saveData(value: self.profileArr!.payload!.kycStatus, key: savedKeys.status)
                    }
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.profileArr?.message!)
                }
            }  else {
                print(response)
                self.showAlertMessage(titleStr: nil, messageStr: nil)
            }
        }
    }
}

extension MoreVC:  MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController!, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func openGallery()
    {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = ["9899968888"]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    private func openCamera(){
        let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=919899968888&text=Hi")
        if UIApplication.shared.canOpenURL(whatsappURL!) {
            UIApplication.shared.open(whatsappURL!, options: [:], completionHandler: nil)
        }
    }
    
    private func openActionSheet() {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: UIFont(name: "Quicksand-Regular", size: 18)!, NSAttributedString.Key.foregroundColor: UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1)]
        let titleString = NSAttributedString(string: "CONTACT US", attributes: titleAttributes)
        let messageAttributes = [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 16)!, NSAttributedString.Key.foregroundColor: UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1)]
        let messageString = NSAttributedString(string: "Please select a option to contact us", attributes: messageAttributes)
        alert.setValue(titleString, forKey: "attributedTitle")
        alert.setValue(messageString, forKey: "attributedMessage")
        let labelAction = UIAlertAction(title: "SMS", style: .default, handler: nil)
        labelAction.setValue(UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1), forKey: "titleTextColor")
        let deleteAction = UIAlertAction(title: "WhatsApp", style: .default, handler: nil)
        deleteAction.setValue(UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1), forKey: "titleTextColor")

        let deleteAction1 = UIAlertAction(title: "Email", style: .default, handler: nil)
        deleteAction1.setValue(UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1), forKey: "titleTextColor")

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        cancelAction.setValue(UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1), forKey: "titleTextColor")

        alert.addAction(labelAction)
        alert.addAction(deleteAction)
        alert.addAction(deleteAction1)
        alert.addAction(cancelAction)
        
        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor.white

//
//        let alert = UIAlertController(title: "CONTACT US", message: "Please select a option to contact us", preferredStyle: .actionSheet)
//
//        alert.addAction(UIAlertAction(title: "SMS", style: .default , handler:{ (UIAlertAction)in
//            //self.openGallery()
//        }))
//        alert.addAction(UIAlertAction(title: "WhatsApp", style: .default , handler:{ (UIAlertAction)in
//            //self.openCamera()
//        }))
//        alert.addAction(UIAlertAction(title: "Email", style: .default , handler:{ (UIAlertAction)in
//                   //self.openCamera()
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
//        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension UILabel {

    override open func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)

        guard newSuperview != nil else {
            if textColor == UIColor.darkText {
                textColor = UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1)
            }
            return
        }

        if #available(iOS 13.0, *) {
            if textColor == UIColor.label {
                textColor = UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1)
            }
           
        } else if textColor == UIColor.darkText {
            textColor = UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1)
        }
    }
}

struct AppSupport {
    
    static let appID = "1483184959"
    
    static func rateApp(){
         if #available(iOS 10.3, *) {
               SKStoreReviewController.requestReview()

           } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + appID) {
               if #available(iOS 10, *) {
                   UIApplication.shared.open(url, options: [:], completionHandler: nil)

               } else {
                   UIApplication.shared.openURL(url)
               }
           }
    }
    
    static func openURL(_ url: URL){
        if UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
