//
//  MyBookingsVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 27/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class MyBookingsVC: BaseViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgVerified: UIButton!
    @IBOutlet weak var notification: UIButton!
    
    var bookingListArr: BookingslistViewModl?
    var bookingCancelArr: CancelViewModl?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigation()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        self.showTabBar()
        
        
        getBookingListApi()
        notificationStatusApi()
        
        if self.getData(key: savedKeys.isNotification) == "YES" {
            notification.setImage(#imageLiteral(resourceName: "notificationUnread"), for: .normal)
        } else {
            notification.setImage(#imageLiteral(resourceName: "notificationIcon"), for: .normal)
        }
    }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgView.image = image
                } else {
                    self.imgView.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                imgVerified.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                imgVerified.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }

    @IBAction func notificationPush(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: NotificationVC.self)) as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MyBookingsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookingListArr?.payload?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MyBookingCell.self), for: indexPath) as! MyBookingCell
        self.setShadowinHeader(headershadowView: [cell.prntView])
        cell.delegate = self
        cell.setData(data: (self.bookingListArr?.payload?[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}


//HIT API
extension MyBookingsVC {
    
    private func getBookingListApi() {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: ProjectUrl.tutorBookingslist, param: nil, header: header).responseBookingslistModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.bookingListArr = BookingslistViewModl(viewModl: response)
                if self.bookingListArr?.code == 1 {
                    
                    self.tblView.reloadData()
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.bookingListArr?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    
    private func notificationStatusApi() {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.notificationStatus, param: ["is_read": 1, "category": "Booking"], header: header).responseJSON(completionHandler: { (response) in
            self.hideLoader()
            if let response = response.result.value as? [String: Any] {
                if response["code"] as? Int != 1 {
                    print("notification not read")
                }
            }
        })
    }
    
    private func cancelApi(param: [String: Any]) {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.cancelBookingByTutor, param: param, header: header).responseCancelModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.bookingCancelArr = CancelViewModl(cancel: response)
                if self.bookingCancelArr?.code == 1 {
                    let action = UIAlertAction(title: "Ok", style: .default) { (_ ) in
                        self.getBookingListApi()
                    }
                    self.showAlertMessage(titleStr: nil, messageStr: self.bookingCancelArr?.message!, completion: action)
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.bookingCancelArr?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    private func successApi(param: [String: Any]) {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.acceptedBookingByTutor, param: param, header: header).responseCancelModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.bookingCancelArr = CancelViewModl(cancel: response)
                if self.bookingCancelArr?.code == 1 {
                    let action = UIAlertAction(title: "Ok", style: .default) { (_ ) in
                        self.getBookingListApi()
                    }
                    self.showAlertMessage(titleStr: nil, messageStr: self.bookingCancelArr?.message!, completion: action)
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.bookingCancelArr?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}

extension MyBookingsVC: BookingActions {
    func getBooking(id: Int?, cancel: Bool?) {
        
        if cancel == true {
            if let id = id {
                self.cancelApi(param: ["booking_id": id])
            }
        } else {
            if let id = id {
                self.successApi(param: ["booking_id": id])
            }
        }
    }
}
