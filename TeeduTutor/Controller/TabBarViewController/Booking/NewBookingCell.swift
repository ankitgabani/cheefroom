//
//  NewBookingCell.swift
//  TeeduTutor
//
//  Created by Ankit on 27/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class NewBookingCell: UITableViewCell {

    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblMoney: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblBookingID: UILabel!
    
    @IBOutlet weak var lblBooked: UILabel!
    
    @IBOutlet weak var lblStrating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
