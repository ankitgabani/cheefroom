//
//  NewBookingListVC.swift
//  TeeduTutor
//
//  Created by Ankit on 27/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class NewBookingListVC: BaseViewController {
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var btnverify: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var upcomingView: UIView!
    @IBOutlet weak var viewPast: UIView!
    
    @IBOutlet weak var btnUp: UIButton!
    @IBOutlet weak var btnPast: UIButton!
    @IBOutlet weak var lblNodataEdit: UILabel!
    
    var arrUpcomingList: [TTBokking] = [TTBokking]()
    var arrPastList: [TTBokking] = [TTBokking]()
    
    var isUpcomeingBook = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewNoData.isHidden = true
        
        tblView.delegate = self
        tblView.dataSource = self
        
        upcomingView.layer.cornerRadius = 5
        upcomingView.clipsToBounds = true
        upcomingView.backgroundColor = UIColor.black
        btnUp.setTitleColor(UIColor.white, for: .normal)
        
        
        viewPast.backgroundColor = UIColor(red: 248/255, green: 247/255, blue: 248/25, alpha: 1)
        btnPast.setTitleColor(UIColor.black, for: .normal)
        viewPast.layer.cornerRadius = 0
        viewPast.clipsToBounds = true
      
        getBookingListApi()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
          setNavigation()
    }
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgProfile.image = image
                } else {
                    self.imgProfile.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                btnverify.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                btnverify.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    @IBAction func btnNewViryfr(_ sender: Any) {
    }
    
    @IBAction func btnUpActio(_ sender: Any) {
        
        
        if arrUpcomingList.count == 0 {
            viewNoData.isHidden = false
            tblView.isHidden = true
        } else {
            viewNoData.isHidden = false
            tblView.isHidden = true
        }
        
        upcomingView.layer.cornerRadius = 5
        upcomingView.clipsToBounds = true
        upcomingView.backgroundColor = UIColor.black
        btnUp.setTitleColor(UIColor.white, for: .normal)
        
        viewPast.backgroundColor = UIColor(red: 248/255, green: 247/255, blue: 248/25, alpha: 1)
        btnPast.setTitleColor(UIColor.black, for: .normal)
        viewPast.layer.cornerRadius = 0
        viewPast.clipsToBounds = true
        
        isUpcomeingBook = true
        self.tblView.reloadData()
        
    }
    @IBAction func btnAddSesion(_ sender: Any) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SessionInformationVC.self)) as! SessionInformationVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPassaCtion(_ sender: Any) {
        
        if arrPastList.count == 0 {
            viewNoData.isHidden = false
            tblView.isHidden = true
            
        } else {
            viewNoData.isHidden = false
            tblView.isHidden = true
            
        }
        viewPast.layer.cornerRadius = 5
        viewPast.clipsToBounds = true
        viewPast.backgroundColor = UIColor.black
        btnPast.setTitleColor(UIColor.white, for: .normal)
        
        upcomingView.backgroundColor = UIColor(red: 248/255, green: 247/255, blue: 248/25, alpha: 1)
        btnUp.setTitleColor(UIColor.black, for: .normal)
        upcomingView.layer.cornerRadius = 0
        upcomingView.clipsToBounds = true
        
        isUpcomeingBook = false
        self.tblView.reloadData()
        
    }
    
    //MARK:- API call
    
    func getBookingListApi() {

        let param = ["": ""]
        self.showLoader()
        print(param)
        APIClient.sharedInstance.MakeAPICallWihAuthHeaderTokenQuery(self.getData(key: savedKeys.api_token)!, url: "past_upcoming_tutorbookingslist", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    self.hideLoader()
                    
                    if let responseUser = response {
                        
                        print(responseUser)
                        let dicResponse = responseUser.value(forKey: "payload") as? NSDictionary
                        
                        let arrUpcoming = dicResponse?.value(forKey: "upcoming") as? NSArray
                        let arrPast = dicResponse?.value(forKey: "past") as? NSArray
                        
                        for data in arrUpcoming!{
                            let list = TTBokking(TTBokkingListDictionary: data as? NSDictionary)
                            self.arrUpcomingList.append(list)
                        }
                        
                        for data in arrPast!{
                            let list = TTBokking(TTBokkingListDictionary: data as? NSDictionary)
                            self.arrPastList.append(list)                            
                        }
                        
                        if self.arrUpcomingList.count == 0 {
                            self.viewNoData.isHidden = false
                            self.tblView.isHidden = true
                        } else {
                            self.viewNoData.isHidden = false
                            self.tblView.isHidden = true
                        }
                        
                        self.tblView.reloadData()
                    }
                } else {
                    
                    self.hideLoader()
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                self.hideLoader()
            }
        })
    }
}


extension NewBookingListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isUpcomeingBook == true {
            
            return arrUpcomingList.count
            
        } else {
            return arrPastList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewBookingCell") as! NewBookingCell
        self.setShadowinHeader(headershadowView: [cell.mainView])
        
        if isUpcomeingBook == true {
            
            let objBookingList = self.arrUpcomingList[indexPath.row]
            
            let full_name = objBookingList.full_name ?? ""
            let booking_id = objBookingList.booking_id ?? ""
            let no_of_session = objBookingList.no_of_session ?? ""
            let start_from = objBookingList.start_session ?? ""
            let profile_image = objBookingList.profile_image
            
            cell.lblName.text = full_name
            
            cell.lblBookingID.attributedText =
                NSMutableAttributedString()
                    .normal("Booking ID ")
                    .bold(booking_id)
            
            cell.lblBooked.attributedText =
                NSMutableAttributedString()
                    .normal("Booked ")
                    .bold(no_of_session)
                    .bold(" Sessions")
            
            var dateFromString = ""
            if let createDateStr = objBookingList.start_session {
                let arrSplit1 = createDateStr.components(separatedBy: "-")
                let strYear = arrSplit1[0] as! String
                let strMonth = arrSplit1[1] as! String
                let strDay = arrSplit1[2] as! String
                
                let monthNumber = (Int)(strMonth)
                
                let fmt = DateFormatter()
                fmt.dateStyle = .medium
                let strMonthName = fmt.monthSymbols[monthNumber! - 1]
                
                dateFromString = String.init(format: "%@ %@", strDay,strMonthName.prefix(3) as CVarArg)
            }
            
            cell.lblStrating.attributedText =
                NSMutableAttributedString()
                    .normal("Starting from ")
                    .bold(dateFromString)
            
            cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height / 2
            cell.imgProfile.clipsToBounds = true
            
            if let imageName = profile_image {
                RestApi.shared.getImage(url: BASE_URL+imageName) { (image) in
                    cell.imgProfile.image = image
                }
            } else {
                cell.imgProfile.image = UIImage(named: "avatar")
            }
            
        } else {
            
            let objBookingList = self.arrPastList[indexPath.row]
            let full_name = objBookingList.full_name ?? ""
            let booking_id = objBookingList.booking_id ?? ""
            let no_of_session = objBookingList.no_of_session ?? ""
            let start_from = objBookingList.start_session ?? ""
            let profile_image = objBookingList.profile_image
            
            cell.lblName.text = full_name
            
            cell.lblBookingID.attributedText =
                NSMutableAttributedString()
                    .normal("Booking ID ")
                    .bold(booking_id)
            
            cell.lblBooked.attributedText =
                NSMutableAttributedString()
                    .normal("Booked ")
                    .bold(no_of_session)
                    .bold(" Sessions")
            
            var dateFromString = ""
            if let createDateStr = objBookingList.start_session {
                let arrSplit1 = createDateStr.components(separatedBy: "-")
                let strYear = arrSplit1[0] as! String
                let strMonth = arrSplit1[1] as! String
                let strDay = arrSplit1[2] as! String
                
                let monthNumber = (Int)(strMonth)
                
                let fmt = DateFormatter()
                fmt.dateStyle = .medium
                let strMonthName = fmt.monthSymbols[monthNumber! - 1]
                
                dateFromString = String.init(format: "%@ %@", strDay,strMonthName.prefix(3) as CVarArg)
            }
            
            cell.lblStrating.attributedText =
                NSMutableAttributedString()
                    .normal("Starting from ")
                    .bold(dateFromString)
            
            cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height / 2
            cell.imgProfile.clipsToBounds = true
            
            if let imageName = profile_image {
                RestApi.shared.getImage(url: BASE_URL+imageName) { (image) in
                    cell.imgProfile.image = image
                }
            } else {
                cell.imgProfile.image = UIImage(named: "avatar")
            }
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 138
    }
    
    
}
extension NSMutableAttributedString {
    var fontSize:CGFloat { return 12 }
    var boldFont:UIFont { return UIFont(name: "Quicksand-Bold", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize) }
    var normalFont:UIFont { return UIFont(name: "Quicksand-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    /* Other styling methods */
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func blackHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}
