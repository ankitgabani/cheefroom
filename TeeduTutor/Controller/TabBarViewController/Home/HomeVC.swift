//
//  HomeVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 25/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import FSCalendar

class HomeVC: BaseViewController {
    
    @IBOutlet weak var nowBtn: UIButton!
    
    @IBOutlet weak var lineChartView: LineChartView!
    
    @IBOutlet weak var laterBtn: UIButton!
    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var topSessionColctnView: UICollectionView!
    @IBOutlet weak var weekTblView: UITableView!
    @IBOutlet weak var weekBtn: UIButton!
    @IBOutlet weak var weekView: UIView!
    @IBOutlet weak var monthBtn: UIButton!
    @IBOutlet weak var monthView: UIView!
    
    @IBOutlet weak var todaySesEmptyView: UIView!
    
    @IBOutlet weak var weekEmptyView: UIView!
    @IBOutlet weak var weekEmptyLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgVerified: UIButton!
    @IBOutlet weak var notification: UIButton!
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var kycView: UIView!
    
    var homeWeekResponse: HomeWeekViewModl?
    var homeTodayResponse: HomeTodayViewModl?
    var homeCancelRespnse: CancelViewModl?
    var homeMonthResponse: HomeMonthViewModl?
    
    var homeCancelID: String?
    var cancelTime: String?
    var cancelDate: String?
    
    var profileArr: ProfileViewModl?
    
    //var dispatchGroup = DispatchGroup()
    
    var somedays : Array = [String]()
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    fileprivate lazy var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter
    }()
    var notificationArr: NotificationViewModl?
    let months = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"]
    let unitsSold = [0,0,0,0,0,0, 500.0,1500.0]
    
    var arrMonthsList = NSArray()
    var arrUnitsSoldList = NSArray()

    weak var axisFormatDelegate: IAxisValueFormatter?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        laterBtn.layer.cornerRadius=10
         nowBtn.layer.cornerRadius=10
        self.calender.delegate = self
        self.calender.dataSource = self
        self.calender.today = nil
        topSessionColctnView.delegate = self
        topSessionColctnView.dataSource = self
        
        weekTblView.delegate = self
        weekTblView.dataSource = self
        
        weekView.isHidden = false
        monthView.isHidden = true
        
        print(self.getData(key: savedKeys.api_token))
                        
//        calender.scrollDirection = .horizontal
        
        self.setChart(dataPoints: months, values: unitsSold)
        
    }
  
    func setChart(dataPoints: [String], values: [Double]) {

        var dataEntries : [ChartDataEntry] = [ChartDataEntry]()

        
        for i in 0 ..< dataPoints.count {
            dataEntries.append(ChartDataEntry(x: Double(i), y: values[i]))
        }

        let lineChartDataSet = LineChartDataSet(entries: dataEntries, label: "")
        lineChartDataSet.axisDependency = .left
        lineChartDataSet.setColor(UIColor.black)
        lineChartDataSet.setCircleColor(UIColor.black) // our circle will be dark red
        lineChartDataSet.lineWidth = 1.0
        lineChartDataSet.circleRadius = 3.0 // the radius of the node circle
        lineChartDataSet.fillAlpha = 1
        lineChartDataSet.fillColor = UIColor.black
        lineChartDataSet.highlightColor = UIColor.white
        lineChartDataSet.drawCircleHoleEnabled = true

        var dataSets = [LineChartDataSet]()
        dataSets.append(lineChartDataSet)


        let lineChartData = LineChartData(dataSets: dataSets)
        lineChartView.data = lineChartData
        lineChartView.rightAxis.enabled = false
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.setLabelCount(dataPoints.count, force: true)
        lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: dataPoints)
        lineChartView.legend.enabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getGraphApi()
        weekEmptyView.isHidden = true
        weekView.isHidden = true

        
        self.alertView.isHidden = true
        self.kycView.isHidden = true
      //  self.weekEmptyView.isHidden = true
        
        self.todaySesEmptyView.isHidden = true
        
        self.setNavigation()
        self.showTabBar()
        
        
        
       // self.getNotificaitonApi()
        self.getProfileApi()
        self.getWeekApi()
        self.getMonthApi()
       // self.getTodayApi()
        
//        dispatchGroup.notify(queue: .main) {
//            print("hit all api with sequence")
//        }
        
        
    }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        if let username = self.getData(key: savedKeys.username) {
            nameLbl.text = username.components(separatedBy: " ")[0]
        } else {
            nameLbl.text = "User"
        }
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgView.image = image
                } else {
                    self.imgView.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                imgVerified.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                imgVerified.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    @IBAction func weekMonthToggle(_ sender: UIButton) {
        
        if sender == weekBtn {
//            weekBtn.setTitleColor(ProjectColor.darkGreen, for: .normal)
//            monthBtn.setTitleColor(UIColor.black, for: .normal)
//            weekBtn.setImage(#imageLiteral(resourceName: "week_view_active"), for: .normal)
//            monthBtn.setImage(#imageLiteral(resourceName: "month_view_inactive"), for: .normal)
            
            if self.homeWeekResponse?.payload?.count == 0 {
                weekEmptyView.isHidden = false
                weekView.isHidden = true
                
            } else {
                weekEmptyView.isHidden = true
                weekView.isHidden = false
            }

            monthView.isHidden = true
            weekBtn.alpha = 1
            monthBtn.alpha = 0.5

            
        } else if sender == monthBtn {
//            weekBtn.setTitleColor(UIColor.black, for: .normal)
//            monthBtn.setTitleColor(ProjectColor.darkGreen, for: .normal)
//            weekBtn.setImage(#imageLiteral(resourceName: "week_view_inactive"), for: .normal)
//            monthBtn.setImage(#imageLiteral(resourceName: "month_view_active"), for: .normal)
            
            weekEmptyView.isHidden = true
            
            weekBtn.alpha = 0.5
            monthBtn.alpha = 1

            weekView.isHidden = true
            monthView.isHidden = false
        }
        
    }
    
    @IBAction func alertOK(_ sender: UIButton) {
        self.alertView.isHidden = true
        
        if let homeCancelID = homeCancelID, let cancelTime = cancelTime, let cancelDate = cancelDate {
            let param = ["program_id": homeCancelID, "date": cancelDate, "time": cancelTime] as [String : Any]
            cancelApi(param: param)
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.alertView.isHidden = true
    }
    
    @IBAction func notificationPush(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: NotificationVC.self)) as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }


    @IBAction func kycOkay(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: DocsUploadVC.self)) as! DocsUploadVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func kycCancel(_ sender: UIButton) {
        self.kycView.isHidden = true
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeWeekResponse?.payload?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HomeWeekViewTblCell.self), for: indexPath) as! HomeWeekViewTblCell
        self.setShadowinHeader(headershadowView: [cell.prntView])
        cell.setData(data: (self.homeWeekResponse?.payload?[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openActionSheet(id: self.homeWeekResponse?.payload?[indexPath.row].id,
                             name: self.homeWeekResponse?.payload?[indexPath.row].payloadClass,
                             image: self.homeWeekResponse?.payload?[indexPath.row].image,
                             date: self.homeWeekResponse?.payload?[indexPath.row].date,
                             title: self.homeWeekResponse?.payload?[indexPath.row].programName,
                             oldDate: self.homeWeekResponse?.payload?[indexPath.row].date,
                             startTime: self.homeWeekResponse?.payload?[indexPath.row].startFrom)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension HomeVC: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        //let dateString : String = dateFormatter1.string(from:date)

        //print(somedays)
        if self.somedays.contains(self.convDate(date: date))
        {
            return UIColor.white
        }
        else{
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        
        //let dateString : String = dateFormatter1.string(from:date)
        
        //print(somedays)
        if self.somedays.contains(self.convDate(date: date))
        {
            return UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1)
        }
        else{
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return false
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.homeTodayResponse?.payload?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: HomeTodaySectionCell.self), for: indexPath) as! HomeTodaySectionCell
        self.setShadowinHeader(headershadowView: [cell.prntView])
        cell.setData(data: (self.homeTodayResponse?.payload?[indexPath.row])!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.openActionSheet(id: self.homeTodayResponse?.payload?[indexPath.row].id,
                             name: self.homeTodayResponse?.payload?[indexPath.row].payloadClass,
                             image: self.homeTodayResponse?.payload?[indexPath.row].image,
                             date: self.homeTodayResponse?.payload?[indexPath.row].date,
                             title: self.homeTodayResponse?.payload?[indexPath.row].programName,
                             oldDate: self.homeTodayResponse?.payload?[indexPath.row].date,
                             startTime: self.homeTodayResponse?.payload?[indexPath.row].startFrom)
    }
}


//MARK:- Open Action Sheet
extension HomeVC {
    
    func openActionSheet(id: String?, name: String?, image: String?, date: String?, title: String?, oldDate: String?, startTime: String?) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Student’s List", style: .default, handler: { (_ ) in
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: StudentListVC.self)) as! StudentListVC
            vc.ID = id
            vc.navTitle = title
            vc.navteacherName = name
            vc.navprogamImage = image
            vc.navDate = date
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Reschedule Session", style: .default, handler: { (_ ) in
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: RescheduleVC.self)) as! RescheduleVC
            vc.ID = id
            vc.oldDate = oldDate
            
            vc.navTitle = title
            vc.navteacherName = name
            vc.navprogamImage = image
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel Session", style: .default, handler: { (_ ) in
            
            self.homeCancelID = id
            self.cancelDate = date
            self.cancelTime = startTime
            self.alertView.isHidden = false
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}


//HIT API
extension HomeVC {
    
    func getGraphApi() {
        
        let param = ["": ""]
        self.showLoader()
        print(param)
        APIClient.sharedInstance.MakeAPICallWihAuthHeaderTokenQuery(self.getData(key: savedKeys.api_token)!, url: "get_graph", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    self.hideLoader()
                    
                    if let responseUser = response {
                        
                        print(responseUser)
                        let dicResponse = responseUser.value(forKey: "payload") as? NSDictionary
                        
//                        self.arrMonthsList = (responseUser.value(forKey: "months") as? NSArray)!
//                        self.arrUnitsSoldList = (responseUser.value(forKey: "earnings") as? NSArray)!
                                                
                        print(self.arrMonthsList)
                        print(self.arrUnitsSoldList)
                        
                    }
                } else {
                    
                    self.hideLoader()
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                self.hideLoader()
            }
        })
    }
    
    private func getNotificaitonApi() {
        //self.dispatchGroup.enter()
      //  self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: ProjectUrl.Notifications, param: nil, header: header).responseNotificationModl { (response) in
            self.hideLoader()
            print("1. First")
            //self.dispatchGroup.leave()
            if let response = response.result.value {
                self.notificationArr = NotificationViewModl(viewModl: response)
                if self.notificationArr?.code == 1 {
                    self.notificationArr?.payload?.contains(where: { (payload) -> Bool in
                        if payload.isRead == "0" {
                            self.saveData(value: "YES", key: savedKeys.isNotification)
                            return true
                        }
                        return false
                    })
                    if self.getData(key: savedKeys.isNotification) == "YES" {
                        self.notification.setImage(#imageLiteral(resourceName: "notificationUnread"), for: .normal)
                    } else {
                        self.notification.setImage(#imageLiteral(resourceName: "notificationIcon"), for: .normal)
                    }
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.notificationArr?.message!)
                }
            }  else {
                print(response)
                self.showAlertMessage(titleStr: nil, messageStr: nil)
            }
        }
    }
    
    private func getProfileApi() {
        //self.dispatchGroup.enter()
       // self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: ProjectUrl.tutorShowProfile, param: nil, header: header).responseProfileModl { (response) in
            self.hideLoader()
            print("2. First")
            //self.dispatchGroup.leave()
            if let response = response.result.value {
                self.profileArr = ProfileViewModl(viewModl: response)
                if self.profileArr?.code == 1 {
                    
                    if self.profileArr?.payload?.kycStatus?.isEmpty == true {
                        self.kycView.isHidden = false
                    } else {
                        self.kycView.isHidden = true
                        
                        self.saveData(value: self.profileArr!.payload!.kycStatus, key: savedKeys.status)
                    }
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.profileArr?.message!)
                }
            }  else {
                print(response)
                self.showAlertMessage(titleStr: nil, messageStr: nil)
            }
        }
    }
    
    private func getWeekApi() {
        //self.dispatchGroup.enter()
       // self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.tutorWeekSessionsData, param: nil, header: header).responseHomeWeekModl { (response) in
            self.hideLoader()
            print("3. First")
            //self.dispatchGroup.leave()
            if var response = response.result.value {
                
                response.payload = response.payload?.sorted(by: { self.stringToDate(dateStr: $0.date ?? "")! > self.stringToDate(dateStr: $1.date ?? "")! })
                
                self.homeWeekResponse = HomeWeekViewModl(homeModel: response)
                
                if self.homeWeekResponse?.code == 1 {
                    
                    if self.homeWeekResponse?.payload?.count == nil || self.homeWeekResponse?.payload?.count == 0 {
                        self.weekEmptyView.isHidden = false
                    } else {
                        self.weekEmptyView.isHidden = true
                        self.weekView.isHidden = false
                        self.monthView.isHidden = true
                    }
                    self.weekTblView.reloadData()
                } else {
                    if self.homeWeekResponse?.payload?.count == nil || self.homeWeekResponse?.payload?.count == 0 {
                        self.weekEmptyView.isHidden = false
                    } else {
                        self.weekEmptyView.isHidden = true
                    }
                    //self.showAlertMessage(titleStr: nil, messageStr: self.homeWeekResponse?.message!)
                }
                
                if self.homeWeekResponse?.payload?.count == 0 {
                    self.weekEmptyView.isHidden = false
                    self.weekView.isHidden = true
                    
                } else {
                    self.weekEmptyView.isHidden = true
                    self.weekView.isHidden = false
                }
                
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    private func getMonthApi() {
        //self.dispatchGroup.enter()
       // self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.tutorMonthsSessionsdata, param: nil, header: header).responseHomeMonthModl { (response) in
            self.hideLoader()
            print("4. First")
            //self.dispatchGroup.leave()
            if let response = response.result.value {
                self.homeMonthResponse = HomeMonthViewModl(homeModel: response)
                if self.homeMonthResponse?.code == 1 {
                    
                    if self.homeMonthResponse?.payload != nil {
                        for i in self.homeMonthResponse!.payload! {
                            
                            self.somedays.append(i.startingFrom!)
                                                        
                        }
                    }
                    
                    self.calender.reloadData()
                    
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.homeMonthResponse?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    private func getTodayApi() {
        //self.dispatchGroup.enter()
     //   self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.tutorTodaysSessionsdata, param: nil, header: header).responseHomeTodayModl { (response) in
            self.hideLoader()
            print("5. First")
            //self.dispatchGroup.leave()
            if var response = response.result.value {
                
                response.payload = response.payload?.sorted(by: { self.stringToDate(dateStr: $0.date ?? "")! > self.stringToDate(dateStr: $1.date ?? "")! })
                
                self.homeTodayResponse = HomeTodayViewModl(homeModel: response)
                if self.homeTodayResponse?.code == 1 {
                    
                    if self.homeTodayResponse?.payload?.count == nil || self.homeTodayResponse?.payload?.count == 0 {
                        self.todaySesEmptyView.isHidden = false
                    } else {
                        self.todaySesEmptyView.isHidden = true
                    }
                    self.topSessionColctnView.reloadData()
                } else {
                    if self.homeTodayResponse?.payload?.count == nil || self.homeTodayResponse?.payload?.count == 0 {
                        self.todaySesEmptyView.isHidden = false
                    } else {
                        self.todaySesEmptyView.isHidden = true
                    }
                    //self.showAlertMessage(titleStr: nil, messageStr: self.homeTodayResponse?.message!)
                }
            }  else {

                // self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    private func cancelApi(param: [String: Any]?) {
       // self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        //let param = ["program_id": ID!, "date": self.convDate(date: Date(), format: "yyyy-MM-dd")] as [String : Any]
        RestApi.shared.requiredPOST(url: ProjectUrl.CancelProgramSession, param: param, header: header).responseCancelModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.homeCancelRespnse = CancelViewModl(cancel: response)
                if self.homeCancelRespnse?.code == 1 {
                    self.getWeekApi()
                    self.getTodayApi()
//                    let action = UIAlertAction(title: "Ok", style: .default) { (_ ) in
//                        self.getWeekApi()
//                        self.getTodayApi()
//                    }
                    //self.showAlertMessage(titleStr: "TeeduTutor", messageStr: self.homeCancelRespnse?.message!, completion: action)
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.homeCancelRespnse?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}


