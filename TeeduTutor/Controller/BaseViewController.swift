//
//  BaseViewController.swift
//  Teedu
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//
import UIKit
import SystemConfiguration
import MBProgressHUD

class BaseViewController: UIViewController, UIGestureRecognizerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if(navigationController!.viewControllers.count > 1){
            return true
        }
        return false
    }
    
    func storyBoardMain() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    func showLoader() {
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    func hideLoader()  {
        MBProgressHUD.hide(for: view, animated: true)
    }
    
    func showAlertMessage(titleStr:String?, messageStr:String?, completion: UIAlertAction? = nil) {
        
        let alert = UIAlertController(title: titleStr ?? Constant.APPNAME, message: messageStr ?? "Something went wrong", preferredStyle: UIAlertController.Style.alert)
        if completion != nil {
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(completion!)
        } else {
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
//        self.present(alert, animated: true, completion: nil)
        
        let navigation = UIApplication.shared.windows.first?.rootViewController// Constant.appDelegate.window?.rootViewController
        navigation?.present(alert, animated: true, completion: {
            print("Complete")
        })
    }
    
    
    func showNavigationBar(title: String?, setTransparent: Bool?) {
        
        if setTransparent == true {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
        }
        self.navigationController?.navigationBar.isHidden = false
        if title == title {
            self.navigationItem.title = title
        }
    }
    
    func showTabBar() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func hideTabBar() {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- hide NavigationBar
    func hideHideNavigationBar() {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setBorder(view: [UIView]?, width: CGFloat, color: UIColor) {
        if view != nil {
            for i in view! {
                i.layer.borderColor = color.cgColor
                i.layer.borderWidth = width
                i.layer.masksToBounds = true
            }
        }
    }
    
    func customReplace(str: String, by: String, what: String) -> String {
        return str.replacingOccurrences(of: by, with: what)
    }
    
    
    func setShadowinHeader(headershadowView: [UIView]? = nil, shadowBtn: [UIButton]? = nil, setRound: Bool = false) {
        // Shadow and Radius
        if headershadowView != nil {
            for i in headershadowView! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
        
        if shadowBtn != nil {
            for i in shadowBtn! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
    }
    
    
    
//    func setShadowinHeader(headershadowView: [UIView]? = nil, shadowBtn: [UIButton]? = nil) {
//        // Shadow and Radius
//        if headershadowView != nil {
//            for i in headershadowView! {
//                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
//                i.layer.shadowOffset = CGSize(width: 0, height: 3)
//                i.layer.shadowOpacity = 2
//                i.layer.shadowRadius = 3
//                i.layer.masksToBounds = false
//            }
//        }
//        
//        if shadowBtn != nil {
//            for i in shadowBtn! {
//                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
//                i.layer.shadowOffset = CGSize(width: 0, height: 3)
//                i.layer.shadowOpacity = 2
//                i.layer.shadowRadius = 3
//                i.layer.masksToBounds = false
//            }
//        }
//    }
    
    func checkInternet() -> Bool {
      var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
      zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
      zeroAddress.sin_family = sa_family_t(AF_INET)
      let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
          SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
      }
      var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
      if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
        return false
      }
      /* Only Working for WIFI
       let isReachable = flags == .reachable
       let needsConnection = flags == .connectionRequired
       return isReachable && !needsConnection
       */
      // Working for Cellular and WIFI
      let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
      let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
      let ret = (isReachable && !needsConnection)
      return ret
    }
    
    func getSafeAreaHeight() -> CGFloat {
          if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            return safeFrame.minY
          } else {
            return 0
        }
    }
    
    func saveData(value: Any?, key: String) {
        UserDefaults.standard.setValue(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func removeData(key: String) {
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    func getData(key: String) -> String? {
        return UserDefaults.standard.value(forKey: key) as? String
    }
    
    
    //convert Date
    func convDate(date: Date, format: String? = "yyyy-MM-dd") -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = format
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        return myStringafd
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func stringToDate(dateStr: String, withFormat format: String = "yyyy-MM-dd")-> Date? {

        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        //dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
        //dateFormatter.locale = Locale(identifier: "fa-IR")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: dateStr)

        return date

    }
    
}



extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
    }
}


extension UITableViewCell {
    
    func stringToDate(dateStr: String, withFormat format: String = "yyyy-MM-dd")-> Date? {

        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
        //dateFormatter.locale = Locale(identifier: "fa-IR")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: dateStr)

        return date

    }
    
    func convDate(date: Date, format: String? = "yyyy-MM-dd") -> String? {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = format
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        return myStringafd
    }
}

extension UICollectionViewCell {
    
    func stringToDate(dateStr: String, withFormat format: String = "yyyy-MM-dd")-> Date? {

        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
        //dateFormatter.locale = Locale(identifier: "fa-IR")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: dateStr)

        return date

    }
    
    func convDate(date: Date, format: String? = "yyyy-MM-dd") -> String? {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = format
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        return myStringafd
    }
}
