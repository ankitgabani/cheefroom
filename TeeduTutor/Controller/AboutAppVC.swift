//
//  AboutAppVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 30/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import WebKit

class AboutAppVC: BaseViewController {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var faqView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var lblTrem: UILabel!
    @IBOutlet weak var lblPrivacy: UILabel!
    
    @IBOutlet weak var lblT: UILabel!
    @IBOutlet weak var lblp: UILabel!
    
    var faqArr: FAQViewModl?
    
    var aboutArr: AboutViewModl?
    
    var whatPage: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        tblView.delegate = self
        tblView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideTabBar()
        
        if whatPage == "T&C" {
            lblTrem.isHidden = false
            lblPrivacy.isHidden = true
            lblT.isHidden = false
            lblp.isHidden = true
            faqView.isHidden = true
            self.getListApi(url: ProjectUrl.TermsandCoditions)
        } else if whatPage == "Support" {
            self.getListApi(url: ProjectUrl.FAQ)
        } else if whatPage == "FAQ" {
            faqView.isHidden = false
            self.faQApi(url: ProjectUrl.FAQ)
        } else if whatPage == "Privacy" {
            lblTrem.isHidden = true
            lblPrivacy.isHidden = false
            lblT.isHidden = true
            lblp.isHidden = false
             faqView.isHidden = true
            self.getListApi(url: ProjectUrl.PrivacyPolicy)
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AboutAppVC: UIWebViewDelegate {
    
}

extension AboutAppVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqArr?.payload?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FAQCell.self), for: indexPath) as! FAQCell
        cell.quesiton.text = "\(faqArr?.payload?[indexPath.row].question ?? "")"
        cell.answer.attributedText = "\(faqArr?.payload?[indexPath.row].answer ?? "")".htmlAttributedString()
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


//HIT API
extension AboutAppVC {
    
    private func faQApi(url: String) {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: url, param: nil, header: nil).responseFAQModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.faqArr = FAQViewModl(viewModl: response)
                if self.faqArr?.code == 200 {
                    self.tblView.reloadData()
                    //self.webView.loadHTMLString((self.aboutArr?.payload?.content)!, baseURL: nil)
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.faqArr?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    private func getListApi(url: String) {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: url, param: nil, header: header).responseAboutModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.aboutArr = AboutViewModl(viewModl: response)
                if self.aboutArr?.code == 200 {
                    self.webView.loadHTMLString((self.aboutArr?.payload?.content)!, baseURL: nil)
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.aboutArr?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}


class FAQCell: UITableViewCell {
    @IBOutlet weak var quesiton: UILabel!
    @IBOutlet weak var answer: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
