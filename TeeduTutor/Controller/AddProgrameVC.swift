//
//  AddProgrameVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 26/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

struct WeekDaysModl {
    var name: String?
    var image: UIImage?
    var isSelected: Bool?
    
    init(name: String?, image: UIImage?, isSelected: Bool?) {
        self.name = name
        self.image = image
        self.isSelected = isSelected
    }
}

class AddProgrameVC: BaseViewController {
    
    @IBOutlet weak var programNameField: UITextField!
    //@IBOutlet weak var standardField: UITextField!
   // @IBOutlet weak var courseField: UITextField!
    @IBOutlet weak var feesField: UITextField!
    @IBOutlet weak var daysColctnView: UICollectionView! {
        didSet {
            self.daysColctnView.register(UINib(nibName: String(describing: ReusableColctnViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ReusableColctnViewCell.self))
        }
    }
    @IBOutlet weak var fromField: UITextField!
    @IBOutlet weak var toField: UITextField!
    @IBOutlet weak var startingFromField: UITextField!
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var daysColctnViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var navTitle: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgVerified: UIButton!
    
    var arr = [WeekDaysModl]()
    
    var file_name: String?
    
    var subjectID: Int?
    var workingDays = [String]()
    var getWorkingDays: String?
    var selectedCourseID: Int?
    
    let subjectPickerView = UIPickerView()
    let coursesPickerView = UIPickerView()
    let fromDatePicker = UIDatePicker()
    let toDatePicker = UIDatePicker()
    let startingFromPickerView = UIDatePicker()
    
    var ID: Int?
    var setEditData: ProgramListModl.Payload?
    
    var objimageSelect: UIImage?
    
    
    var addProgramResponse: AddProgramViewModl?
    var editProgramResponse: EditProgramViewModl?
    var classSubjectArr: AddProgramClassSubjectViewModl?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        daysColctnView.delegate = self
        daysColctnView.dataSource = self
        
        arr.append(WeekDaysModl(name: "Mon", image: nil, isSelected: false))
        arr.append(WeekDaysModl(name: "Tue", image: nil, isSelected: false))
        arr.append(WeekDaysModl(name: "Wed", image: nil, isSelected: false))
        arr.append(WeekDaysModl(name: "Thu", image: nil, isSelected: false))
        arr.append(WeekDaysModl(name: "Fri", image: nil, isSelected: false))
        arr.append(WeekDaysModl(name: "Sat", image: nil, isSelected: false))
        arr.append(WeekDaysModl(name: "Sun", image: nil, isSelected: false))

        // Do any additional setup after loading the view.
        
        if ID == nil {
            self.sendBtn.setTitle("Confirm", for: .normal)
            self.navTitle.text = "Add"
        } else {
            self.sendBtn.setTitle("Save Changes", for: .normal)
            self.navTitle.text = "Edit"
            self.fillTxtField(data: setEditData!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigation()
        
        
        DispatchQueue.main.async {
            //self.view.layoutIfNeeded()
            self.daysColctnViewHeight.constant = self.daysColctnView.contentSize.height
            self.daysColctnView.layoutIfNeeded()
        }
        
        openCityPicker()
    }
    
    private func setNavigation() {
        
        self.hideHideNavigationBar()
        self.hideTabBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgView.image = image
                } else {
                    self.imgView.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                imgVerified.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                imgVerified.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    func openCityPicker() {
        
        subjectPickerView.backgroundColor = .white
        coursesPickerView.backgroundColor = .white
        
//        standardField.inputView = subjectPickerView
//        courseField.inputView = coursesPickerView
        
        subjectPickerView.delegate = self
        coursesPickerView.delegate = self
        
        //self.getSubjectClassListApi()
        
        
        self.fromDatePicker.datePickerMode = .time
        self.toDatePicker.datePickerMode = .time
        fromField.inputView = self.fromDatePicker
        toField.inputView = self.toDatePicker
        self.fromDatePicker.addTarget(self, action: #selector(self.selectTime(time:)), for: .valueChanged)
        self.fromDatePicker.date = Date()
        
        self.toDatePicker.addTarget(self, action: #selector(self.selectTime(time:)), for: .valueChanged)
        self.toDatePicker.date = Date()
        
        self.startingFromPickerView.datePickerMode = .date
        self.startingFromPickerView.minimumDate = Date()
        self.startingFromField.inputView = self.startingFromPickerView
        self.startingFromPickerView.addTarget(self, action: #selector(self.selectTime(time:)), for: .valueChanged)
        
    }
    
    @objc private func selectTime(time: UIDatePicker) {
        if time == fromDatePicker {
            self.fromField.text = self.convDate(date: self.fromDatePicker.date, format: "HH:mm")
        } else if time == toDatePicker {
            self.toField.text = self.convDate(date: self.toDatePicker.date, format: "HH:mm")
            
            if self.toField.text?.isEmpty == false {
                let earlyDate = Calendar.current.date(byAdding: .hour, value: 1, to: self.toDatePicker.date)
                self.fromField.text = self.convDate(date: earlyDate!, format: "HH:mm")
            }
        } else if time == startingFromPickerView {
            self.startingFromField.text = self.convDate(date: self.startingFromPickerView.date, format: "yyyy-MM-dd")
        }
    }
    
    
    func fillTxtField(data: ProgramListModl.Payload) {
        self.programNameField.text = data.session_name
        self.feesField.text = data.fee
        
        self.startingFromField.text = data.schedule_start_date
        if let sunjectId = Int(data.subjectID ?? "") {
            subjectID = sunjectId
        }
        
        if let classID = Int(data.classID ?? "") {
            selectedCourseID = classID
        }
        
        if let getFrom = data.startFrom {
            if let cDate = self.stringToDate(dateStr: getFrom, withFormat: "HH:mm") {
                self.toField.text = self.convDate(date: cDate, format: "HH:mm")
            }
        } else {
            self.fromField.text = ""
        }
        
        if let getTo = data.endTo {
            if let cTime = self.stringToDate(dateStr: getTo, withFormat: "HH:mm") {
                self.fromField.text = self.convDate(date: cTime, format: "HH:mm")
            }
        } else {
            self.toField.text = ""
        }
        
        var dateFromString = ""
        if let createDateStr = data.startFrom {
            let arrSplit2 = createDateStr.components(separatedBy: ":")
            let strHour = arrSplit2[0] as! String
            let strMin = arrSplit2[1] as! String
            
            dateFromString = String.init(format: "%@:%@", strHour,strMin)
        }
        
        var dateFromString1 = ""
        if let createDateStr = data.endTo {
            let arrSplit2 = createDateStr.components(separatedBy: ":")
            let strHour = arrSplit2[0] as! String
            let strMin = arrSplit2[1] as! String
            dateFromString1 = String.init(format: "%@:%@", strHour,strMin)
        }
        
        self.fromField.text = dateFromString1

        self.toField.text = dateFromString
        
        
        self.descTxtView.text = data.descriptions
        //getWorkingDays = data.workingDays
        
        if let imageUrl = data.image {
            RestApi.shared.getImage(url: BASE_URL+imageUrl) { (image) in
                if let image = image {
                    self.img.image = image
                    self.objimageSelect = image
                }
            }
        } else {
            self.img.image = #imageLiteral(resourceName: "avatar")
        }
        
        workingDays = data.workingDays!.components(separatedBy: ",")
        
        for i in 0..<arr.count {
            for j in workingDays {
                if arr[i].name == j {
                    arr[i].isSelected = true
                }
            }
        }
        self.daysColctnView.reloadData()
    }
    
    
    @IBAction func imgBtnClicked(_ sender: Any) {
        
        openActionSheet()

    }
    
    @IBAction func confirm(_ sender: UIButton) {
        
        self.workingDays.removeAll()
        arr.filter { (selectedDay) -> Bool in
            if selectedDay.isSelected == true {
                workingDays.append(selectedDay.name!)
            }
            return true
        }
        
        if ID == nil {
        
            if self.programNameField.text?.isEmpty == false && self.feesField.text?.isEmpty == false && self.startingFromField.text?.isEmpty == false && self.fromField.text?.isEmpty == false && self.toField.text?.isEmpty == false && self.descTxtView.text.isEmpty == false && workingDays.isEmpty == false  {
                let param = ["session_name": self.programNameField.text!,
                             "starting_from": self.startingFromField.text!,
                             "fee": self.feesField.text!,
                             "start_from": self.toField.text!,
                             "end_to": self.fromField.text!,
                             "description": self.descTxtView.text!,
                             "working_days": self.workingDays.joined(separator: ",")
                             //"image": self.img.image!.jpegData(compressionQuality: 0.2)!,
                    ] as [String : Any]
                
               // self.addProgramListApi(param: param)
                self.callAddSessionAPI(param: param)
    //            let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
    //            RestApi.shared.uploadImg(url: ProjectUrl.addTutorPrograms, imgKeyName: "image", img: self.img.image!, param: param, header: header) { (str) -> Void? in
    //                print(str)
    //            }
            }
        } else {
            if self.programNameField.text?.isEmpty == false && self.feesField.text?.isEmpty == false && self.startingFromField.text?.isEmpty == false && self.fromField.text?.isEmpty == false && self.toField.text?.isEmpty == false && self.descTxtView.text.isEmpty == false && workingDays.isEmpty == false  {
                        let param = ["program_name": self.programNameField.text!,
                                     "schedule_start_date": self.startingFromField.text!,
                                     "fee": self.feesField.text!,
                                     "start_from": self.toField.text!,
                                     "end_to": self.fromField.text!,
                                     "description": self.descTxtView.text!,
                                     "working_days": self.workingDays.joined(separator: ","),
                                     "id": ID!
                            ] as [String : Any]
                        
                //self.editProgramListApi(param: param)
                self.callAddSessionAPI(param: param)
            //            let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
            //            RestApi.shared.uploadImg(url: ProjectUrl.addTutorPrograms, imgKeyName: "image", img: self.img.image!, param: param, header: header) { (str) -> Void? in
            //                print(str)
            //            }
                    }
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension AddProgrameVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ReusableColctnViewCell.self), for: indexPath) as! ReusableColctnViewCell
        
        self.setShadowinHeader(headershadowView: [cell.prntView])
        //cell.backgroundColor = UIColor.green
        cell.prntView.layer.cornerRadius = cell.prntView.frame.height/2
        
        cell.name.text = arr[indexPath.item].name
        
        if arr[indexPath.item].isSelected == true {
            cell.img.image = #imageLiteral(resourceName: "Group 2335")
        } else {
            cell.img.image = #imageLiteral(resourceName: "Group -3")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ReusableColctnViewCell else { return }
        
        if arr[indexPath.item].isSelected == true {
            arr[indexPath.item].isSelected = false
            cell.img.image = #imageLiteral(resourceName: "Group -3")
        } else {
            cell.img.image = #imageLiteral(resourceName: "Group 2335")
            arr[indexPath.item].isSelected = true
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = collectionView.frame.width / 2 * 2
        let totalSpacingWidth = 10 * (2 - 1)

        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + CGFloat(totalSpacingWidth))) / 2
        let rightInset = leftInset

        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        
        //return UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2, height: 70)
    }
}

extension AddProgrameVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == subjectPickerView {
            return self.classSubjectArr?.payload?.courseSelectedID?.count ?? 0
        } else {
            return self.classSubjectArr?.payload?.subjectSelectedID?.count ?? 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == subjectPickerView {
            return self.classSubjectArr?.payload?.courseSelectedID?[row].className
        } else {
            return self.classSubjectArr?.payload?.subjectSelectedID?[row].subjectName
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == subjectPickerView {
          //  self.standardField.text = self.classSubjectArr?.payload?.courseSelectedID?[row].className
            
            let objSub = self.classSubjectArr?.payload?.courseSelectedID?[row]
            self.subjectID = Int((objSub?.classID)!)
        } else {
          //  self.courseField.text = self.classSubjectArr?.payload?.subjectSelectedID?[row].subjectName
            let objSub = self.classSubjectArr?.payload?.subjectSelectedID?[row]
            self.selectedCourseID = Int((objSub?.subjectID!)!)
        }
    }
}


extension AddProgrameVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.editedImage] as? UIImage {
            // imageView.contentMode = .scaleAspectFit
            self.img.image = pickedImage
            self.objimageSelect = pickedImage
            
            let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            file_name = url!.lastPathComponent
        }
        
        picker.dismiss(animated:true,completion:nil)
        //let imageData = tutorImg.image?.jpegData(compressionQuality: 0.30)
        //guard let data = imageData else { return }
        //self.uploadImage(by: data)
    }
    
    private func openGallery()
    {
        let Imgpicker = UIImagePickerController()
        Imgpicker.delegate = self
        Imgpicker.sourceType = .photoLibrary
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    private func openCamera(){
        let Imgpicker = UIImagePickerController()
        Imgpicker.delegate = self
        Imgpicker.sourceType = .camera
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    private func openActionSheet(){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}


//HIT API
extension AddProgrameVC {
    
    func callAddSessionAPI(param: [String: Any]?) {
       self.showLoader()
        
        print(param)
        APIClient.sharedInstance.postImageToServer(self.getData(key: savedKeys.api_token)!, "editTutorPrograms", file_name!, image: objimageSelect, parameters: param!) { (response, error, statusCode) in
                 print("STATUS CODE \(String(describing: statusCode))")
                 print("Response \(String(describing: response))")
                 if error == nil {
                     self.hideLoader()
                     if statusCode == 200
                     {
                         print("upload")
                        self.navigationController?.popViewController(animated: true)

                         // self.showProgressSuccess(true, withTitle: "Image uploaded Successfully.")
                     }else
                     {
                         print("not upload")
                         print("STATUS CODE \(String(describing: statusCode))")
                         //  self.hideHUD()
                         self.navigationController?.popViewController(animated: true)

                         if let msg = response!["msg"] as? String
                         {

                      }
                         
                     }
                 }
                 else{
                     self.hideLoader()
                    self.navigationController?.popViewController(animated: true)
                     print("not upload")
                     // self.hideHUD()
                     
                 }
             }
             
         }
    
    private func addProgramListApi(param: [String: Any]?) {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        
        var imgData: Data?
        if self.img.image == #imageLiteral(resourceName: "programImg") {
            imgData = nil
        } else {
            imgData = self.img.image?.jpegData(compressionQuality: 0.2)
        }
        RestApi.shared.uploadImageWithParam(url: ProjectUrl.addTutorPrograms, param: param!, header: header, imgData: imgData) { (response, str, status) in
            self.hideLoader()
            if status == 1 {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.showAlertMessage(titleStr: nil, messageStr: str)
            }
        }
    }
    
    private func editProgramListApi(param: [String: Any]?) {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        
        var imgData: Data?
        if self.img.image == #imageLiteral(resourceName: "programImg") {
            imgData = nil
        } else {
            imgData = self.img.image?.jpegData(compressionQuality: 0.2)
        }
        RestApi.shared.uploadImageWithParam(url: ProjectUrl.editTutorPrograms, param: param!, header: header, imgData: imgData) { (response, str, status) in
            self.hideLoader()
            if status == 1 {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.showAlertMessage(titleStr: nil, messageStr: str)
            }
        }
    }
    
    
    private func getSubjectClassListApi() {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: ProjectUrl.tutorSelectedSubjectAndClassdata, param: nil, header: header).responseAddProgramClassSubjectModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.classSubjectArr = AddProgramClassSubjectViewModl(viewModl: response)
                if self.classSubjectArr?.code == 1 {
                    
                    if self.ID != nil {
                        self.classSubjectArr?.payload?.subjectSelectedID?.filter({ (selectedSubject) -> Bool in
                            if Int(selectedSubject.subjectID!) == self.subjectID {
                               // self.courseField.text = selectedSubject.subjectName
                                
                                self.classSubjectArr?.payload?.courseSelectedID?.filter({ (selectedCourse) -> Bool in
                                    if Int(selectedCourse.classID!) == self.selectedCourseID {
                                       // self.standardField.text = selectedCourse.className
                                        self.subjectID = Int(selectedCourse.classID!)
                                    }
                                    return true
                                })
                                
                                self.selectedCourseID = Int(selectedSubject.subjectID!)
                            }
                            return true
                        })
                    } else {
                    
                        //self.courseField.text = self.classSubjectArr?.payload?.subjectSelectedID?.first?.subjectName
                      
                        self.selectedCourseID = Int((self.classSubjectArr?.payload?.subjectSelectedID?.first?.subjectID)!)
                        
                       // self.standardField.text = self.classSubjectArr?.payload?.courseSelectedID?.first?.className
                        self.subjectID = Int((self.classSubjectArr?.payload?.courseSelectedID?.first?.classID)!)
                    }
                    
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.classSubjectArr?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}

