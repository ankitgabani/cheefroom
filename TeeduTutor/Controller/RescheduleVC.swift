//
//  RescheduleVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 26/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class RescheduleVC: BaseViewController {
    
    @IBOutlet weak var morningView: UIView!
    @IBOutlet weak var changingMonth: UILabel!
    @IBOutlet weak var morningColctnView: UICollectionView! {
        didSet {
            self.morningColctnView.register(UINib(nibName: String(describing: RescheduleCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: RescheduleCell.self))
        }
    }
    @IBOutlet weak var afterNoonView: UIView!
    @IBOutlet weak var afterNoonColctnView: UICollectionView! {
       didSet {
           self.afterNoonColctnView.register(UINib(nibName: String(describing: RescheduleCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: RescheduleCell.self))
       }
   }
    @IBOutlet weak var nightView: UIView!
    @IBOutlet weak var nightColctnView: UICollectionView! {
       didSet {
           self.nightColctnView.register(UINib(nibName: String(describing: RescheduleCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: RescheduleCell.self))
       }
   }
    @IBOutlet weak var nightColctnViewHeight: NSLayoutConstraint!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var alertText: UILabel!
    
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var teacherName: UILabel!
    @IBOutlet weak var progamImage: UIImageView!
    
    
    
    var morningArr = ["09:00", "09:30", "10:00", "10:30", "11:00"]
    var afternoonArr = ["12:00", "12:30", "13:00", "13:30", "14:00"]
    var nightArr = ["17:00", "17:30", "18:00", "18:30", "19:00", "19:30"]
    
    
    
    
    var monthsArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    var nextDay: Int = 0
    
    var rescheduleResponse: RescheduleViewModl?
    
    var selectedDate: String?
    var selectedTime: String?
    var ID: String?
    var oldDate: String?
    
    var navTitle: String?
    var navteacherName: String?
    var navprogamImage: String?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        morningColctnView.delegate = self
        morningColctnView.dataSource = self
        afterNoonColctnView.delegate = self
        afterNoonColctnView.dataSource = self
        nightColctnView.delegate = self
        nightColctnView.dataSource = self

        self.setBorder(view: [morningView, afterNoonView, nightView], width: 1, color: UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1.0))
        
        self.setShadowinHeader(headershadowView: nil, shadowBtn: [btnLeft, btnRight])
        //currentDate = Calendar.current.component(.day, from: Date())
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navTitle = navTitle, let navteacherName = navteacherName {
            self.titleName.text = navTitle
            self.teacherName.text = navteacherName
        }
        if let navprogamImage = navprogamImage {
            RestApi.shared.getImage(url: BASE_URL+navprogamImage ) { (image) in
                self.progamImage.image = image
            }
        }
        
        self.alertView.isHidden = true
        self.hideTabBar()
        self.hideHideNavigationBar()
        
        setCustomCalender(date: Date())
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    @IBAction func forward(_ sender: UIButton) {
        //changingMonth.text = self.customizeDate(date: Date(), number: 1)
        nextDay += 1
        let selectedDate = Calendar.current.date(byAdding: .day, value: nextDay, to: Date())!
        setCustomCalender(date: selectedDate)
    }
    
    @IBAction func backward(_ sender: UIButton) {
        
        nextDay -= 1
        let selectedPreDate = Calendar.current.date(byAdding: .day, value: nextDay, to: Date())!
        setCustomCalender(date: selectedPreDate)
    }
    
    func setCustomCalender(date: Date) {
        
        let day = Calendar.current.component(.day, from: date)
        let month = Calendar.current.component(.month, from: date)
        let year = Calendar.current.component(.year, from: date)
        
        let now = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        
        selectedDate = "\(year)-\(month)-\(day)"
        changingMonth.text = "\(day), \(nameOfMonth) \(year)"
    }
    
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        if selectedDate != nil && selectedTime != nil && ID != nil {
            let param = ["id": ID!,
                         "date": selectedDate!,
                         "time": selectedTime!,
                         "old_date": oldDate!,
                         "api_token":self.getData(key: savedKeys.api_token)!
                ] as [String : Any]
            self.hitApi(param: param)
        }
    }
    
    @IBAction func alertOkAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
//        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: RegisterVC.self)) as! RegisterVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func selectUnselect(colctnView: UICollectionView, colctnView1: UICollectionView, arr: [String], arr2: [String], indexPath: IndexPath) {
        for i in 0..<arr.count {
            guard let cell = colctnView.cellForItem(at: [0, i]) as? RescheduleCell else { print("Cell not clicked"); return }
            if cell.backgroundColor == ProjectColor.darkGreen {
                cell.backgroundColor = .clear
            }
        }
        for j in 0..<arr2.count {
            guard let cell = colctnView1.cellForItem(at: [0, j]) as? RescheduleCell else { print("Cell not clicked"); return }
            if cell.backgroundColor == ProjectColor.darkGreen {
                cell.backgroundColor = .clear
            }
        }
    }
}

extension RescheduleVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == morningColctnView {
            return 5
        } else if collectionView == afterNoonColctnView {
            return 5
        } else if collectionView == nightColctnView {
            return 6
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == morningColctnView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RescheduleCell.self), for: indexPath) as! RescheduleCell
            cell.layer.cornerRadius = cell.frame.height/2
            cell.name.text = morningArr[indexPath.row]
            return cell
        } else if collectionView == afterNoonColctnView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RescheduleCell.self), for: indexPath) as! RescheduleCell
            cell.layer.cornerRadius = cell.frame.height/2
            cell.name.text = afternoonArr[indexPath.row]
            return cell
        } else if collectionView == nightColctnView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RescheduleCell.self), for: indexPath) as! RescheduleCell
            cell.layer.cornerRadius = cell.frame.height/2
            cell.name.text = nightArr[indexPath.row]
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == morningColctnView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? RescheduleCell else { print("Cell not clicked"); return }
            cell.backgroundColor = ProjectColor.darkGreen
            selectedTime = morningArr[indexPath.row]
            selectUnselect(colctnView: afterNoonColctnView, colctnView1: nightColctnView, arr: afternoonArr, arr2: nightArr, indexPath: indexPath)
        } else if collectionView == afterNoonColctnView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? RescheduleCell else { print("Cell not clicked"); return }
            cell.backgroundColor = ProjectColor.darkGreen
            selectedTime = afternoonArr[indexPath.row]
            selectUnselect(colctnView: morningColctnView, colctnView1: nightColctnView, arr: morningArr, arr2: nightArr, indexPath: indexPath)
        } else if collectionView == nightColctnView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? RescheduleCell else { print("Cell not clicked"); return }
            cell.backgroundColor = ProjectColor.darkGreen
            selectedTime = nightArr[indexPath.row]
            selectUnselect(colctnView: morningColctnView, colctnView1: afterNoonColctnView, arr: morningArr, arr2: afternoonArr, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == morningColctnView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? RescheduleCell else { print("Cell not clicked"); return }
            cell.backgroundColor = .clear

        } else if collectionView == afterNoonColctnView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? RescheduleCell else { print("Cell not clicked"); return }
            cell.backgroundColor = .clear
        } else if collectionView == nightColctnView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? RescheduleCell else { print("Cell not clicked"); return }
            cell.backgroundColor = .clear
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == morningColctnView {
            return CGSize(width: collectionView.frame.width/5, height: 40)
        } else if collectionView == afterNoonColctnView {
            return CGSize(width: collectionView.frame.width/5, height: 40)
        } else if collectionView == nightColctnView {
            return CGSize(width: collectionView.frame.width/5, height: 40)
        } else {
            return CGSize(width: collectionView.frame.width/5, height: 40)
        }
    }
}

//HIT API
extension RescheduleVC {
    
    private func hitApi(param: [String: Any]) {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        //let header = ["Authorization": "Bearer tutor_I0gFNl0Uu7kiEiM"]
        RestApi.shared.requiredPOST(url: ProjectUrl.rescheduleTutorSession, param: param, header: header).responseRescheduleModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.rescheduleResponse = RescheduleViewModl(rescheduleModel: response)
                if self.rescheduleResponse?.code == 1 {
                    print(response)
                    self.alertText.text = self.rescheduleResponse!.message
                    self.alertView.isHidden = false
                    //self.showAlertMessage(titleStr: nil, messageStr: self.rescheduleResponse?.message ?? nil)
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.rescheduleResponse?.message ?? nil)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
