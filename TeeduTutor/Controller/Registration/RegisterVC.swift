//
//  RegisterVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
//import MapKit

class RegisterVC: BaseViewController {
    
    @IBOutlet weak var fullNameField: UITextField!
    @IBOutlet weak var mobileField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var skillSetField: UITextField!
    @IBOutlet weak var countryCodeField: UITextField!
    
    @IBOutlet weak var nameImg: UIImageView!
    @IBOutlet weak var mobileImg: UIImageView!
    @IBOutlet weak var cityImg: UIImageView!
    @IBOutlet weak var emailImg: UIImageView!
    @IBOutlet weak var contunueBtn: UIButton!
    
//    var location: Location? {
//        didSet {
//            cityField.text = location.flatMap({ $0.title }) ?? "No location selected"
//        }
//    }
    
    
    let cityPicker = UIPickerView()
    var selectedCityID: Int?
    var selectedSkillId: String?
    //var cityArr: CityViewModl?
    var registerResponse: RegisterViewModl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contunueBtn.layer.cornerRadius=10
        // Do any additional setup after loading the view.
        setFieldDelegate(txtField: [fullNameField, mobileField, emailField, cityField])
        //openCityPicker()
        
    }
    
    func setFieldDelegate(txtField: [UITextField]) {
        for i in txtField {
            i.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.setValidateImg()
        if cityField.text == "" || cityField.text?.isEmpty == true {
            //cityImg.image = #imageLiteral(resourceName: "uncheck")
        } else {
            cityImg.image = #imageLiteral(resourceName: "check")
        }
        
        if emailField.text?.isEmpty == false {
            if isValidEmail(emailField.text!) == true {
                emailImg.image = #imageLiteral(resourceName: "check")
            } else {
                emailImg.image = #imageLiteral(resourceName: "uncheck")
            }
        }
        self.hideHideNavigationBar()
    }
    
    @IBAction func cityBtnTapped(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: CityVC.self)) as! CityVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func skillSetTapped(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SkillSetVC.self)) as! SkillSetVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
        if fullNameField.text == "" {
            self.view.makeToast("Please enter full name", duration: 0.3, position: .center)
            return false
        } else if mobileField.text == "" {
            self.view.makeToast("Please enter mobile number", duration: 0.3, position: .center)
            return false
        } else if (mobileField.text?.count)! < 10 {
            self.view.makeToast("Please enter valid mobile number", duration: 0.3, position: .center)
            return false
        } else if emailField.text == "" {
            self.view.makeToast("Please enter email", duration: 0.3, position: .center)
            return false
        } else if cityField.text == "" {
            self.view.makeToast("Please select city", duration: 0.3, position: .center)
            return false
        } else if skillSetField.text == "" {
            self.view.makeToast("Please select skillset", duration: 0.3, position: .center)
            return false
        }
        return true
    }
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        
        if self.isValidatedLogin() {
            if allFieldsReturn() == true {
                if fullNameField.text?.isEmpty == false && mobileField.text?.isEmpty == false && emailField.text?.isEmpty == false && cityField.text?.isEmpty == false && countryCodeField.text?.isEmpty == false {
                    let param = ["full_name":fullNameField.text!,
                                 "email":emailField.text!,
                                 "mobile":mobileField.text!,
                                 "password":"12345",
                                 "city":cityField.text!,
                                 "user_type":"TUTOR",
                                 "signUp_type":"normal",
                                 "countryCode":"91",
                                 "device_type":"I",
                                 "skill_id":selectedSkillId ?? 0,
                                 "device_token": self.getData(key: savedKeys.device_token)!,
                                 "latitude": UserDefaults.standard.value(forKey: savedKeys.lat) as! Double,
                                 "longitude": UserDefaults.standard.value(forKey: savedKeys.long) as! Double
                        ] as [String: Any]
                    
                    self.showLoader()
                    RestApi.shared.requiredPOST(url: ProjectUrl.registerTutor, param: param, header: nil).responseRegisterModl { (response) in
                        self.hideLoader()
                        if let response = response.result.value {
                            self.registerResponse = RegisterViewModl(registerModel: response)
                            if self.registerResponse?.code == 1 {
                                print(response)
                                self.saveData(value: self.registerResponse!.payload!.apiToken!, key: savedKeys.api_token)
                                let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: VerifyOTPVC.self)) as! VerifyOTPVC
                                vc.fromRegister = true
                                vc.countryCode = "\(self.registerResponse!.payload!.countryCode!)"
                                vc.mobileNumber = self.registerResponse!.data!.mobileNumber!
                                self.navigationController?.pushViewController(vc, animated: true)
                                self.hideHideNavigationBar()
                                //self.showAlertMessage(titleStr: "OTP", messageStr: "\(self.registerResponse?.data?.otp ?? 0)")
                            } else if self.registerResponse?.code == 4 {
                                print(response)
                                self.saveData(value: self.registerResponse!.payload!.apiToken!, key: savedKeys.api_token)
                                let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: VerifyOTPVC.self)) as! VerifyOTPVC
                                vc.fromRegister = true
                                vc.countryCode = "\(self.registerResponse!.payload!.countryCode!)"
                                vc.mobileNumber = self.registerResponse!.data!.mobileNumber!
                                self.navigationController?.pushViewController(vc, animated: true)
                                self.hideHideNavigationBar()
                                //self.showAlertMessage(titleStr: "OTP", messageStr: "\(self.registerResponse?.data?.otp ?? 0)")
                            } else {
                                self.showAlertMessage(titleStr: nil, messageStr: self.registerResponse?.message!)
                            }
                        }  else {
                            self.showAlertMessage(titleStr: nil, messageStr: nil)
                            print(response)
                        }
                        
                    }
                    
                }
            }
        }
        
    }
    
    private func allFieldsReturn() -> Bool {
        if nameImg.image == #imageLiteral(resourceName: "check") && mobileImg.image == #imageLiteral(resourceName: "check") && emailImg.image == #imageLiteral(resourceName: "check") && cityImg.image == #imageLiteral(resourceName: "check") {
            return true
        } else {
            return false
        }
    }
    
    private func setValidateImg() {
        if fullNameField.text?.isEmpty == true {
            nameImg.image = nil
        }
        if mobileField.text?.isEmpty == true {
            mobileImg.image = nil
        }
        if cityField.text?.isEmpty == true {
            cityImg.image = nil
        }
        if emailField.text?.isEmpty == true {
            emailImg.image = nil
        }
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension RegisterVC: selectedCityWithLatLong {
    func getCity(txt: String?, lat: Double?, long: Double?) {
        self.cityField.text = txt
        self.saveData(value: lat, key: savedKeys.lat)
        self.saveData(value: long, key: savedKeys.long)
    }
}


extension RegisterVC: selectedSkillSet {
    func getSkillSet(txt: String?, id: String?) {
        self.skillSetField.text = txt
        selectedSkillId = id
//        self.saveData(value: lat, key: savedKeys.lat)
//        self.saveData(value: long, key: savedKeys.long)
    }
}


extension RegisterVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == fullNameField {
            if textField.text!.count <= 1 {
                nameImg.image = #imageLiteral(resourceName: "uncheck")
            } else {
                nameImg.image = #imageLiteral(resourceName: "check")
            }
            
        } else if textField == mobileField {
            if textField.text!.count <= 8 {
                mobileImg.image = #imageLiteral(resourceName: "uncheck")
            } else {
                mobileImg.image = #imageLiteral(resourceName: "check")
            }
            
        } else if textField == emailField {
            if isValidEmail(textField.text!) == true {
                emailImg.image = #imageLiteral(resourceName: "check")
            } else {
                emailImg.image = #imageLiteral(resourceName: "uncheck")
            }
        }
        return true
    }
}

//extension RegisterVC: UIPickerViewDelegate, UIPickerViewDataSource {
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return cityArr?.payload?.count ?? 0
//    }
//
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return cityArr?.payload?[row].name
//    }
//
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        cityField.text = cityArr?.payload?[row].name
//        self.selectedCityID = cityArr?.payload?[row].id
//    }
//}
