//
//  LoginVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 03/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class LoginVC: BaseViewController {

    @IBOutlet weak var phField: UITextField!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var countryCodeField: UITextField!
    @IBOutlet weak var headingTxt: UILabel!
    @IBOutlet weak var signUpView: UIView!
 //   @IBOutlet weak var loginBackHeight: NSLayoutConstraint!
    
    var updateMobileNumber: Bool?
    

    var loginResponse: LoginViewModl?
    var updateNoResponse: UpdateNumberOtpViewModl?
    override func viewDidLoad() {
        super.viewDidLoad()
         
        continueBtn.layer.cornerRadius=10;
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if updateMobileNumber == true {
            signUpView.isHidden = true
           // loginBackHeight.constant = 44
           // headingTxt.text = "Enter your\nNew number"
        } else {
            signUpView.isHidden = false
          //   loginBackHeight.constant = 0
           // headingTxt.text = "Enter your\nPhone number"
        }
        
        view.layoutIfNeeded()
        
        self.hideTabBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.hideHideNavigationBar()
    }
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
        if phField.text == "" {
            self.view.makeToast("Please enter mobile number", duration: 0.3, position: .center)
            return false
        } else if (phField.text?.count)! < 10 {
            self.view.makeToast("Please enter valid mobile number", duration: 0.3, position: .center)
            return false
        }
        return true
    }
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        
        if self.isValidatedLogin() {
            if phField.text?.isEmpty == false && countryCodeField.text?.isEmpty == false {
                let param = ["mobile": phField.text!,
                             "countryCode": "91",
                             "user_type": "TUTOR"
                    ] as [String : Any]
                if updateMobileNumber == true {
                    self.changeMobileApi(param: param)
                } else {
                    self.hitApi(param: param)
                }
            }
        }
        
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registerPressed(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: RegisterVC.self)) as! RegisterVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


//HIT API
extension LoginVC {
    
    private func changeMobileApi(param: [String: Any]) {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.updateNumberOtp, param: param, header: header).responseUpdateNumberOtpModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.updateNoResponse = UpdateNumberOtpViewModl(login: response)
                if self.updateNoResponse?.code == 200 {
                    print(response)
                    self.saveData(value: response.payload!.apiToken, key: savedKeys.api_token)
                    let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: VerifyOTPVC.self)) as! VerifyOTPVC
                    vc.fromRegister = false
                    vc.updateMobileNumber = self.updateMobileNumber
                    vc.countryCode = "\(self.updateNoResponse!.payload!.countryCode!)"
                    vc.mobileNumber = self.updateNoResponse!.payload!.mobile!
                    self.navigationController?.pushViewController(vc, animated: true)
                    //self.showAlertMessage(titleStr: "OTP", messageStr: "\(self.updateNoResponse?.payload?.otp ?? 0)")
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.updateNoResponse?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    private func hitApi(param: [String: Any]) {
        self.showLoader()
        RestApi.shared.requiredPOST(url: ProjectUrl.sendOtp, param: param, header: nil).responseLoginModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.loginResponse = LoginViewModl(login: response)
                if self.loginResponse?.code == 200 {
                    print(response)
                    
                    let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: VerifyOTPVC.self)) as! VerifyOTPVC
                    vc.fromRegister = false
                    vc.countryCode = "\(self.loginResponse!.payload!.countryCode!)"
                    vc.mobileNumber = self.loginResponse!.payload!.mobile!
                    self.navigationController?.pushViewController(vc, animated: true)
                    //self.showAlertMessage(titleStr: "OTP", messageStr: "\(self.loginResponse?.payload?.otp ?? 0)")
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.loginResponse?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
