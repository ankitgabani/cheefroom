//
//  SubjectVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 25/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout

class SubjectVC: BaseViewController {
    
    @IBOutlet weak var colctnView: UICollectionView! {
        didSet {
            self.colctnView.register(UINib(nibName: String(describing: ReusableColctnViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ReusableColctnViewCell.self))
            
            self.colctnView.register(UINib(nibName: String(describing: Col.self), bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: Col.self))
        }
    }
    @IBOutlet weak var colctnViewHeight: NSLayoutConstraint!
    @IBOutlet weak var coursesName: UILabel!
    
    var arr:SubjectsViewModl?// = [TutorialModl]()
    var selectedCoursesID: [Int]?
    var convertedCoursesID: String?
    var selectedCoursesName: [String]?
    
    var whereToTeachID: Int?
    var selectedSubject = [Int]()
    
    var fromEditProfile: Bool?
    var profileArr: [String : Any]?
    private var editProfie: EditProfileViewModl?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        colctnView.delegate = self
        colctnView.dataSource = self
        
        if let alignedFlowLayout = colctnView?.collectionViewLayout as? AlignedCollectionViewFlowLayout {
            alignedFlowLayout.horizontalAlignment = .left
            //alignedFlowLayout.verticalAlignment = .top
        }
        
        //self.selectedCoursesID = [16,17]
        let first = self.customReplace(str: "\(self.selectedCoursesID!)", by: "[", what: "")
        let second = self.customReplace(str: first, by: "]", what: "")//removeFirstBracked.replacingOccurrences(of: "]", with: "")
        self.convertedCoursesID = "\(second)"
        
        
        
        //selectedCoursesName = ["10th", "11th Science", "12th Science"]
        coursesName.text = selectedCoursesName?.joined(separator: ", ")
            
        
        self.hitApi()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideHideNavigationBar()
        DispatchQueue.main.async {
            self.colctnViewHeight.constant = self.colctnView.contentSize.height
        }
    }
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        
        if selectedSubject.isEmpty == false && convertedCoursesID != nil {
            if fromEditProfile == true {
                let first = self.customReplace(str: "\(self.selectedSubject)", by: "[", what: "")
                let second = self.customReplace(str: first, by: "]", what: "")
                let subjectIDs = "\(second)"
                
                let param = ["fullname":profileArr?["fullname"] as! String,
                             "email":profileArr?["email"] as! String,
                             "city":profileArr?["city"] as! String,
                             "bio": profileArr?["bio"] as! String,
                             "place_id": profileArr?["place_id"] as! Int,
                             "subject_id": subjectIDs,
                             "course_id":convertedCoursesID!
                    ] as [String : Any]
                self.editProfileApi(param: param)
            } else {
                let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: AboutTutorVC.self)) as! AboutTutorVC
                vc.selectedSubject = selectedSubject
                vc.whereToTeachID = whereToTeachID
                vc.convertedCoursesID = convertedCoursesID
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension SubjectVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arr?.payload?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr?.payload?[section].count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ReusableColctnViewCell.self), for: indexPath) as! ReusableColctnViewCell
        
        self.setShadowinHeader(headershadowView: [cell.prntView])
        //cell.backgroundColor = UIColor.green
        cell.prntView.layer.cornerRadius = cell.prntView.frame.height/2
        
        cell.img.image = #imageLiteral(resourceName: "Group 11119")
        cell.name.text = arr?.payload?[indexPath.section][indexPath.item].subjectName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ReusableColctnViewCell else { return }
        if cell.img.image == #imageLiteral(resourceName: "Group 11113") {
            if self.selectedSubject.isEmpty == false {
                self.selectedSubject.removeAll(where: { (id) -> Bool in
                    if id == (arr?.payload?[indexPath.section][indexPath.item].subjectID)! {
                        cell.img.image = #imageLiteral(resourceName: "Group 11119")
                        return true
                    }
                    return false
                })
            }
        } else {
            cell.img.image = #imageLiteral(resourceName: "Group 11113")
            self.selectedSubject.append((arr?.payload?[indexPath.section][indexPath.item].subjectID)!)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionView.elementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: Col.self), for: indexPath) as! Col
            headerView.backgroundColor = UIColor.clear
            headerView.name.text = selectedCoursesName?[indexPath.section]
            return headerView
        } else {
            return UICollectionReusableView()
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((arr?.payload?[indexPath.section][indexPath.item].subjectName)! as NSString).size(withAttributes: [NSAttributedString.Key.font : UIFont(name: "Quicksand-Regular", size: 20)!]).width + 80, height: 65)
    }
}

//HIT API
extension SubjectVC {
    
    private func hitApi() {
        self.showLoader()
        RestApi.shared.requiredPOST(url: ProjectUrl.getTutorSubjectdata, param: nil, header: nil).responseSubjectsModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.arr = SubjectsViewModl(registerSubject: response)
                if self.arr?.code == 1 {
                    print(response)
                    
                    self.colctnView.reloadData()
                    
                    DispatchQueue.main.async {
                        self.colctnViewHeight.constant = self.colctnView.contentSize.height
                        self.colctnView.layoutIfNeeded()
                    }
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.arr?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    private func editProfileApi(param: [String: Any]) {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.updateTutorProfile, param: param, header: header).responseEditProfileModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.editProfie = EditProfileViewModl(viewModl: response)
                if self.editProfie?.code == 1 {
                    print(response)
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller .isKind(of: ProfileVC.self) {
                            self.navigationController?.popToViewController(controller, animated: true)
                        }
                    }
                    
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.editProfie?.message ?? nil)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
