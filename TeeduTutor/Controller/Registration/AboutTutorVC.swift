//
//  AboutTutorVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 25/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class AboutTutorVC: BaseViewController {
    
    @IBOutlet weak var aboutTxtView: UITextView!
    @IBOutlet weak var tutorNameLbl: UILabel!
    @IBOutlet weak var tutorImg: UIImageView!
    
    var selectedSubject: [Int]?
    var convertedCoursesID: String?
    var whereToTeachID: Int?
    
    var bioResponse: BioDataViewModl?

    override func viewDidLoad() {
        super.viewDidLoad()

        aboutTxtView.delegate = self
        
        aboutTxtView.text = "Type here - 500 words"
        aboutTxtView.textColor = UIColor.lightGray
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let username = self.getData(key: savedKeys.username) {
            self.tutorNameLbl.text = username
        }
        
        
        
        self.hideHideNavigationBar()
    }
    
    @IBAction func profileBtnClicked(_ sender: Any) {
        openActionSheet()

    }
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        
        if aboutTxtView.text != "Type here - 500 words" && aboutTxtView.text.isEmpty == false && selectedSubject?.isEmpty == false && whereToTeachID != nil && convertedCoursesID != nil {
            
            //self.selectedSubject = [16,17]
            let first = self.customReplace(str: "\(self.selectedSubject!)", by: "[", what: "")
            let second = self.customReplace(str: first, by: "]", what: "")
            let subjectIDs = "\(second)"
            
            let param = ["bio":aboutTxtView.text!,
                         "class_id":self.convertedCoursesID!,
                         "subject_id":subjectIDs,
                         "tuition_place":whereToTeachID!,
                ] as [String : Any]
            self.hitApi(param: param)
        }
    }
}


extension AboutTutorVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.editedImage] as? UIImage {
            // imageView.contentMode = .scaleAspectFit
            tutorImg.image = pickedImage
            let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            let fileName = url!.lastPathComponent
            
            let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
            self.showLoader()
            RestApi.shared.uploadImage(url: ProjectUrl.ProfileImage, fileName, header: header, imgData: tutorImg.image!.jpegData(compressionQuality: 0.2)!) { (response, msg, status) in
                self.hideLoader()
                if status != 1 {
                    self.showAlertMessage(titleStr: nil, messageStr: msg)
                } else {
                    self.saveData(value: BASE_URL+(response ?? ""), key: savedKeys.profile_image)
                }
            }
        }
        picker.dismiss(animated:true,completion:nil)
        //let imageData = tutorImg.image?.jpegData(compressionQuality: 0.30)
        //guard let data = imageData else { return }
        //self.uploadImage(by: data)
    }
    
    private func openGallery()
    {
        let Imgpicker = UIImagePickerController()
        Imgpicker.delegate = self
        Imgpicker.sourceType = .photoLibrary
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    private func openCamera(){
        let Imgpicker = UIImagePickerController()
        Imgpicker.delegate = self
        Imgpicker.sourceType = .camera
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    private func openActionSheet(){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Remove", style: .destructive , handler:{ (UIAlertAction)in
            self.tutorImg.image = UIImage(named: "aboutImg")
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}



extension AboutTutorVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Type here - 500 words"
            textView.textColor = UIColor.lightGray
        }
    }
}


//HIT API
extension AboutTutorVC {
    
    private func hitApi(param: [String: Any]) {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.TutorBiodata, param: param, header: header).responseBioDataModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.bioResponse = BioDataViewModl(registerBio: response)
                if self.bioResponse?.code == 1 {
                    print(self.bioResponse)
                    let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.bioResponse?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
