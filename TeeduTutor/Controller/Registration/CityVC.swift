//
//  CityVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout

protocol selectedCityWithLatLong {
    func getCity(txt: String?, lat: Double?, long: Double?)
}

class CityVC: BaseViewController {
    
    @IBOutlet weak var colctnView: UICollectionView! {
       didSet {
           self.colctnView.register(UINib(nibName: String(describing: RescheduleCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: RescheduleCell.self))
       }
    }
    
    @IBOutlet weak var searchParentView: UIView!
    @IBOutlet weak var searchField: UITextField!
    
    var cityArr: CityModl?
    var cityLatLongArr: CityLatLongModl?
    
    var delegate: selectedCityWithLatLong?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchField.delegate = self
        colctnView.allowsMultipleSelection=true
        self.setShadowinHeader(headershadowView: [searchParentView], setRound: true)

        // Do any additional setup after loading the view.
        
        if let alignedFlowLayout = colctnView?.collectionViewLayout as? AlignedCollectionViewFlowLayout {
            alignedFlowLayout.horizontalAlignment = .left
            alignedFlowLayout.verticalAlignment = .top
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getCityApi(text: "ne")
        self.hideHideNavigationBar()
        self.hideTabBar()
    }
    
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension CityVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchField {
            self.getCityApi(text: searchField.text)
        }
        
        return true
    }
}

extension CityVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityArr?.predictions?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RescheduleCell.self), for: indexPath) as? RescheduleCell else { return UICollectionViewCell() }
        
        self.setShadowinHeader(headershadowView: [cell.parentView])
        cell.parentView.layer.cornerRadius = 8
        cell.parentView.backgroundColor = .white
        //self.setBorder(view: [cell.parentView], width: 1, color: .black)
        
        cell.name.text = cityArr?.predictions?[indexPath.item].predictionDescription
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        getCityLatLongApi(selectedName: cityArr?.predictions?[indexPath.item].predictionDescription, text: cityArr?.predictions?[indexPath.item].placeID)
        

    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if ((cityArr?.predictions?[indexPath.item].predictionDescription)! as NSString).size(withAttributes: [NSAttributedString.Key.font : UIFont(name: "Quicksand-Regular", size: 20)!]).width > 60 {
            return CGSize(width: collectionView.frame.width, height: 60)
        } else {
            return CGSize(width: ((cityArr?.predictions?[indexPath.item].predictionDescription)! as NSString).size(withAttributes: [NSAttributedString.Key.font : UIFont(name: "Quicksand-Regular", size: 20)!]).width, height: 60)
        }
    }
}


extension CityVC {
    
    func getCityApi(text: String?) {
        
        RestApi.shared.requiredGET(url: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(text ?? "")&components=country:in&key=AIzaSyBNUdEqTkJmoTlsoU8HvPcqCR5K_m6-pRc", param: nil, header: nil).responseCityModl { (response) in
            if let response = response.result.value {
                self.cityArr = response
                
                self.colctnView.reloadData()
                
            } else {
                print(response)
                self.showAlertMessage(titleStr: nil, messageStr: nil)
            }
        }
    }
    
    func getCityLatLongApi(selectedName: String?, text: String?) {
        
        RestApi.shared.requiredGET(url: "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(text ?? "")&key=AIzaSyBNUdEqTkJmoTlsoU8HvPcqCR5K_m6-pRc", param: nil, header: nil).responseCityLatLongModl { (response) in
            if let response = response.result.value {
                self.cityLatLongArr = response
                
                self.delegate?.getCity(txt: selectedName, lat: self.cityLatLongArr?.result?.geometry?.location?.lat, long: self.cityLatLongArr?.result?.geometry?.location?.lng)
                self.navigationController?.popViewController(animated: true)
                
            } else {
                print(response)
                self.showAlertMessage(titleStr: nil, messageStr: nil)
            }
        }
    }
}
