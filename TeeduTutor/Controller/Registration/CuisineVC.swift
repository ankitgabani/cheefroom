//
//  CityVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout

protocol selectedCuisine {
    func getCity(txt: String?, lat: Double?, long: Double?)
}

class CuisineVC: BaseViewController {
    
    @IBOutlet weak var btnContne: UIButton!
    @IBOutlet weak var colctnView: UICollectionView!
    
    @IBOutlet weak var searchParentView: UIView!
    @IBOutlet weak var searchField: UITextField!
    
    var cityArr: [Alaphabet] = [Alaphabet]()
    var cityArrSearching: [Alaphabet] = [Alaphabet]()

    var isSearching: Bool = false
    var selectedSuisine: [Cuisine]?
    var cityLatLongArr: [CityLatLongModl]?
    
    var delegate: selectedCuisine?
    
     var arrCollectionKey = NSMutableArray()
    
    
    var isFromEditProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        if isFromEditProfile == true {
            btnContne.setTitle("Save", for: .normal)
        } else {
            btnContne.setTitle("Continue", for: .normal)
        }
        
        self.searchField.delegate = self
        colctnView.allowsMultipleSelection=true
        colctnView.register(UINib(nibName: "CuisineCell", bundle: nil), forCellWithReuseIdentifier: "CuisineCell")
        self.colctnView.register(UINib(nibName: String(describing: CuisineHeader.self), bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: CuisineHeader.self))
        colctnView.delegate = self
        colctnView.dataSource = self
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        colctnView!.collectionViewLayout = layout
        self.setShadowinHeader(headershadowView: [searchParentView], setRound: true)
        
        // Do any additional setup after loading the view.
        
        if let alignedFlowLayout = colctnView?.collectionViewLayout as? AlignedCollectionViewFlowLayout {
            alignedFlowLayout.horizontalAlignment = .left
            alignedFlowLayout.verticalAlignment = .top
        }
        
        searchField.addTarget(self, action: #selector(SkillSetVC.textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
        self.getCityApi()
        self.hideHideNavigationBar()
        self.hideTabBar()
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ONCONTINUECLICK(_ sender: Any) {
        
        print(self.arrCollectionKey)
        
        let set = NSSet(array: arrCollectionKey as! [Any])

        print(set)
        
        let strings1 = set.allObjects as? [String] // or as!

        let finalStr = strings1?.joined(separator: ",")
 
         var param = ["": ""] as [String: Any]
        
        if self.arrCollectionKey != [""] {
                        
            param = ["cuisine_ids": finalStr ?? ""]
        } else {
            param = ["cuisine_ids": ""]
        }
        
        hitSaveCuisineAPI(param: param)
    }
}


extension CuisineVC: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if self.searchField.text!.isEmpty {
            
            self.isSearching = false
            
            self.colctnView.reloadData()
            
        } else {
            self.isSearching = true
            
            self.cityArrSearching.removeAll(keepingCapacity: false)
            
            for i in 0..<self.cityArr.count {
                
                let listItem: Alaphabet = self.cityArr[i]
//                if listItem.cuisines.lowercased().range(of: self.searchField.text!.lowercased()) != nil {
//                    self.cityArrSearching.append(listItem)
//                }
            }
            
            self.colctnView.reloadData()
        }
        
    }
    
}

extension CuisineVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.isSearching == true {
            return cityArrSearching[section].cuisines.count
        } else {
            return cityArr[section].cuisines.count
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if self.isSearching == true {
            return cityArrSearching.count
        } else {
            return cityArr.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionView.elementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: CuisineHeader.self), for: indexPath) as! CuisineHeader
            headerView.backgroundColor = UIColor.clear
            
            if self.isSearching == true {
                headerView.name.text = cityArrSearching[indexPath.section].alphabet
            } else {
                headerView.name.text = cityArr[indexPath.section].alphabet
            }
            
            return headerView
        } else {
            return UICollectionReusableView()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 8)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CuisineCell.self), for: indexPath) as? CuisineCell else { return UICollectionViewCell() }
        
        //        self.setShadowinHeader(headershadowView: [cell.parentView])
        //        cell.parentView.layer.cornerRadius = 8
        //        cell.parentView.backgroundColor = .white
        //        //self.setBorder(view: [cell.parentView], width: 1, color: .black)
        //
        
        if self.isSearching == true {
            cell.name.text = cityArrSearching[indexPath.section].cuisines[indexPath.row].cuisineTitle
            
            let key = cityArrSearching[indexPath.section].cuisines[indexPath.row].cuisineID
            
            if cityArrSearching[indexPath.section].cuisines[indexPath.row].selected == "1"
            {
                self.arrCollectionKey.add(key)
                cell.image.image = #imageLiteral(resourceName: "check")
            } else {
                
                self.arrCollectionKey.remove(key)
                cell.image.image = #imageLiteral(resourceName: "Group -3")
            }
        } else {
            cell.name.text = cityArr[indexPath.section].cuisines[indexPath.row].cuisineTitle
            
            let key = cityArr[indexPath.section].cuisines[indexPath.row].cuisineID
            
            if cityArr[indexPath.section].cuisines[indexPath.row].selected == "1"
            {
                self.arrCollectionKey.add(key)
                cell.image.image = #imageLiteral(resourceName: "check")
            } else {
                
                self.arrCollectionKey.remove(key)
                cell.image.image = #imageLiteral(resourceName: "Group -3")
            }
        }
        
            
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.isSearching == true {
            let key = cityArrSearching[indexPath.section].cuisines[indexPath.row].cuisineID
            
            if cityArrSearching[indexPath.section].cuisines[indexPath.row].selected == "1"
            {
                cityArrSearching[indexPath.section].cuisines[indexPath.row].selected = "0"
                self.arrCollectionKey.remove(key)
                selectedSuisine?.remove(at: indexPath.row)
            }else {
                
                cityArrSearching[indexPath.section].cuisines[indexPath.row].selected = "1"
                 self.arrCollectionKey.add(key)
                selectedSuisine?.append((cityArr[indexPath.section].cuisines[indexPath.row]))
            }
            collectionView.reloadData()
        } else {
            let key = cityArr[indexPath.section].cuisines[indexPath.row].cuisineID
            
            if cityArr[indexPath.section].cuisines[indexPath.row].selected == "1"
            {
                cityArr[indexPath.section].cuisines[indexPath.row].selected = "0"
                self.arrCollectionKey.remove(key)
                selectedSuisine?.remove(at: indexPath.row)
            }else {
                
                cityArr[indexPath.section].cuisines[indexPath.row].selected = "1"
                 self.arrCollectionKey.add(key)
                selectedSuisine?.append((cityArr[indexPath.section].cuisines[indexPath.row]))
            }
            collectionView.reloadData()
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: 60)
    }
}


extension CuisineVC {
    
    func getCityApi() {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        
        RestApi.shared.requiredGET(url: "http://api.chefroom.in/api/cuisines_list", param: nil, header: header).responseCuisineModel { (response) in
            if let response = response.result.value {
                self.hideLoader()
                self.cityArr = response.payload.alaphabets
                
                self.colctnView.reloadData()
                
            } else {
                self.hideLoader()
                print(response)
                self.showAlertMessage(titleStr: nil, messageStr: nil)
            }
        }
    }
    
    private func hitSaveCuisineAPI(param: [String: Any]) {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]

        RestApi.shared.requiredPOST(url: "http://api.chefroom.in/api/cuisines_save", param: param, header: header).responseSaveCuisineModl { (response) in
            self.hideLoader()
            if let response = response.result.value {

                print(response)

                if self.isFromEditProfile == true {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    let redViewController = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                    
                    let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
