//
//  WhereToTeachVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class WhereToTeachVC: BaseViewController {
    
    @IBOutlet weak var tblViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tblView: UITableView! {
        didSet {
            self.tblView.register(UINib(nibName: "ReusableCell", bundle: nil), forCellReuseIdentifier: "ReusableCell")
        }
    }
    
    var arr: WhereTeachViewModl?
    var selectedTeach: Int?

    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        // Do any additional setup after loading the view.\
        
        hitApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.hideHideNavigationBar()
    }
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        
        if selectedTeach != nil {
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: CoursesVC.self)) as! CoursesVC
            vc.whereToTeachID = selectedTeach
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension WhereToTeachVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr?.payload?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ReusableCell.self), for: indexPath) as! ReusableCell
        
        self.setShadowinHeader(headershadowView: [cell.prntView])
        cell.prntView.layer.cornerRadius = cell.prntView.frame.height/2
        
        cell.img.image = #imageLiteral(resourceName: "Group 11119")
        cell.name.text = arr?.payload?[indexPath.row].tuitionType
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ReusableCell else { return }
        cell.img.image = #imageLiteral(resourceName: "Group 11113")
        
        let objPay = arr?.payload?[indexPath.row]
        
        self.selectedTeach = Int((objPay?.id)!)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ReusableCell else { return }
        cell.img.image = #imageLiteral(resourceName: "Group 11119")
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    
}


//HIT API
extension WhereToTeachVC {
    
    private func hitApi() {
        self.showLoader()
        RestApi.shared.requiredGET(url: ProjectUrl.getplacewheretoteach, param: nil, header: nil).responseWhereTeachModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.arr = WhereTeachViewModl(registerWTM: response)
                if self.arr?.code == 1 {
                    print(response)
                    
                    self.tblView.reloadData()
                    
                    DispatchQueue.main.async {
                        self.tblViewHeight.constant = self.tblView.contentSize.height
                    }
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.arr?.message ?? nil)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
