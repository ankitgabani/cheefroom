//
//  SkillSetVC.swift
//  TeeduTutor
//
//  Created by Navjot Singh on 20/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//


import UIKit
import AlignedCollectionViewFlowLayout

protocol selectedSkillSet {
    func getSkillSet(txt: String?, id: String?)
}

class SkillSetVC: BaseViewController {
    
    @IBOutlet weak var colctnView: UICollectionView! {
        didSet {
            self.colctnView.register(UINib(nibName: String(describing: RescheduleCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: RescheduleCell.self))
        }
    }
    
    @IBOutlet weak var searchParentView: UIView!
    @IBOutlet weak var searchField: UITextField!
    
    var isSearching: Bool = false
    var cityArr: [Payload] = [Payload]()
    var cityArrSearching: [Payload] = [Payload]()
    
    var cityLatLongArr: CityLatLongModl?
    
    var delegate: selectedSkillSet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  self.searchField.delegate = self
        
        self.setShadowinHeader(headershadowView: [searchParentView], setRound: true)
        
        // Do any additional setup after loading the view.
        
        if let alignedFlowLayout = colctnView?.collectionViewLayout as? AlignedCollectionViewFlowLayout {
            alignedFlowLayout.horizontalAlignment = .left
            alignedFlowLayout.verticalAlignment = .top
        }
        
        searchField.addTarget(self, action: #selector(SkillSetVC.textFieldDidChange(_:)),
                              for: UIControl.Event.editingChanged)
        
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        self.navigationController?.navigationBar.isHidden = true
        self.getCityApi()
        self.hideHideNavigationBar()
         self.hideTabBar()
    }
    
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SkillSetVC: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if self.searchField.text!.isEmpty {
            
            self.isSearching = false
            
            self.colctnView.reloadData()
            
        } else {
            self.isSearching = true
            
            self.cityArrSearching.removeAll(keepingCapacity: false)
            
            for i in 0..<self.cityArr.count {
                
                let listItem: Payload = self.cityArr[i]
                if listItem.skillTitle.lowercased().range(of: self.searchField.text!.lowercased()) != nil {
                    self.cityArrSearching.append(listItem)
                }
            }
            
            self.colctnView.reloadData()
        }
        
    }
    
}

extension SkillSetVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.isSearching == true {
            return cityArrSearching.count
        } else {
            return cityArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RescheduleCell.self), for: indexPath) as? RescheduleCell else { return UICollectionViewCell() }
        
        self.setShadowinHeader(headershadowView: [cell.parentView])
        cell.parentView.layer.cornerRadius = 8
        cell.parentView.backgroundColor = .white
        //self.setBorder(view: [cell.parentView], width: 1, color: .black)
        
        
        var user: Payload!
        if self.isSearching == true {
            user = self.cityArrSearching[indexPath.row]
        } else {
            user = self.cityArr[indexPath.row]
        }
        
        cell.name.text = user.skillTitle
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if self.isSearching == true {
            
            self.delegate?.getSkillSet(txt: cityArrSearching[indexPath.item].skillTitle, id: cityArrSearching[indexPath.item].skillID)
            
        } else {
            
            self.delegate?.getSkillSet(txt: cityArr[indexPath.item].skillTitle, id: cityArr[indexPath.item].skillID)
            
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if ((cityArr[indexPath.item].skillTitle) as NSString).size(withAttributes: [NSAttributedString.Key.font : UIFont(name: "Quicksand-Regular", size: 20)!]).width > 60 {
            return CGSize(width: collectionView.frame.width, height: 60)
        } else {
            return CGSize(width: ((cityArr[indexPath.item].skillTitle) as NSString).size(withAttributes: [NSAttributedString.Key.font : UIFont(name: "Quicksand-Regular", size: 20)!]).width, height: 60)
        }
    }
}


extension SkillSetVC {
    
    func getCityApi() {
        self.showLoader()
        RestApi.shared.requiredGET(url: "http://api.chefroom.in/api/skillsets", param: nil, header: nil).responseSkillsetModel { (response) in
            if let response = response.result.value {
                self.hideLoader()
                
                print(response)
                self.cityArr = response.payload
                
                
                self.colctnView.reloadData()
                
            } else {
                print(response)
                self.hideLoader()
                self.showAlertMessage(titleStr: nil, messageStr: nil)
            }
        }
    }
    
}
