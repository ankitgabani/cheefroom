//
//  VerifyOTPVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 25/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class VerifyOTPVC: BaseViewController {
    
    @IBOutlet weak var otpFourthTF: UITextField!
    @IBOutlet weak var otpThirdTF: UITextField!
    @IBOutlet weak var otpSeconedTF: UITextField!
    @IBOutlet weak var otpFirstTF: UITextField!
    @IBOutlet weak var otpFifthTF: UITextField!
    @IBOutlet weak var otpSixthTF: UITextField!
    @IBOutlet weak var setNumberLbl: UILabel!
    @IBOutlet weak var resendView: UIView!
    
    
    var mobileNumber: String?
    var countryCode: String?
    var fromRegister: Bool?
    var updateMobileNumber: Bool?
    
    var verifyOtpArr: VerifyOtpViewMod?
    var registerResponse: LoginViewModl?
    var updateNoResponse: UpdateNumberOtpViewModl?

    @IBOutlet weak var continueBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        continueBtn.layer.cornerRadius=10
        otpFirstTF.delegate = self
        otpSeconedTF.delegate = self
        otpThirdTF.delegate = self
        otpFourthTF.delegate = self
        otpFifthTF.delegate = self
        otpSixthTF.delegate = self
        
        if let mobileNumber = mobileNumber, let countryCode = countryCode {
            self.setNumberLbl.text = "(+\(countryCode)) - \(mobileNumber)"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideHideNavigationBar()
        
        if updateMobileNumber == true {
            resendView.isHidden = true
        } else {
            resendView.isHidden = false
        }
    }
    
    @IBAction func resendBtn(_ sender: UIButton) {
        
        self.showLoader()
        let param = ["mobile": mobileNumber!, "countryCode": "91", "user_type": "TUTOR"] as [String: Any]
        RestApi.shared.requiredPOST(url: ProjectUrl.sendOtp, param: param, header: nil).responseLoginModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.registerResponse = LoginViewModl(login: response)
                if self.registerResponse?.code == 200 {
                    print(response)
                    self.countryCode = "\(self.registerResponse!.payload!.countryCode!)"
                    self.mobileNumber = self.registerResponse!.payload!.mobile!
                    //self.showAlertMessage(titleStr: "OTP", messageStr: "\(self.registerResponse?.payload?.otp ?? 0)")
                    //self.showAlertMessage(titleStr: nil, messageStr: self.registerResponse?.message ?? nil)
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.registerResponse?.message ?? nil)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        
        if otpFirstTF.text?.isEmpty == false && otpSeconedTF.text?.isEmpty == false && otpThirdTF.text?.isEmpty == false && otpFourthTF.text?.isEmpty == false && otpFifthTF.text?.isEmpty == false && otpSixthTF.text?.isEmpty == false {
            
            let otp = "\(otpFirstTF.text!)\(otpSeconedTF.text!)\(otpThirdTF.text!)\(otpFourthTF.text!)\(otpFifthTF.text!)\(otpSixthTF.text!)"
            
            if updateMobileNumber == true {
                let param = ["mobile": mobileNumber!, "otp": otp, "user_type": "TUTOR"] as [String: Any]
                if checkInternet() == true {
                    updateNoverifyOTPApi(param: param)
                }
            } else {
                let param = ["mobile": mobileNumber!, "otp": otp, "user_type": "TUTOR", "device_token": self.getData(key: savedKeys.device_token) ?? ""] as [String: Any]
                verifyOTPApi(param: param)
            }
            
        } else {
             self.view.makeToast("Please enter otp", duration: 0.3, position: .center)
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}


extension VerifyOTPVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if ((textField.text?.count)! < 1) && (string.count > 0)
        {
            if textField ==  otpFirstTF {
                otpSeconedTF.becomeFirstResponder()
            }
            if textField ==  otpSeconedTF {
                otpThirdTF.becomeFirstResponder()
            }
            if textField ==  otpThirdTF {
                otpFourthTF.becomeFirstResponder()
            }
            if textField ==  otpFourthTF {
                otpFifthTF.becomeFirstResponder()
            }
            if textField == otpFifthTF {
                otpSixthTF.becomeFirstResponder()
            }
            if textField == otpSixthTF {
                otpSixthTF.resignFirstResponder()
            }
            textField.text = string
            return false
        }
        else if ((textField.text?.count)!>=1) && (string.count == 0)
        {
            
            if textField == otpSeconedTF
            {
                otpFirstTF.becomeFirstResponder()
            }
            if textField == otpThirdTF
            {
                otpSeconedTF.becomeFirstResponder()
            }
            if textField == otpFourthTF
            {
                otpThirdTF.becomeFirstResponder()
            }
            if textField == otpFirstTF
            {
                otpFirstTF.resignFirstResponder()
            }
            
            if textField == otpFifthTF
            {
                otpFourthTF.becomeFirstResponder()
            }
            
            if textField == otpSixthTF
            {
                otpFifthTF.becomeFirstResponder()
            }
            
            
            textField.text = ""
            return false
            
        }
        else if (textField.text?.count)! >= 1
        {
            textField.text = string
            return false
        }
        
        return true
    }
}


extension VerifyOTPVC {
    
    func updateNoverifyOTPApi(param: [String: Any]) {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.verifyupdatenumberOtp, param: param, header: header).responseVerifyOtp { (response) in
           self.hideLoader()
           if let response = response.result.value {
               self.verifyOtpArr = VerifyOtpViewMod(registerOTP: response)
               if self.verifyOtpArr?.code == 200 {
                   print(response)
                   self.saveData(value: self.verifyOtpArr?.payload?.apiToken!, key: savedKeys.api_token)
                   self.saveData(value: self.verifyOtpArr?.payload?.id, key: savedKeys.userID)
                   self.saveData(value: self.verifyOtpArr?.payload?.email, key: savedKeys.email)
                   self.saveData(value: self.verifyOtpArr?.payload?.mobile, key: savedKeys.mobile)
                   self.saveData(value: BASE_URL+(self.verifyOtpArr?.payload?.profileImage ?? ""), key: savedKeys.profile_image)
                   self.saveData(value: self.verifyOtpArr?.payload?.status, key: savedKeys.status)
                   self.saveData(value: self.verifyOtpArr?.payload?.fullName, key: savedKeys.username)
                   
                   
                   for controller in self.navigationController!.viewControllers as Array {
                       if controller.isKind(of: ProfileVC.self) {
                           self.navigationController!.popToViewController(controller, animated: true)
                           break
                       }
                   }
               } else {
                   self.showAlertMessage(titleStr: nil, messageStr: self.verifyOtpArr?.message ?? nil)
               }
           }  else {
               self.showAlertMessage(titleStr: nil, messageStr: nil)
               print(response)
           }
       }
    }
    
    
    func verifyOTPApi(param: [String: Any]) {
        self.showLoader()
        RestApi.shared.requiredPOST(url: ProjectUrl.verify_otp, param: param, header: nil).responseVerifyOtp { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.verifyOtpArr = VerifyOtpViewMod(registerOTP: response)
                if self.verifyOtpArr?.code == 200 {
                    print(response)
                    
                    UserDefaults.standard.setValue(self.verifyOtpArr?.payload?.bank_details?.name, forKey: "name")
                    UserDefaults.standard.setValue(self.verifyOtpArr?.payload?.bank_details?.bank_name, forKey: "bank_name")
                    UserDefaults.standard.setValue(self.verifyOtpArr?.payload?.bank_details?.account_no, forKey: "account_no")
                    UserDefaults.standard.setValue(self.verifyOtpArr?.payload?.bank_details?.ifsc, forKey: "ifsc")
                    UserDefaults.standard.setValue(self.verifyOtpArr?.payload?.bank_details?.created_at, forKey: "created_at")
                    UserDefaults.standard.setValue(self.verifyOtpArr?.payload?.bank_details?.updated_at, forKey: "updated_at")
                    UserDefaults.standard.synchronize()

                    self.saveData(value: self.verifyOtpArr?.payload?.apiToken!, key: savedKeys.api_token)
                    self.saveData(value: self.verifyOtpArr?.payload?.id, key: savedKeys.userID)
                    self.saveData(value: self.verifyOtpArr?.payload?.email, key: savedKeys.email)
                    self.saveData(value: self.verifyOtpArr?.payload?.mobile, key: savedKeys.mobile)
                    self.saveData(value: BASE_URL+(self.verifyOtpArr?.payload?.profileImage ?? ""), key: savedKeys.profile_image)
                    
                    self.saveData(value: self.verifyOtpArr?.payload?.latitude, key: savedKeys.lat)
                    self.saveData(value: self.verifyOtpArr?.payload?.longitude, key: savedKeys.long)
                    
                    self.saveData(value: self.verifyOtpArr?.payload?.status, key: savedKeys.status)
                    self.saveData(value: self.verifyOtpArr?.payload?.fullName, key: savedKeys.username)
                    
                    
                    if self.fromRegister == true {
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: CuisineVC.self)) as! CuisineVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        
                        let redViewController = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = redViewController
                        
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.verifyOtpArr?.message ?? nil)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
