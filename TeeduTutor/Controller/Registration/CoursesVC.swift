//
//  CoursesVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout

class CoursesVC: BaseViewController {
    
    @IBOutlet weak var colctnView: UICollectionView! 
    @IBOutlet weak var colctnViewHeight: NSLayoutConstraint!
    
    var arr:ClassesViewModl? // = [CourcesModel]()
    
    var selectedCourse = [Int]()
    var selectedCoursesName = [String]()
    var whereToTeachID: Int?
    
    var fromEditProfile: Bool?
    var profileArr: [String : Any]?

    override func viewDidLoad() {
        super.viewDidLoad()

        colctnView.delegate = self
        colctnView.dataSource = self
        
//        if let flowLayout = colctnView.collectionViewLayout as? UICollectionViewFlowLayout {
//            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
//        }
        
        if let alignedFlowLayout = colctnView?.collectionViewLayout as? AlignedCollectionViewFlowLayout {
            alignedFlowLayout.horizontalAlignment = .left
            alignedFlowLayout.verticalAlignment = .top
        }
        
        self.hitApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideHideNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.hideHideNavigationBar()
    }
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        
        if selectedCourse.isEmpty == false {
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SubjectVC.self)) as! SubjectVC
            vc.selectedCoursesID = selectedCourse
            vc.selectedCoursesName = selectedCoursesName
            vc.whereToTeachID = whereToTeachID
            vc.fromEditProfile = fromEditProfile
            vc.profileArr = profileArr
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension CoursesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return arr.count
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr?.payload?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ReusableColctnViewCell.self), for: indexPath) as! ReusableColctnViewCell
        
        self.setShadowinHeader(headershadowView: [cell.prntView])
        //cell.backgroundColor = UIColor.green
        cell.prntView.layer.cornerRadius = cell.prntView.frame.height/2
        
        cell.img.image = #imageLiteral(resourceName: "Group 11119")
        cell.name.text = arr?.payload?[indexPath.item].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ReusableColctnViewCell else { return }
        if cell.img.image == #imageLiteral(resourceName: "Group 11113") {
            if self.selectedCourse.isEmpty == false {
                self.selectedCourse.removeAll(where: { (id) -> Bool in
                    if id == (arr?.payload?[indexPath.item].id)! {
                        cell.img.image = #imageLiteral(resourceName: "Group 11119")
                        return true
                    }
                    return false
                })
            }
            
            if self.selectedCoursesName.isEmpty == false {
                self.selectedCoursesName.removeAll(where: { (id) -> Bool in
                    if id == (arr?.payload?[indexPath.item].name)! {
                        return true
                    }
                    return false
                })
            }
            
            
            
        } else {
            cell.img.image = #imageLiteral(resourceName: "Group 11113")
            self.selectedCourse.append((arr?.payload?[indexPath.item].id)!)
            self.selectedCoursesName.append((arr?.payload?[indexPath.item].name)!)
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        guard let cell = collectionView.cellForItem(at: indexPath) as? ReusableColctnViewCell else { return }
//        //cell.img.image = #imageLiteral(resourceName: "Group 11119")
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width: ((arr?.payload?[indexPath.item].name)! as NSString).size(withAttributes: [NSAttributedString.Key.font : UIFont(name: "Quicksand-Regular", size: 20)!]).width + 80, height: 65)
    }
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        if kind == UICollectionView.elementKindSectionHeader {
//            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: ColctnReusableView.self), for: indexPath) as! ColctnReusableView
//            headerView.backgroundColor = UIColor.clear
//            headerView.name.text = arr[indexPath.section].name
//            return headerView
//        } else {
//            return UICollectionReusableView()
//        }
//    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: 60)
//    }
}


//HIT API
extension CoursesVC {
    
    private func hitApi() {
        self.showLoader()
        RestApi.shared.requiredGET(url: ProjectUrl.getClassesData, param: nil, header: nil).responseClassesModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.arr = ClassesViewModl(registerClasses: response)
                if self.arr?.code == 1 {
                    print(response)
                    
                    self.colctnView.reloadData()
                    
                    DispatchQueue.main.async {
                        self.colctnViewHeight.constant = self.colctnView.contentSize.height
                        self.colctnView.layoutIfNeeded()
                    }
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.arr?.message ?? nil)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}
