//
//  NotificationVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 30/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class NotificationVC: BaseViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    var notificationArr: NotificationViewModl?

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.saveData(value: "NO", key: savedKeys.isNotification)
        
        tblView.delegate = self
        tblView.dataSource = self
        
        self.hideTabBar()
        self.hideHideNavigationBar()
        self.getNotificaitonApi()
    }
    
     
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr?.payload?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NotificationCell.self), for: indexPath) as! NotificationCell
        cell.setData(data: (self.notificationArr?.payload?[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.notificationArr?.payload?[indexPath.row].notificationCategory == "Booking" {
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
            vc.selectedIndex = 2
            self.navigationController?.pushViewController(vc, animated: true)
        } else if self.notificationArr?.payload?[indexPath.row].notificationCategory == "Review" {
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
            vc.selectedIndex = 4
            vc.sendToReview = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
}

//HIT API
extension NotificationVC {
    
    private func getNotificaitonApi() {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: ProjectUrl.Notifications, param: nil, header: header).responseNotificationModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.notificationArr = NotificationViewModl(viewModl: response)
                if self.notificationArr?.code == 1 {
                    self.tblView.reloadData()
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.notificationArr?.message!)
                }
            }  else {
                print(response)
                self.showAlertMessage(titleStr: nil, messageStr: nil)
            }
        }
    }
}
