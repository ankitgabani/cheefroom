//
//  ReviewsVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 10/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import Cosmos

class ReviewsVC: BaseViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var reviewCount: UILabel!
    
    var reviewResponse: ReviewViewModl?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self

        // Do any additional setup after loading the view.
        
        self.reviewListApi()
        self.notificationStatusApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hideTabBar()
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ReviewsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reviewResponse?.payload?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ReviewCell.self)) as! ReviewCell
        cell.setData(data: (self.reviewResponse?.payload?[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}


//HIT API
extension ReviewsVC {
    
    private func notificationStatusApi() {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.notificationStatus, param: ["is_read": 1, "category": "Review"], header: header).responseJSON(completionHandler: { (response) in
            self.hideLoader()
            if let response = response.result.value as? [String: Any] {
                if response["code"] as? Int != 1 {
                    print("notification not read")
                }
            }
        })
    }
    
    private func reviewListApi() {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: ProjectUrl.tutorFeedbacks, param: nil, header: header).responseReviewModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                print(response)
                self.reviewResponse = ReviewViewModl(viewModl: response)
                if self.reviewResponse?.code == 1 {
                    self.tblView.reloadData()
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.reviewResponse?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
}


class ReviewCell: UITableViewCell {
    
    @IBOutlet weak var smallDesc: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var showDate: UILabel!
    @IBOutlet weak var stars: CosmosView!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setData(data: ReviewModl.Payload) {
        self.userName.text = data.fullName
        if let imageName = data.profileImage {
            RestApi.shared.getImage(url: BASE_URL+imageName) { (image) in
                self.img.image = image
            }
        } else {
            self.img.image = #imageLiteral(resourceName: "avatar")
        }
        self.smallDesc.text = data.title!
        self.desc.text = data.payloadDescription
        self.stars.rating = Double(data.rating ?? "")!
        self.showDate.text = data.createdAt
    }
}
