//
//  KycAlertVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 25/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class KycAlertVC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideHideNavigationBar()
    }
    
    @IBAction func doITNow(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: DocsUploadVC.self)) as! DocsUploadVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func later(_ sender: UIButton) {
        
    }

}
