//
//  TutorialVC.swift
//  Teedu
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import FSPagerView

struct TutorialModl {
    var name: String?
    var image: UIImage?
    
    init(name: String?, image: UIImage?) {
        self.name = name
        self.image = image
    }
}

class TutorialVC: BaseViewController {
    
   @IBOutlet weak var textView: UILabel!
    @IBOutlet weak var nextbtn: UIButton!
    @IBOutlet weak var skipbtn: UIButton!
    @IBOutlet weak var pagrView: FSPagerView! {
        didSet {
            self.pagrView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    
    @IBOutlet weak var pageControl: FSPageControl!
    @IBOutlet weak var skip: UIButton!
    
    var arr = [TutorialModl]()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text="Different recipes that will help you prepare your own."
        //skip.addTextSpacing(spacing: 2.0)
        
        arr.append(TutorialModl(name: "Different recipes that will help you prepare your own.", image: ProjectImages.tutorial1))
        arr.append(TutorialModl(name: "One dish meal, for you & your family.", image: ProjectImages.tutorial2))
        arr.append(TutorialModl(name: "One dish meal, Lorem lpsum is simply dummy text.", image: ProjectImages.tutorial3))
        skipbtn.layer.cornerRadius=skipbtn.frame.height/2
         nextbtn.layer.cornerRadius=nextbtn.frame.height/2
        pageControl.numberOfPages = 3
        pageControl.currentPage = 0
        pageControl.contentHorizontalAlignment = .center
        
        pageControl.setStrokeColor(.gray, for: .normal)
        pageControl.setFillColor(.gray, for: .normal)
        
        pageControl.setFillColor(.black, for: .selected)
        pageControl.setStrokeColor(.black, for: .selected)
        
        pagrView.delegate = self
        pagrView.dataSource = self
    
      
        self.hideHideNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
//        if UserDefaults.standard.value(forKey: savedKeys.userID) != nil  {
//            let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
//            self.navigationController?.pushViewController(rootVC, animated: true)
//        }
        
        self.hideHideNavigationBar()
    }
    
    
    @IBAction func skip(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension TutorialVC: FSPagerViewDelegate, FSPagerViewDataSource {
     
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return arr.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleToFill
        cell.contentView.layer.shadowRadius = 0
        cell.imageView?.image = arr[index].image
        return cell
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        pageControl.currentPage = targetIndex
       textView.text=arr[targetIndex].name
                     if targetIndex==1{
                         textView.textColor=UIColor.black
                     }else{
                         textView.textColor=UIColor.white
                     }
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        pageControl.currentPage = pagerView.currentIndex
       
    }
    
}

