//
//  DocsUploadVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 25/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import Alamofire

enum setCard {
    case aadhar
    case pan
    case degree
}

class DocsUploadVC: BaseViewController {
    
    var kycArr: KycViewModl?
    
    @IBOutlet weak var aadharBtn: UIButton!
    @IBOutlet weak var panCardBtn: UIButton!
    @IBOutlet weak var degreeBtn: UIButton!
    
    @IBOutlet weak var aadharImg: UIImageView!
    @IBOutlet weak var panCardImg: UIImageView!
    @IBOutlet weak var degreeImg: UIImageView!
    
    var aadharData: Data?
    var panCardData: Data?
    var degreeData: Data?
    var aditionalData: Data?
    
    var aadharFile: String?
    var panCardFile: String?
    var degreeFile: String?
    var aditionalFile: String?
    
    var fromMore: Bool?
    
    var aadharMimType: String?
    var panCardMimType: String?
    var degreeMimType: String?
    var aditionalMimType: String?
    
    var selectCard: setCard?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setShadowinHeader(headershadowView: nil, shadowBtn: [aadharBtn, panCardBtn, degreeBtn])

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideTabBar()
        self.hideHideNavigationBar()
    }
    
    @IBAction func aadharCardUpload(_ sender: UIButton) {
        
        selectCard = .aadhar
        openActionSheet()
        
    }
    
    @IBAction func panCardUpload(_ sender: UIButton) {
        selectCard = .pan
        openActionSheet()
    }
    
    @IBAction func degreeUpload(_ sender: UIButton) {
        selectCard = .degree
        openActionSheet()
    }
 
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        if aadharData != nil && panCardData != nil && degreeData != nil && aditionalData != nil {
            self.uploadKycApi(img1: aadharData!, fileName1: aadharFile!, name1: aadharMimType!, img2: panCardData!, fileName2: panCardFile!, name2: panCardMimType!, img3: degreeData!, fileName3: degreeFile!, name3: degreeMimType!, img4: aditionalData!, fileName4: degreeFile!, name4: degreeMimType!)
        }
    }
    
    fileprivate func p_documentclicked() {
        let importMenu = UIDocumentPickerViewController(documentTypes: ["public.text", "public.data","public.pdf", "public.doc"], in: UIDocumentPickerMode.import)
        importMenu.delegate = self
        self.present(importMenu, animated: true, completion: nil)
    }

}


extension DocsUploadVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
       let cico = url as URL
       print(cico)
       self.downloadfile(URL: cico as NSURL)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.originalImage] as? UIImage {
            
            if selectCard == setCard.aadhar {
                aadharImg.image = pickedImage
                aadharData = pickedImage.jpegData(compressionQuality: 0.2)
                aadharFile = "aadhar.jpeg"
                aadharMimType = "image/jpeg"
                
            } else if selectCard == setCard.pan {
                panCardImg.image = pickedImage
                panCardData = pickedImage.jpegData(compressionQuality: 0.2)
                panCardFile = "panCard.jpeg"
                panCardMimType = "image/jpeg"
                
            } else if selectCard == setCard.degree {
                degreeImg.image = pickedImage
                degreeData = pickedImage.jpegData(compressionQuality: 0.2)
                degreeFile = "degree.jpeg"
                degreeMimType = "image/jpeg"
                
            }
            // imageView.contentMode = .scaleAspectFit
            //imgView.image = pickedImage
        }
        picker.dismiss(animated:true,completion:nil)
        //let imageData = tutorImg.image?.jpegData(compressionQuality: 0.30)
        //guard let data = imageData else { return }
        //self.uploadImage(by: data)
    }
    
    private func openGallery()
    {
        let Imgpicker = UIImagePickerController()
        Imgpicker.delegate = self
        Imgpicker.sourceType = .photoLibrary
        Imgpicker.allowsEditing = false
        present(Imgpicker, animated: true, completion: nil)
    }
    
    private func openCamera(){
        let Imgpicker = UIImagePickerController()
        Imgpicker.delegate = self
        Imgpicker.sourceType = .camera
        Imgpicker.allowsEditing = false
        present(Imgpicker, animated: true, completion: nil)
    }
    
    private func openActionSheet(){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Files", style: .default , handler:{ (UIAlertAction)in
            self.p_documentclicked()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}


//HIT API
extension DocsUploadVC {
    
    private func uploadKycApi(img1: Data, fileName1: String, name1: String, img2: Data, fileName2: String, name2: String, img3: Data, fileName3: String, name3: String, img4: Data, fileName4: String, name4: String) {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        
        RestApi.shared.uploadKycImage(url: ProjectUrl.kyc_Documents, header: header, imgData1: img1, imgFileName1: fileName1, mimType1: name1, imgData2: img2, imgFileName2: fileName2, mimType2: name2, imgData3: img3, imgFileName3: fileName3, mimType3: name3, imgData4: img4, imgFileName4: fileName4, mimType4: name4) { (response, str, status) in
            self.hideLoader()
            if status == 1 {
//                let action = UIAlertAction(title: "Ok", style: .default) { (_ ) in
//                    self.navigationController?.popViewController(animated: true)
//                }
//                self.showAlertMessage(titleStr: nil, messageStr: str, completion: action)
                if self.fromMore == true {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TabBarVC.self)) as! TabBarVC
                    vc.selectedIndex = 1
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                self.hideHideNavigationBar()
            } else {
                self.showAlertMessage(titleStr: nil, messageStr: str)
            }
        }
    }
}


extension DocsUploadVC {
    fileprivate func downloadfile(URL: NSURL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL as URL)
        request.httpMethod = "POST"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error == nil) {
                // Success
                let statusCode = response?.mimeType
                print("Success: \(String(describing: statusCode))")
                DispatchQueue.main.async(execute: {
                    self.aadharImg.image = UIImage(named: "file")
                    
                    
                    
                    if self.selectCard == setCard.aadhar {
                        self.aadharImg.image = UIImage(named: "file")
                        self.aadharData = data!
                        self.aadharFile = URL.lastPathComponent!
                        self.aadharMimType = "text/plain"
                        
                    } else if self.selectCard == setCard.pan {
                        self.panCardImg.image = UIImage(named: "file")
                        self.panCardData = data!
                        self.panCardFile = URL.lastPathComponent!
                        self.panCardMimType = "text/plain"
                        
                    } else if self.selectCard == setCard.degree {
                        self.degreeImg.image = UIImage(named: "file")
                        self.degreeData = data!
                        self.degreeFile = URL.lastPathComponent!
                        self.degreeMimType = "text/plain"
                        
                    }
                    
                    
                    //self.p_uploadDocument(data!, filename: URL.lastPathComponent!)
                })

                // This is your file-variable:
                // data
            }
            else {
                // Failure
                print("Failure: %@", error!.localizedDescription)
            }
        })
        task.resume()
    }
    
    fileprivate func p_uploadDocument(_ file: Data,filename : String) {

        //let parameters = ["yourParam" : "sample text"]
        let fileData = file
        
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let URL2 = try! URLRequest(url: ProjectUrl.kyc_Documents, method: .post, headers: header)

        Alamofire.upload(multipartFormData: { (multipartFormData) in

            multipartFormData.append(fileData as Data, withName: "upfile", fileName: filename, mimeType: "text/plain")


//            for (key, value) in parameters {
//                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//            }
        }, with: URL2 , encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]
                    print("Json Object: \(jsonResponse)")
                    let statusCode = Int("\(jsonResponse["code"] as? Int ?? 0)") ?? 0
                    guard statusCode == 1 else {
                        print(jsonResponse)
                        //completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                        return
                    }
                    if let imageObject = jsonResponse["payload"] as? [String: Any] {
                        if let imageUrl = imageObject["uploaded"] as? String {
                            print(imageUrl)
                            print(jsonResponse)
                            //completion(imageUrl,jsonResponse["message"] as? String ?? "", statusCode)
                        } else {
                            print(jsonResponse)
                            //completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                        }
                    } else {
                        print(jsonResponse)
                        //completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                    }
                }
            case .failure( _):
                print("Fail")
                //completion(nil,"API Failed", 0)
            }
        })
        
    }
}
