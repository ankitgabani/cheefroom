//
//  StudentListVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 26/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class StudentListVC: BaseViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var teacherName: UILabel!
    @IBOutlet weak var progamImage: UIImageView!
    @IBOutlet weak var navigationDate: UILabel!
    
    var navTitle: String?
    var navteacherName: String?
    var navprogamImage: String?
    var navDate: String?
    
    
    var studentListArr: StudentListViewModl?
    var ID:String?

    override func viewDidLoad() {
        super.viewDidLoad()

        //Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navTitle = navTitle, let navteacherName = navteacherName, let navDate = navDate {
            self.titleName.text = navTitle
            self.teacherName.text = navteacherName
            if let cDate = self.stringToDate(dateStr: navDate) {
                self.navigationDate.text = self.convDate(date: cDate, format: "dd-MM-yyyy")
            } else {
                self.navigationDate.text = ""
            }
        } else {
            self.titleName.text = ""
            self.teacherName.text = ""
            self.navigationDate.text = ""
        }
        if let navprogamImage = navprogamImage {
            RestApi.shared.getImage(url: BASE_URL+navprogamImage ) { (image) in
                self.progamImage.image = image
            }
        }
        
        tblView.delegate = self
        tblView.dataSource = self
        
        self.hideTabBar()
        self.hideHideNavigationBar()
        
        self.getStudentListApi()
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension StudentListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentListArr?.payload?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: StudentListCell.self), for: indexPath) as! StudentListCell
        cell.setData(data: (self.studentListArr?.payload?[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
}


extension StudentListVC {

    private func getStudentListApi() {
        self.showLoader()
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        //let header = ["Authorization": "Bearer tutor_I0gFNl0Uu7kiEiM"]
        RestApi.shared.requiredPOST(url: ProjectUrl.showStudentList, param: ["id": ID!], header: header).responseStudentListModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.studentListArr = StudentListViewModl(studentListModel: response)
                if self.studentListArr?.code == 1 {
                    
                    self.tblView.reloadData()
                } else {
                    //self.showAlertMessage(titleStr: nil, messageStr: self.studentListArr?.message!)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }

}
