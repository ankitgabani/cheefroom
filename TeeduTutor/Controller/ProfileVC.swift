//
//  ProfileVC.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 30/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout

class ProfileVC: BaseViewController {
    
    @IBOutlet weak var mainViewHieght: NSLayoutConstraint!
    @IBOutlet weak var colletionViewHieght: NSLayoutConstraint!
    @IBOutlet weak var collectyionView: UICollectionView!
    @IBOutlet weak var TXTINSTAGRAM: UITextField!
    
    @IBOutlet weak var TXTTICKTOK: UITextField!
    @IBOutlet weak var TXTFB: UITextField!
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var fullNameField: UITextField!
    @IBOutlet weak var mobileField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var countryCodeField: UITextField!
    
    @IBOutlet weak var nameImg: UIImageView!
    @IBOutlet weak var mobileImg: UIImageView!
    @IBOutlet weak var cityImg: UIImageView!
    @IBOutlet weak var emailImg: UIImageView!
    
    var selectedSkillId: String?
   
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgVerified: UIButton!
    
    @IBOutlet weak var viewFrameSklos: UIView!
    
    @IBOutlet weak var lblSkllis: UILabel!
    let cityPicker = UIPickerView()
    var cityArr: CityModl?
    //var selectedCityID: Int?
    
    var getSubjectIDs = [Int]()
    var getCourseIDs = [Int]()
    var selectedSubjectID: String? = ""
    var selectedCourseID: String? = ""
    
    var selectedWhereToTeach: String?
    var selectedTeach: Int?
    
    var whereToTeachArr: WhereTeachViewModl?
    var coursesArr: ClassesViewModl?
    var subjectArr:SubjectsViewModl?
    
    var profileArr: ProfileViewModl?
    
    var editProfie: EditProfileViewModl?
    
    var arrCuisionList = NSArray()
    
    var arrFinalCuisionList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectyionView.delegate = self
        collectyionView.dataSource = self
        
        let layout = TagFlowLayout()
        layout.estimatedItemSize = CGSize(width: 140, height: 45)
        collectyionView.collectionViewLayout = layout
        
        self.getWhereToTeachApi()
        self.getProfileApi()
        self.setProfileImage()
        setUPViewTag()
        
        viewFrameSklos.layer.cornerRadius = 8
        viewFrameSklos.clipsToBounds = true
        
        viewFrameSklos.layer.borderColor = UIColor(red: 44/255, green: 49/255, blue: 55/255, alpha: 1).cgColor
        viewFrameSklos.layer.borderWidth = 1
        
        setFieldDelegate(txtField: [fullNameField, mobileField, emailField, cityField])
    }
    
    func setUPViewTag() {
        
    }
    
    func setFieldDelegate(txtField: [UITextField]) {
        for i in txtField {
            i.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getProgramListApi()
        
        if cityField.text == "No location selected" || cityField.text?.isEmpty == true {
            //cityImg.image = #imageLiteral(resourceName: "uncheck")
        } else {
            cityImg.image = #imageLiteral(resourceName: "check")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.showNavigationBar(title: nil, setTransparent: false)
    }
    
    private func setProfileImage() {
        
        self.hideHideNavigationBar()
        self.hideTabBar()
        
        if let imageUrl = self.getData(key: savedKeys.profile_image) {
            RestApi.shared.getImage(url: imageUrl ) { (image) in
                if let image = image {
                    self.imgView.image = image
                } else {
                    self.imgView.image = #imageLiteral(resourceName: "avatar")
                }
            }
        }
        if let imageVerified = self.getData(key: savedKeys.status) {
            if imageVerified == "VERIFIED" {
                imgVerified.setImage(#imageLiteral(resourceName: "profileVerified"), for: .normal)
            } else {
                imgVerified.setImage(#imageLiteral(resourceName: "crossShield"), for: .normal)
            }
        }
    }
    
    
    func openCityPicker() {
        cityPicker.backgroundColor = .white
        cityField.inputView = cityPicker
        //cityPicker.delegate = self
        //self.getCityList()
    }
    
    private func setProfile(data: ProfileViewModl) {
        
        self.selectedSkillId = data.payload?.skill_id
        
        self.lblSkllis.text = data.payload?.skill_title
        
        self.fullNameField.text = data.payload?.fullName
        self.mobileField.text = data.payload?.mobile
        self.emailField.text = data.payload?.email
        if cityField.text?.isEmpty == true {
            cityField.text = data.payload?.city?.cityName
        }
        self.descTxtView.text = data.payload?.bio
        self.selectedWhereToTeach = data.payload?.placeWhereToTeach
        
        if data.payload?.subjectSelectedID?.count != nil {
            if data.payload?.subjectSelectedID?.count != 0 {
                self.getSubjectIDs.removeAll()
                for i in data.payload!.subjectSelectedID! {
                    
                    let strID = i.subjectID!
                    self.getSubjectIDs.append(Int(strID)!)
                }
                
                let first = self.customReplace(str: "\(self.getSubjectIDs)", by: "[", what: "")
                let second = self.customReplace(str: first, by: "]", what: "")
                self.selectedSubjectID = "\(second)"
            }
        }
        
        if data.payload?.courseSelectedID?.count != nil {
            if data.payload?.courseSelectedID?.count != 0 {
                self.getCourseIDs.removeAll()
                for i in data.payload!.courseSelectedID! {
                    
                    let strID = i.classID!
                    self.getCourseIDs.append(Int(strID)!)
                }
                
                let first = self.customReplace(str: "\(self.getCourseIDs)", by: "[", what: "")
                let second = self.customReplace(str: first, by: "]", what: "")
                self.selectedCourseID = "\(second)"
            }
        }
    }
    
    
    @IBAction func citySelectionAct(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: CityVC.self)) as! CityVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        self.hideHideNavigationBar()
    }
    
    
    @IBAction func courseEditClicked(_ sender: UIButton) {
        
        if fullNameField.text?.isEmpty == false && mobileField.text?.isEmpty == false && emailField.text?.isEmpty == false && cityField.text?.isEmpty == false && self.descTxtView.text.isEmpty == false && selectedTeach != nil {
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: CoursesVC.self)) as! CoursesVC
            vc.fromEditProfile = true
            let param = ["fullname":fullNameField.text!,
                         "email":emailField.text!,
                         "city":cityField.text!,
                         "place_id": selectedTeach!,
                         "bio": self.descTxtView.text!
                ] as [String : Any]
            vc.profileArr = param
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func updatePhoneNumber(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
        vc.updateMobileNumber = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func profileBtnClicked(_ sender: Any) {
        openActionSheet()
    }
    
    @IBAction func subjectEditClicked(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: CoursesVC.self)) as! CoursesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmBtnClicked(_ sender: UIButton) {
        
        if fullNameField.text?.isEmpty == false && mobileField.text?.isEmpty == false && emailField.text?.isEmpty == false && cityField.text?.isEmpty == false && countryCodeField.text?.isEmpty == false && self.descTxtView.text.isEmpty == false {
            let param = ["fullname":fullNameField.text!,
                         "email":emailField.text!,
                         "city":cityField.text!,
                         "bio": self.descTxtView.text!,
                         "skill_id": selectedSkillId!,
                         "facebook": self.TXTFB.text!,
                         "instagram": self.TXTINSTAGRAM.text!,
                         "tiktok": self.TXTTICKTOK!,
                         "latitude": UserDefaults.standard.value(forKey: savedKeys.lat)!,
                         "longitude": UserDefaults.standard.value(forKey: savedKeys.long)!
                ] as [String: Any]
            self.editProfileApi(param: param)
        }
    }
    
    @IBAction func btnSlecteSkills(_ sender: Any) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SkillSetVC.self)) as! SkillSetVC
        vc.navigationController?.navigationBar.isHidden = true
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFinalCuisionList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCollectionViewCell",
                                                            for: indexPath) as? TagCollectionViewCell else {
                                                                return TagCollectionViewCell()
        }
        
        cell.tagLabel.text = arrFinalCuisionList[indexPath.row]
        cell.tagLabel.preferredMaxLayoutWidth = collectionView.frame.width - 35
        
        if (indexPath.row != arrFinalCuisionList.count - 1)
        {
            
        } else {
            
        }
        
        self.colletionViewHieght.constant = self.collectyionView.contentSize.height
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (indexPath.row == arrFinalCuisionList.count - 1)
        {
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: CuisineVC.self)) as! CuisineVC
            vc.isFromEditProfile = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

extension ProfileVC: selectedCityWithLatLong {
    func getCity(txt: String?, lat: Double?, long: Double?) {
        self.cityField.text = txt
        self.saveData(value: lat, key: savedKeys.lat)
        self.saveData(value: long, key: savedKeys.long)
    }
}

extension ProfileVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == fullNameField {
            if textField.text!.count <= 1 {
                nameImg.image = #imageLiteral(resourceName: "uncheck")
            } else {
                nameImg.image = #imageLiteral(resourceName: "check")
            }
            
        } else if textField == mobileField {
            if textField.text!.count <= 8 {
                mobileImg.image = #imageLiteral(resourceName: "uncheck")
            } else {
                mobileImg.image = #imageLiteral(resourceName: "check")
            }
            
        } else if textField == emailField {
            if isValidEmail(textField.text!) == true {
                emailImg.image = #imageLiteral(resourceName: "check")
            } else {
                emailImg.image = #imageLiteral(resourceName: "uncheck")
            }
        }
        return true
    }
}


extension ProfileVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.editedImage] as? UIImage {
            // imageView.contentMode = .scaleAspectFit
            imgView.image = pickedImage
            
            let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            let fileName = url!.lastPathComponent
            
            let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
            self.showLoader()
            RestApi.shared.uploadImage(url: ProjectUrl.ProfileImage, fileName, header: header, imgData: imgView.image!.jpegData(compressionQuality: 0.2)!) { (response, msg, status) in
                self.hideLoader()
                if status != 1 {
                    self.showAlertMessage(titleStr: nil, messageStr: msg)
                } else {
                    //BASE_URL+(self.verifyOtpArr?.payload?.profileImage ?? "")
                    self.saveData(value: BASE_URL+(response ?? ""), key: savedKeys.profile_image)
                    
                    if let imageUrl = self.getData(key: savedKeys.profile_image) {
                    let url = URL(string: imageUrl)
                    
                    DispatchQueue.main.async { [weak self] in
                        self!.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
                          }
                        
//                        RestApi.shared.getImage(url: imageUrl ) { (image) in
//                            if let image = image {
//                                self.imgView.image = image
//                            } else {
//                                self.imgView.image = #imageLiteral(resourceName: "avatar")
//                            }
//                        }
                    }
                }
            }
        }
        picker.dismiss(animated:true,completion:nil)
        //let imageData = tutorImg.image?.jpegData(compressionQuality: 0.30)
        //guard let data = imageData else { return }
        //self.uploadImage(by: data)
    }
    
    private func openGallery()
    {
        let Imgpicker = UIImagePickerController()
        Imgpicker.delegate = self
        Imgpicker.sourceType = .photoLibrary
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    private func openCamera(){
        let Imgpicker = UIImagePickerController()
        Imgpicker.delegate = self
        Imgpicker.sourceType = .camera
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    private func openActionSheet(){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

//HIT API
extension ProfileVC {
    
    private func getProfileApi() {
        self.showLoader()
        //let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredGET(url: ProjectUrl.tutorShowProfile, param: nil, header: header).responseProfileModl { (response) in
            self.hideLoader()
            print(response)
            if let response = response.result.value {
                self.profileArr = ProfileViewModl(viewModl: response)
                if self.profileArr?.code == 1 {
                    
                    self.setProfile(data: self.profileArr!)
                    self.getWhereToTeachApi()
                    
                } else {
                    self.setProfile(data: self.profileArr!)
                    self.showAlertMessage(titleStr: nil, messageStr: self.profileArr?.message!)
                }
            }  else {
                print(response)
                self.showAlertMessage(titleStr: nil, messageStr: nil)
            }
        }
    }
    
    private func getWhereToTeachApi() {
        self.showLoader()
        RestApi.shared.requiredGET(url: ProjectUrl.getplacewheretoteach, param: nil, header: nil).responseWhereTeachModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.whereToTeachArr = WhereTeachViewModl(registerWTM: response)
                if self.whereToTeachArr?.code == 1 {
                    print(response)
                    
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.whereToTeachArr?.message ?? nil)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    func getProgramListApi() {
        
        let param = ["": ""]
        //  self.showLoader()
        print(param)
        APIClient.sharedInstance.MakeAPICallWihAuthHeaderTokenQuery(self.getData(key: savedKeys.api_token)!, url: "tutorShowProfile", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    //  self.hideLoader()
                    
                    if let responseUser = response {
                        
                        print(responseUser)
                        
                        self.arrFinalCuisionList.removeAll()
                        let dicResposne = responseUser.value(forKey: "payload") as? NSDictionary
                        
                        let arrCheck = (dicResposne?.value(forKey: "cuisines") as? NSArray)!
                        
                        for title in arrCheck {
                            let name = title as? NSDictionary
                            let str = name?.value(forKey: "cuisine_title") as? String
                            self.arrFinalCuisionList.append(str ?? "")
                        }
                        self.arrFinalCuisionList.append("Edit")
                        self.collectyionView.reloadData()
                    } else {
                        
                    }
                    
                } else {
                    
                    self.hideLoader()
                }
                
            } else {
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.hideLoader()
            }
        })
    }
    
    private func editProfileApi(param: [String: Any]) {
        self.showLoader()
        print(param)
        let header = ["Authorization": "Bearer \(self.getData(key: savedKeys.api_token)!)"]
        RestApi.shared.requiredPOST(url: ProjectUrl.updateTutorProfile, param: param, header: header).responseEditProfileModl { (response) in
            self.hideLoader()
            if let response = response.result.value {
                self.editProfie = EditProfileViewModl(viewModl: response)
                if self.editProfie?.code == 1 {
                    print(response)
                    
                    self.saveData(value: self.editProfie?.payload?.fullName, key: savedKeys.username)
                    
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    self.showAlertMessage(titleStr: nil, messageStr: self.editProfie?.message ?? nil)
                }
            }  else {
                self.showAlertMessage(titleStr: nil, messageStr: nil)
                print(response)
            }
        }
    }
    
    
    //    private func getCityList() {
    //
    //        self.showLoader()
    //        RestApi.shared.requiredGET(url: ProjectUrl.getCitiesData, param: nil, header: nil).responseCityModl { (response) in
    //            guard let response = response.result.value else { return }
    //            self.hideLoader()
    //            if response.code == 1 {
    //                self.cityArr = CityViewModl(registerCity: response)
    //
    //                if self.cityField.text?.isEmpty == true {
    //                    self.cityField.text = self.cityArr?.payload?.first?.name
    //                    //self.selectedCityID = self.cityArr?.payload?.first?.id
    //                }
    //            } else {
    //                print("city not found")
    //            }
    //        }
    //    }
}

extension ProfileVC: selectedSkillSet {
    func getSkillSet(txt: String?, id: String?) {
        self.lblSkllis.text = txt
        selectedSkillId = id
    }
}
