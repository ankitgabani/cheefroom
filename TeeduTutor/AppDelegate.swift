//
//  AppDelegate.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import FirebaseCore
import FirebaseMessaging
import FirebaseCrashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    var locationManager = CLLocationManager()
    
    var sceneDelegate = SceneDelegate()
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        
        Thread.sleep(forTimeInterval: 1.0)
        
        FirebaseApp.configure()
        
        self.getSetLatLong()
        if UserDefaults.standard.value(forKey: savedKeys.userID) != nil  {
            if #available(iOS 13.0, *) {
                sceneDelegate.setStoryboard(name: "Main", controllerIdentifier: "TabBarVC")
            } else {
                let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                let navController = UINavigationController.init(rootViewController: rootVC)
                navController.isNavigationBarHidden = true
                self.window!.rootViewController = navController
            }
        }
        
        
        if #available(iOS 10.0, *) {
         // For iOS 10 display notification (sent via APNS)
         UNUserNotificationCenter.current().delegate = self

         let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
         UNUserNotificationCenter.current().requestAuthorization(
           options: authOptions,
           completionHandler: {_, _ in })
       } else {
         let settings: UIUserNotificationSettings =
         UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
         application.registerUserNotificationSettings(settings)
       }

       application.registerForRemoteNotifications()
        
       Messaging.messaging().delegate = self
        
        
//
//
//
//       Messaging.messaging().isAutoInitEnabled = true
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print(deviceToken)
        //print(InstanceID.instanceID().token() ?? "")
        
//        InstanceID.instanceID().setAPNSToken(deviceToken, type: .sandbox)
//        InstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
        
        //UserDefaults.standard.setValue(InstanceID.instanceID().token(), forKey: savedKeys.device_token)
       

    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        if UserDefaults.standard.value(forKey: savedKeys.userID) != nil  {
            if let getResponse = userInfo["aps"] as? [String: Any] {
                if let checkFromTitle = getResponse["alert"] as? [String: Any] {
                    
                    if checkFromTitle["title"] as? String  == "Booking" {
                        let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                        let navController = UINavigationController.init(rootViewController: rootVC)
                        rootVC.selectedIndex = 2
                        navController.isNavigationBarHidden = true
                        self.window!.rootViewController = navController
                    } else if checkFromTitle["title"] as? String == "Review" {
                        let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                        let navController = UINavigationController.init(rootViewController: rootVC)
                        rootVC.selectedIndex = 4
                        rootVC.sendToReview = true
                        navController.isNavigationBarHidden = true
                        self.window!.rootViewController = navController
                    }
                    
                }
            }
        }
        
        
        print("Recived: \(userInfo)")
        print()
    }
    
    func getNotificationSettings() {
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                print("Notification settings: \(settings)")
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {

        }
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}


extension AppDelegate: CLLocationManagerDelegate {
    private func getSetLatLong() {
        
        var clLocation: CLLocation!
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            clLocation = locationManager.location
            
            if clLocation != nil {
                UserDefaults.standard.set(clLocation.coordinate.latitude, forKey: savedKeys.lat)
                UserDefaults.standard.set(clLocation.coordinate.longitude, forKey: savedKeys.long)
            }
        } else {
            UserDefaults.standard.set("", forKey: savedKeys.lat)
            UserDefaults.standard.set("", forKey: savedKeys.long)
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        //print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::: \(error)")
        locationManager.stopUpdatingLocation()
        let alert = UIAlertController(title: "Settings", message: "Allow location from settings", preferredStyle: .alert)
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "", style: .default, handler: { action in
            switch action.style{
            case .default: UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            case .cancel: print("cancel")
            case .destructive: print("destructive")
            @unknown default:
                print(error)
            }
        }))
    }
}


extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: savedKeys.device_token)
      let dataDict:[String: String] = ["token": fcmToken]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
}


extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
