//
//  RestApi.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 02/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class RestApi: NSObject {
    
    static let shared = RestApi()
    
    func requiredPOST(url: String, param: [String: Any]?, header: [String: String]?) -> DataRequest {
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: header)
    }
    
    func requiredGET(url: String, param: [String: Any]?, header: [String: String]?) -> DataRequest {
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding.default, headers: header)
    }
    
    
    func getImage(url: String, completion: @escaping (UIImage?)->Void) {
        Alamofire.request(url).responseImage { (response) in
            if case .success(let image) = response.result {
                completion(image)
            } else {
                completion(nil)
            }
        }
    }
    
    func uploadImage(url: String,_ filename: String, header: HTTPHeaders?,imgData: Data, completion: @escaping(String?,String?, Int) -> ()) {
        let urlStr = url
        let headerData = header

        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            multipartFormData.append(imgData, withName: "profile_image", fileName: filename, mimeType: "image/jpeg")
           // multipartFormData.append( withName: "type")
        }, to: urlStr,headers : headerData,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]
                    print("Json Object: \(jsonResponse)")
                    let statusCode = Int("\(jsonResponse["code"] as? Int ?? 0)") ?? 0
                    guard statusCode == 1 else {
                        completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                        return
                    }
                    if let imageObject = jsonResponse["payload"] as? [String: Any] {
                        if let imageUrl = imageObject["uploaded"] as? String {
                            print(imageUrl)
                            completion(imageUrl,jsonResponse["message"] as? String ?? "", statusCode)
                        } else {
                            completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                        }
                    } else {
                        completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                    }
                }
            case .failure( _):
                completion(nil,"API Failed", 0)
            }
        })
    }
    
    func uploadKycImage(url: String, header: HTTPHeaders?,imgData1: Data, imgFileName1: String, mimType1: String,imgData2: Data, imgFileName2: String, mimType2: String,imgData3: Data, imgFileName3: String, mimType3: String,imgData4: Data, imgFileName4: String, mimType4: String, completion: @escaping(String?,String?, Int) -> ()) {
        let urlStr = url
        let headerData = header

        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            //multipartFormData.append(fileData as Data, withName: "upfile", fileName: filename, mimeType: "text/plain")
            //multipartFormData.append(imgData1, withName: "aadhar_card", fileName: imgFileName1, mimeType: "image/jpeg")
            multipartFormData.append(imgData1, withName: "aadhar_card", fileName: imgFileName1, mimeType: mimType1)
            multipartFormData.append(imgData2, withName: "pan_card", fileName: imgFileName2, mimeType: mimType2)
            multipartFormData.append(imgData3, withName: "degree_certificate", fileName: imgFileName3, mimeType: mimType3)
            multipartFormData.append(imgData4, withName: "other_certificate", fileName: imgFileName4, mimeType: mimType4)
           // multipartFormData.append( withName: "type")
        }, to: urlStr,headers : headerData,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]
                    print("Json Object: \(jsonResponse)")
                    let statusCode = Int("\(jsonResponse["code"] as? Int ?? 0)") ?? 0
                    guard statusCode == 1 else {
                        completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                        return
                    }
                    if let imageObject = jsonResponse["payload"] as? [String: Any] {
                        if let imageUrl = imageObject["uploaded"] as? String {
                            print(imageUrl)
                            completion(imageUrl,jsonResponse["message"] as? String ?? "", statusCode)
                        } else {
                            completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                        }
                    } else {
                        completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                    }
                }
            case .failure( _):
                completion(nil,"API Failed", 0)
            }
        })
    }
    
    func uploadImageWithParam(url: String, param: [String: Any], header: HTTPHeaders?,imgData: Data?, completion: @escaping(String?,String?, Int) -> ()) {

        
        let urlStr = url
        let headerData = header

        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            if let imageDate = imgData {
                multipartFormData.append(imageDate, withName: "image", fileName: "photo.jpg", mimeType: "image/jpeg")
            }
            
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, to: urlStr,headers : headerData,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]
                    print("Json Object: \(jsonResponse)")
                    let statusCode = Int("\(jsonResponse["code"] as? Int ?? 0)") ?? 0
                    guard statusCode == 1 else {
                        completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                        return
                    }
                    if let imageObject = jsonResponse["payload"] as? [String: Any] {
                        if let imageUrl = imageObject["uploaded"] as? String {
                            print(imageUrl)
                            completion(imageUrl,jsonResponse["message"] as? String ?? "", statusCode)
                        } else {
                            completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                        }
                    } else {
                        completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                    }
                }
            case .failure( _):
                completion(nil,"API Failed", 0)
            }
        })
    }
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }

            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }

            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }

    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }

    //MARK:- Registration
    @discardableResult
    func responseRegisterModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<RegisterModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Add Bank Details
    @discardableResult
    func responseAddBankDetailsModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<AddBankModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- City Data
    @discardableResult
    func responseCityModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<CityModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    //MARK:- City Data
    @discardableResult
    func responseCuisineModel(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<CuisineModel>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- City Data
       @discardableResult
       func responseSkillsetModel(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<SkillSetModel>) -> Void) -> Self {
           return responseDecodable(queue: queue, completionHandler: completionHandler)
       }
    
    //MARK:- City Lat Long Data
    @discardableResult
    func responseCityLatLongModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<CityLatLongModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- verifyOTP
    @discardableResult
    func responseVerifyOtp(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<VerifyOtpMod>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Where to teach
    @discardableResult
    func responseWhereTeachModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<WhereTeachModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- CLASSES
    @discardableResult
    func responseClassesModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ClassesModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- SUBJECTS
    @discardableResult
    func responseSubjectsModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<SubjectsModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- biodata
    @discardableResult
    func responseBioDataModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<BioDataModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseSaveCuisineModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<SaveCuisineModel>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Login / SendOTP
    @discardableResult
    func responseLoginModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<LoginModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- UpdateNumberOtp
    @discardableResult
    func responseUpdateNumberOtpModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<UpdateNumberOtpModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Home Week Model
    @discardableResult
    func responseHomeWeekModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<HomeWeekModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Home Month Model
    @discardableResult
    func responseHomeMonthModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<HomeMonthModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Home Today Model
    @discardableResult
    func responseHomeTodayModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<HomeTodayModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Home / BOOKING CANCEL
    @discardableResult
    func responseCancelModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<CancelModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Student List Model
    @discardableResult
    func responseStudentListModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<StudentListModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
   
    //MARK:- RescheduleModel
    @discardableResult
    func responseRescheduleModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<RescheduleModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Program List Model
    @discardableResult
    func responseProgramListModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ProgramListModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Add Session Paramters
       @discardableResult
       func responseAddSessionParamters(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<AddSessionParaModl>) -> Void) -> Self {
           return responseDecodable(queue: queue, completionHandler: completionHandler)
       }
    
    //MARK:- Add Program Model
    @discardableResult
    func responseAddProgramModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<AddProgramModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Add Program Subject Class Model
    @discardableResult
    func responseAddProgramClassSubjectModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<AddProgramClassSubjectModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Edit Program Model
    @discardableResult
    func responseEditProgramModlModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<EditProgramModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Booking List Model
    @discardableResult
    func responseBookingslistModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<BookingslistModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Session List Model
    @discardableResult
    func responseSessionlistModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<SessionlistModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- User Profile Model
    @discardableResult
    func responseProfileModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ProfileModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- User Edit Profile Model
    @discardableResult
    func responseEditProfileModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<EditProfileModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Kyc Upload Model
    @discardableResult
    func responseKycModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<KycModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Notification Model
    @discardableResult
    func responseNotificationModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<NotificationModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- About Model
    @discardableResult
    func responseAboutModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<AboutModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    // MARK: - FAQ Model
    @discardableResult
    func responseFAQModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<FAQModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    //MARK:- Review Model
    @discardableResult
    func responseReviewModl(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ReviewModl>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}


func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}
