//
//  APIClient.swift
//  TeeduTutor
//
//  Created by Ankit on 25/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class APIClient: NSObject {
    
    class var sharedInstance: APIClient {
        
        struct Static {
            static let instance: APIClient = APIClient()
        }
        return Static.instance
    }
    
    var responseData: NSMutableData!
    
    func MakeAPICallWihAuthHeaderTokenQuery(_ token: String, url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        
        print(BASE_URL_New + url)
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        Alamofire.request(BASE_URL_New + url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: headers).responseJSON { response in
            
            switch(response.result) {
                
            case .success:
                if response.result.value != nil{
                    if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                        completionHandler(responseDict, nil, response.response?.statusCode)
                    }
                }
                
            case .failure:
                print(response.result.error!)
                print("Http Status Code: \(String(describing: response.response?.statusCode))")
                completionHandler(nil, response.result.error, response.response?.statusCode )
            }
        }
        
    }
    
    func MakeAPICallWihAuthHeaderPost(_ token: String, _ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        print(headers)
        
        Alamofire.request(BASE_URL_New + url, method: .post, encoding: URLEncoding(destination: .httpBody), headers: headers).responseJSON { response in
            
            switch(response.result) {
                
            case .success:
                if response.result.value != nil{
                    if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                        completionHandler(responseDict, nil, response.response?.statusCode)
                    }
                }
                
            case .failure:
                print(response.result.error!)
                print("Http Status Code: \(String(describing: response.response?.statusCode))")
                completionHandler(nil, response.result.error, response.response?.statusCode )
            }
        }
        
    }
    
    func postImageToServer(_ token: String, _ url: String, _ fileName: String, image: UIImage!, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void){
        
        let finalUrl = BASE_URL_New + url
        print("Requesting \(finalUrl)")
        print("Parameters: \(parameters)")
        
        print(image)
        let imageData = image.jpegData(compressionQuality: 0.1)
        
       // let tokenID = UserDefaults.standard.value(forKey: "TokenID") as? String
        
        // let headers: HTTPHeaders = ["TokenID": tokenID!]
                
        //        let imageData  = UIImagePNGRepresentation(image)
        
        if (imageData != nil)
        {
            
           let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                multipartFormData.append(imageData!, withName: "image", fileName: fileName, mimeType: "image/jpeg")
                
            }, usingThreshold: UInt64.init(), to: finalUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        if let err = response.error{
                            completionHandler(nil, err, response.response?.statusCode)
                            
                            return
                        }
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                        
                        
                    }
                    upload.uploadProgress(closure: { (progrress) in
                        print(progrress.fractionCompleted)
                    })
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    completionHandler(nil, error, -1 )
                }
            }
        }
        
    }
}

