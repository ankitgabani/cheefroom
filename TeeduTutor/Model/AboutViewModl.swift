//
//  AboutViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 10/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

struct AboutViewModl: Codable {
    let payload: AboutModl.Payload?
    let status: Bool?
    let message: String?
    let code: Int?
    
    init(viewModl: AboutModl) {
        self.payload = viewModl.payload
        self.message = viewModl.message
        self.status = viewModl.status
        self.code = viewModl.code
    }

}


struct FAQViewModl: Codable {
    let payload: [FAQModl.Payload]?
    let status: Bool?
    let message: String?
    let code: Int?
    
    init(viewModl: FAQModl) {
        self.payload = viewModl.payload
        self.message = viewModl.message
        self.status = viewModl.status
        self.code = viewModl.code
    }

}
