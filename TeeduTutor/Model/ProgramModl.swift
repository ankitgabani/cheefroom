//
//  ProgramModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 08/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - ProgramListModl
struct ProgramListModl: Codable {
    let payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id: String?
        let programName: String?
        let classID: String?
        let payloadClass: String?
        let subjectID: String?
        let subject, workingDays, startFrom, endTo: String?
        let image: String?
        let fee: String?
        let startingFrom: String?
        let schedule_start_date: String?
        let session_name: String?
        let tutor_id: String?
        let cuisine: String?
        let languages: String?
        let suitable_for: String?
        let hashtag: String?
        let session_frequency: String?
        let schedule_end_date: String?
        let descriptions: String?
        let instruction_video: String?
        let ingredients_list: String?
        let utensils_required: String?
        let session_type: String?
        let status: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case tutor_id
            case cuisine
            case languages
            case suitable_for
            case session_frequency
            case programName = "program_name"
            case classID = "class_id"
            case payloadClass = "class"
            case subjectID = "subject_id"
            case subject
            case workingDays = "working_days"
            case startFrom = "start_from"
            case endTo = "end_to"
            case image
            case fee
            case startingFrom = "starting_from"
            case schedule_start_date = "schedule_start_date"
            case session_name
            case descriptions = "description"
            case status
            case session_type
            case utensils_required
            case ingredients_list
            case instruction_video
            case schedule_end_date
            case hashtag
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}

// MARK: - AddProgramModl
struct AddProgramModl: Codable {
    let payload: Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let tutorProgramName, subjectID, startingFrom, payloadDescription: String?
        let workingDays, startFrom, endTo, fee: String?
        let classID: String?
        let tutorID: Int?

        enum CodingKeys: String, CodingKey {
            case tutorProgramName = "tutor_program_name"
            case subjectID = "subject_id"
            case startingFrom = "starting_from"
            case payloadDescription = "description"
            case workingDays = "working_days"
            case startFrom = "start_from"
            case endTo = "end_to"
            case fee
            case classID = "class_id"
            case tutorID = "tutor_id"
        }
    }

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}


// MARK: - AddProgramClassSubjectModl
struct AddProgramClassSubjectModl: Codable {
    let payload: Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let courseSelectedID: [CourseSelectedID]?
        let subjectSelectedID: [SubjectSelectedID]?

        enum CodingKeys: String, CodingKey {
            case courseSelectedID = "course_selected_id"
            case subjectSelectedID = "subject_selected_id"
        }
    }
    
    // MARK: - CourseSelectedID
    struct CourseSelectedID: Codable {
        let classID: String?
        let className: String?

        enum CodingKeys: String, CodingKey {
            case classID = "class_id"
            case className = "class_name"
        }
    }
    
    // MARK: - SubjectSelectedID
    struct SubjectSelectedID: Codable {
        let subjectID: String?
        let subjectName: String?

        enum CodingKeys: String, CodingKey {
            case subjectID = "subject_id"
            case subjectName = "subject_name"
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}


// MARK: - EditProgramModl
struct EditProgramModl: Codable {
    let payload: Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id: Int?
        let programName, startingFrom, payloadDescription, workingDays: String?
        let startFrom, endTo: String?
        let fee, classID, subjectID: Int?

        enum CodingKeys: String, CodingKey {
            case id
            case programName = "program_name"
            case startingFrom = "starting_from"
            case payloadDescription = "description"
            case workingDays = "working_days"
            case startFrom = "start_from"
            case endTo = "end_to"
            case fee
            case classID = "class_id"
            case subjectID = "subject_id"
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
