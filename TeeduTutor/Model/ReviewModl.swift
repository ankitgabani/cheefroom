//
//  ReviewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 12/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - ReviewModl
struct ReviewModl: Codable {
    let payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let fullName, profileImage: String?
        let rating: String?
        let title, payloadDescription, createdAt: String?

        enum CodingKeys: String, CodingKey {
            case fullName = "full_name"
            case profileImage = "profile_image"
            case rating, title
            case payloadDescription = "description"
            case createdAt = "created_at"
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
