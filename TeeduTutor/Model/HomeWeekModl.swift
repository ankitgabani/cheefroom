//
//  HomeWeekModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 06/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - HomeWeekModl
struct HomeWeekModl: Codable {
    var payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?

    // MARK: - Payload
    struct Payload: Codable {
        let id, tutorID: String?
        let programName, workingDays, image, payloadClass: String?
        let startingFrom, startFrom, date: String?
        let noOfStudents: Int?

        enum CodingKeys: String, CodingKey {
            case id
            case tutorID = "tutor_id"
            case programName = "program_name"
            case workingDays = "working_days"
            case image
            case payloadClass = "class"
            case startingFrom = "starting_from"
            case startFrom = "start_from"
            case date
            case noOfStudents = "No_of_students"
        }
    }

    
    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}

// MARK: - HomeMonthModl
struct HomeMonthModl: Codable {
    var payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id, tutorID: String?
        let programName, workingDays, tuitionType, image, payloadClass: String?
        let startingFrom, endTo, startFrom, date: String?
        let noOfStudents: Int?

        enum CodingKeys: String, CodingKey {
            case id
            case tutorID = "tutor_id"
            case programName = "program_name"
            case workingDays = "working_days"
            case image
            case tuitionType = "tuition_type"
            case endTo = "end_to"
            case payloadClass = "class"
            case startingFrom = "schedule_start_date"
            case startFrom = "start_from"
            case date
            case noOfStudents = "No_of_students"
        }
    }

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}

// MARK: - HomeTodayModl
struct HomeTodayModl: Codable {
    var payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id, tutorID: String?
        let programName, workingDays, image, payloadClass: String?
        let startingFrom, startFrom: String?
        let bookings: String?
        let NoOfStudents: Int?
        let date: String?

        enum CodingKeys: String, CodingKey {
            case id
            case NoOfStudents = "No_of_students"
            case tutorID = "tutor_id"
            case programName = "program_name"
            case workingDays = "working_days"
            case image
            case payloadClass = "class"
            case startingFrom = "starting_from"
            case startFrom = "start_from"
            case bookings, date
        }
    }

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}


// MARK: - StudentListModl
struct StudentListModl: Codable {
    let payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id: Int?
        let programName: String?
        let classID: Int?
        let payloadClass: String?
        let subjectID: Int?
        let subject, workingDays, startFrom, fullName: String?
        let mobile, profileImage: String?

        enum CodingKeys: String, CodingKey {
            case id
            case programName = "program_name"
            case classID = "class_id"
            case payloadClass = "class"
            case subjectID = "subject_id"
            case subject
            case workingDays = "working_days"
            case startFrom = "start_from"
            case fullName = "full_name"
            case mobile
            case profileImage = "profile_image"
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}


// MARK: - RescheduleModl
struct RescheduleModl: Codable {
    let payload, message, devMessage, type: String?
    let code: Int?

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
