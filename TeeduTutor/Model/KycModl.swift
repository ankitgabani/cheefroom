//
//  KycModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 10/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - KycModl
struct KycModl: Codable {
    let payload: Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let uploadedDoc1, uploadedDoc2, uploadedDoc3, uploadedDoc4: String?
        let response: Int?

        enum CodingKeys: String, CodingKey {
            case uploadedDoc1 = "uploaded_doc1"
            case uploadedDoc2 = "uploaded_doc2"
            case uploadedDoc3 = "uploaded_doc3"
            case uploadedDoc4 = "uploaded_doc4"
            case response
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
