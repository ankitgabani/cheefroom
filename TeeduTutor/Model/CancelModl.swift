//
//  CancelModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 06/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - CancelModl
struct CancelModl: Codable {
    let payload, message, devMessage, type: String?
    let code: Int?

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
