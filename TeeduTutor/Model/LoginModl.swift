//
//  LoginModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 03/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - LoginModl
struct LoginModl: Codable {
    let payload: Payload?
    let status: Bool?
    let message: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let mobile, countryCode: String?
        let otp: Int?
    }
}


// MARK: - UpdateNumberOtpModl
struct UpdateNumberOtpModl: Codable {
    let payload: Payload?
    let status: Bool?
    let message: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let mobile, countryCode, userType, apiToken: String?
        let otp: Int?

        enum CodingKeys: String, CodingKey {
            case mobile, countryCode
            case userType = "user_type"
            case apiToken = "api_token"
            case otp
        }
    }

}

