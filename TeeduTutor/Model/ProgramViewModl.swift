//
//  ProgramViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 08/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - ProgramListViewModl
struct ProgramListViewModl: Codable {
    let payload: [ProgramListModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(listModl: ProgramListModl) {
        self.payload = listModl.payload
        self.devMessage = listModl.devMessage
        self.message = listModl.message
        self.type = listModl.type
        self.code = listModl.code
    }
}

// MARK: - AddProgramModl
struct AddProgramViewModl: Codable {
    let payload: AddSessionParaModl.Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    init(addModl: AddSessionParaModl) {
        self.payload = addModl.payload
        self.devMessage = addModl.devMessage
        self.message = addModl.message
        self.type = addModl.type
        self.code = addModl.code
    }
}

struct AddProgramClassSubjectViewModl: Codable {
    let payload: AddProgramClassSubjectModl.Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    init(viewModl: AddProgramClassSubjectModl) {
        self.payload = viewModl.payload
        self.devMessage = viewModl.devMessage
        self.message = viewModl.message
        self.type = viewModl.type
        self.code = viewModl.code
    }
}


// MARK: - EditProgramModlModl
struct EditProgramViewModl: Codable {
    let payload: EditProgramModl.Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    init(viewModl: EditProgramModl) {
        self.payload = viewModl.payload
        self.devMessage = viewModl.devMessage
        self.message = viewModl.message
        self.type = viewModl.type
        self.code = viewModl.code
    }
}
