//
//  ProfileModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 10/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - ProfileModl
struct ProfileModl: Codable {
    let payload: Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let fullName, email, mobile: String?
        let city: City?
        let tuitionType: String?
        let courseSelectedID: [CourseSelectedID]?
        let subjectSelectedID: [SubjectSelectedID]?
        let profileImage, bio, aadharCard, panCard: String?
        let degreeCertificate, additionalCertificate, kycStatus, placeWhereToTeach: String?
        let skill_id: String?
        let skill_title: String?
        let cuisines: [CuisinesList]?
        enum CodingKeys: String, CodingKey {
            case fullName = "full_name"
            case email, mobile, city
            case tuitionType = "tuition_type"
            case courseSelectedID = "course_selected_id"
            case subjectSelectedID = "subject_selected_id"
            case profileImage = "profile_image"
            case bio
            case aadharCard = "aadhar_card"
            case panCard = "pan_card"
            case degreeCertificate = "degree_certificate"
            case additionalCertificate = "additional_certificate"
            case kycStatus = "kyc_status"
            case placeWhereToTeach = "place_where_to_teach"
            case skill_id
            case skill_title
            case cuisines

        }
    }
    
    // MARK: - City
    struct City: Codable {
        let cityID: String?
        let cityName: String?

        enum CodingKeys: String, CodingKey {
            case cityID = "city_id"
            case cityName = "city_name"
        }
    }
    
    // MARK: - CourseSelectedID
    struct CuisinesList: Codable {
        let user_cuisine_id: String?
        let user_id: String?
        let cuisine_id: String?
        let cuisine_title: String?
        
        enum CodingKeys: String, CodingKey {
            case user_cuisine_id = "user_cuisine_id"
            case user_id = "user_id"
            case cuisine_id = "cuisine_id"
            case cuisine_title = "cuisine_title"
            
        }
    }
    
    // MARK: - CourseSelectedID
       struct CourseSelectedID: Codable {
           let classID: String?
           let className: String?

           enum CodingKeys: String, CodingKey {
               case classID = "class_id"
               case className = "class_name"
           }
       }
    
    // MARK: - SubjectSelectedID
    struct SubjectSelectedID: Codable {
        let subjectID: String?
        let subjectName: String?

        enum CodingKeys: String, CodingKey {
            case subjectID = "subject_id"
            case subjectName = "subject_name"
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}

struct AddSessionParaModl: Codable {
    let payload: Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        
        let cuisines: [CuisinesSelectedID]?
        let languages: [NSArray]?
        let suitable_for: [NSArray]?
        let skillset: [skillsetSelectedID]?
        
        enum CodingKeys: String, CodingKey {
            
            case skillset = "skillset"
            case languages = "languages"
            case suitable_for = "suitable_for"
            case cuisines = "cuisines"
        }
    }
    
    struct NSArray: Codable {
        
        enum CodingKeys: CodingKey {
            
        }
    }
    
    struct CuisinesSelectedID: Codable {
        let cuisine_id: String?
        let cuisine_title: String?
        let created_at: String?

        enum CodingKeys: String, CodingKey {
            case cuisine_id = "cuisine_id"
            case cuisine_title = "cuisine_title"
            case created_at = "created_at"
        }
    }
    
    struct skillsetSelectedID: Codable {
        let cuisine_id: String?
        let cuisine_title: String?
        let created_at: String?

        enum CodingKeys: String, CodingKey {
            case cuisine_id = "cuisine_id"
            case cuisine_title = "cuisine_title"
            case created_at = "created_at"
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}

// MARK: - EditProfileModl
struct EditProfileModl: Codable {
    let payload: Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id: Int?
        let fullName, email, mobile: String?
        let city: String?
        let bio, subjectSelectedID, courseSelectedID: String?
        let placeWhereToTeach: String?

        enum CodingKeys: String, CodingKey {
            case id
            case fullName = "full_name"
            case email, mobile, city, bio
            case subjectSelectedID = "subject_selected_id"
            case courseSelectedID = "course_selected_id"
            case placeWhereToTeach = "place_where_to_teach"
        }
    }

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
