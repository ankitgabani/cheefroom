//
//  BookingModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 09/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - BookingslistModl
struct BookingslistModl: Codable {
    let payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let fullName, profileImage, programName, workingDays: String?
        let payloadClass, startFrom, endTo: String?
        let bookingID, noOfSession: Int?
        let startSession, endSession: String?

        enum CodingKeys: String, CodingKey {
            case fullName = "full_name"
            case profileImage = "profile_image"
            case programName = "program_name"
            case workingDays = "working_days"
            case payloadClass = "class"
            case startFrom = "start_from"
            case endTo = "end_to"
            case bookingID = "booking_id"
            case noOfSession = "no_of_session"
            case startSession = "start_session"
            case endSession = "end_session"
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
