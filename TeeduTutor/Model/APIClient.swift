//
//  APIClient.swift
//  TeeduTutor
//
//  Created by Ankit on 25/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class APIClient: NSObject {
    
    class var sharedInstance: APIClient {
        
        struct Static {
            static let instance: APIClient = APIClient()
        }
        return Static.instance
    }
    
    var responseData: NSMutableData!
    
    func MakeAPICallWihAuthHeaderTokenQuery(_ token: String, url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        
        print(BASE_URL_New + url)
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        Alamofire.request(BASE_URL_New + url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: headers).responseJSON { response in
            
            switch(response.result) {
                
            case .success:
                if response.result.value != nil{
                    if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                        completionHandler(responseDict, nil, response.response?.statusCode)
                    }
                }
                
            case .failure:
                print(response.result.error!)
                print("Http Status Code: \(String(describing: response.response?.statusCode))")
                completionHandler(nil, response.result.error, response.response?.statusCode )
            }
        }
        
    }
    
    func MakeAPICallWihAuthHeaderPost(_ token: String, _ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
            
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            print(headers)
            
            Alamofire.request(BASE_URL_New + url, method: .post, encoding: URLEncoding(destination: .httpBody), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        
    }
}

