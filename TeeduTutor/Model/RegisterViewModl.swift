//
//  RegisterViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 02/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit


// MARK: - RegisterViewModl
struct RegisterViewModl: Codable {
    let payload: RegisterModl.Payload?
    let data: RegisterModl.DataClass?
    let message, devMessage, type: String?
    let code: Int?
    
    init(registerModel: RegisterModl?) {
        self.payload = registerModel?.payload
        self.data = registerModel?.data
        self.message = registerModel?.message
        self.devMessage = registerModel?.devMessage
        self.type = registerModel?.type
        self.code = registerModel?.code
    }
}

// MARK: - CityViewModl
//struct CityViewModl: Codable {
//    let payload: [CityModl]?
//    let message, devMessage, type: String?
//    let code: Int?
//
//    init(registerCity: CityModl?) {
//        self.payload = registerCity?.payload
//        self.message = registerCity?.message
//        self.devMessage = registerCity?.devMessage
//        self.type = registerCity?.type
//        self.code = registerCity?.code
//    }
//}

// MARK: - VerifyOtpViewMod
struct VerifyOtpViewMod: Codable {
    let payload: VerifyOtpMod.Payload?
    let status: Bool?
    let message: String?
    let code: Int?
    
    init(registerOTP: VerifyOtpMod?) {
        self.payload = registerOTP?.payload
        self.status = registerOTP?.status
        self.message = registerOTP?.message
        self.code = registerOTP?.code
    }
}

// MARK: - WhereTeachViewModl
struct WhereTeachViewModl: Codable {
    let payload: [WhereTeachModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(registerWTM: WhereTeachModl?) {
        self.payload = registerWTM?.payload
        self.message = registerWTM?.message
        self.devMessage = registerWTM?.devMessage
        self.type = registerWTM?.type
        self.code = registerWTM?.code
    }
    
}

// MARK: - ClassesViewModl
struct ClassesViewModl: Codable {
    let payload: [ClassesModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(registerClasses: ClassesModl?) {
        self.payload = registerClasses?.payload
        self.message = registerClasses?.message
        self.devMessage = registerClasses?.devMessage
        self.type = registerClasses?.type
        self.code = registerClasses?.code
    }
}

// MARK: - SubjectsViewModl
struct SubjectsViewModl: Codable {
    let payload: [[SubjectsModl.Payload]]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(registerSubject: SubjectsModl?) {
        self.payload = registerSubject?.payload
        self.message = registerSubject?.message
        self.devMessage = registerSubject?.devMessage
        self.type = registerSubject?.type
        self.code = registerSubject?.code
    }
}

// MARK: - BioDataViewModl
struct BioDataViewModl: Codable {
    let payload: BioDataModl.Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    init(registerBio: BioDataModl?) {
        self.payload = registerBio?.payload
        self.message = registerBio?.message
        self.devMessage = registerBio?.devMessage
        self.type = registerBio?.type
        self.code = registerBio?.code
    }
    
}
