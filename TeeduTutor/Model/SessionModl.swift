//
//  SessionModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 09/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - SessionlistModl
struct SessionlistModl: Codable {
    let payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id, tutorID: Int?
        let programName, workingDays, payloadClass, subject: String?
        let image, startingFrom, startFrom, endTo: String?
        let tuitionType: String?
        let bookings: Int?

        enum CodingKeys: String, CodingKey {
            case id
            case tutorID = "tutor_id"
            case programName = "program_name"
            case workingDays = "working_days"
            case payloadClass = "class"
            case subject, image
            case startingFrom = "starting_from"
            case startFrom = "start_from"
            case endTo = "end_to"
            case tuitionType = "tuition_type"
            case bookings
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
