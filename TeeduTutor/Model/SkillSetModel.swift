// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let skillSetModel = try? newJSONDecoder().decode(SkillSetModel.self, from: jsonData)

import Foundation

// MARK: - SkillSetModel
struct SkillSetModel: Codable {
    let payload: [Payload]
    let message, devMessage, type: String
    let code: Int

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}

// MARK: - Payload
struct Payload: Codable {
    let skillID, skillTitle, createdAt: String

    enum CodingKeys: String, CodingKey {
        case skillID = "skill_id"
        case skillTitle = "skill_title"
        case createdAt = "created_at"
    }
}
