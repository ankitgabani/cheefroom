//
//  SessionViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 09/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - SessionlistModl
struct SessionlistViewModl: Codable {
    let payload: [SessionlistModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(viewModl: SessionlistModl) {
        self.payload = viewModl.payload
        self.devMessage = viewModl.devMessage
        self.message = viewModl.message
        self.type = viewModl.type
        self.code = viewModl.code
    }
}
