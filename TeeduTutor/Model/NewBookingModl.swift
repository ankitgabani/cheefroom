//
//  NewBookingModl.swift
//  TeeduTutor
//
//  Created by Ankit on 27/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import Foundation

class TTBokking: NSObject {
    
    var Code: Int?
    var message: String?
    var dev_message: String?
    var type: String?
    var full_name: String?
    var profile_image: String?
   var id: String?
    var start_from: String?
    var working_days: String?
    var end_to: String?
    var booking_id: String?
    var no_of_session: String?
    var start_session: String?
    var end_session: String?
    
    override init() {
        super.init()
    }
    
    init(TTBokkingListDictionary: NSDictionary?) {
        super.init()
        
        Code = TTBokkingListDictionary?.value(forKey: "Code") as? Int
        
        message = TTBokkingListDictionary?.value(forKey: "message") as? String
        dev_message = TTBokkingListDictionary?.value(forKey: "dev_message") as? String
        type = TTBokkingListDictionary?.value(forKey: "type") as? String
        full_name = TTBokkingListDictionary?.value(forKey: "full_name") as? String
        profile_image = TTBokkingListDictionary?.value(forKey: "profile_image") as? String
        id = TTBokkingListDictionary?.value(forKey: "id") as? String
        working_days = TTBokkingListDictionary?.value(forKey: "working_days") as? String
        start_from = TTBokkingListDictionary?.value(forKey: "start_from") as? String
        end_to = TTBokkingListDictionary?.value(forKey: "end_to") as? String
        booking_id = TTBokkingListDictionary?.value(forKey: "booking_id") as? String
        no_of_session = TTBokkingListDictionary?.value(forKey: "no_of_session") as? String
        start_session = TTBokkingListDictionary?.value(forKey: "start_session") as? String
        end_session = TTBokkingListDictionary?.value(forKey: "end_session") as? String
    }
}
