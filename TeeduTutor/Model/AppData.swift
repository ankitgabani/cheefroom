//
//  AppData.swift
//  Blipd
//
//  Created by Harry on 3/1/18.
//  Copyright © 2018 Harry. All rights reserved.
//

import UIKit
import CoreLocation

class AppData: NSObject {

    class var sharedInstance: AppData {
        
        struct Static {
            static let instance: AppData = AppData()
        }
        return Static.instance
    }
    
    var sessionName: String?
    var cuisine: String?
    var fee: String?
    var image: UIImage?
    var fileName: String?
    var languages: String?
    var suitable_for: String?
    var session_type: String?
    var hashtag: String?
    
    var session_frequency: String?
    var working_days: String?
    var schedule_start_date: String?
    var schedule_end_date: String?
    var start_from: String?
    var end_to: String?
    
    var descriptions: String?
    var instruction_video: String?
    
    var ingredients_list: String?
    var utensils_required: String?
}
