//
//  LoginViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 06/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit


// MARK: - LoginViewModl
struct LoginViewModl: Codable {
    let payload: LoginModl.Payload?
    let status: Bool?
    let message: String?
    let code: Int?
    
    init(login: LoginModl) {
        self.payload = login.payload
        self.status = login.status
        self.message = login.message
        self.code = login.code
        
    }
}


// MARK: - LoginViewModl
struct UpdateNumberOtpViewModl: Codable {
    let payload: UpdateNumberOtpModl.Payload?
    let status: Bool?
    let message: String?
    let code: Int?
    
    init(login: UpdateNumberOtpModl) {
        self.payload = login.payload
        self.status = login.status
        self.message = login.message
        self.code = login.code
        
    }
}
