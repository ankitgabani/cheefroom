//
//  ProfileViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 10/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - ProfileViewModl
struct ProfileViewModl: Codable {
    let payload: ProfileModl.Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    init(viewModl: ProfileModl) {
        self.payload = viewModl.payload
        self.devMessage = viewModl.devMessage
        self.message = viewModl.message
        self.type = viewModl.type
        self.code = viewModl.code
    }
}



// MARK: - EditProfileViewModl
struct EditProfileViewModl: Codable {
    let payload: EditProfileModl.Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    init(viewModl: EditProfileModl) {
        self.payload = viewModl.payload
        self.devMessage = viewModl.devMessage
        self.message = viewModl.message
        self.type = viewModl.type
        self.code = viewModl.code
    }
}
