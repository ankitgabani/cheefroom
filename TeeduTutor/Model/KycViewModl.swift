//
//  KycViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 10/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - KycViewModl
struct KycViewModl: Codable {
    let payload: KycModl.Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    init(viewModl: KycModl) {
        self.payload = viewModl.payload
        self.devMessage = viewModl.devMessage
        self.message = viewModl.message
        self.type = viewModl.type
        self.code = viewModl.code
    }
}
