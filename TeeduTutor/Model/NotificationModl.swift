//
//  NotificationModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 10/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - NotificationModl
struct NotificationModl: Codable {
    let payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id, studentID, tutorID, programID: String?
        let deviceToken, title, message, deviceType: String?
        let messageType, notificationType, createdAt: String?
        let modifiedAt, status, profileImage, payloadClass: String?
        let subject: String?
        let isRead: String?
        let actionID: String?
        let notificationCategory: String?

        enum CodingKeys: String, CodingKey {
            case id
            case studentID = "student_id"
            case tutorID = "tutor_id"
            case programID = "program_id"
            case deviceToken = "device_token"
            case title, message
            case deviceType = "device_type"
            case messageType = "message_type"
            case actionID = "action_id"
            case notificationType = "notification_type"
            case createdAt = "created_at"
            case modifiedAt = "modified_at"
            case status
            case isRead = "is_read"
            case profileImage = "profile_image"
            case notificationCategory = "notification_category"
            case payloadClass = "class"
            case subject
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
