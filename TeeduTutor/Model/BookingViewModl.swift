//
//  BookingViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 09/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - BookingslistViewModl
struct BookingslistViewModl: Codable {
    let payload: [BookingslistModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    
    init(viewModl: BookingslistModl) {
        self.payload = viewModl.payload
        self.devMessage = viewModl.devMessage
        self.message = viewModl.message
        self.type = viewModl.type
        self.code = viewModl.code
    }
}
