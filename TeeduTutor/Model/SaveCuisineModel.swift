// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let saveCuisineModel = try? newJSONDecoder().decode(SaveCuisineModel.self, from: jsonData)

import Foundation

// MARK: - SaveCuisineModel
struct SaveCuisineModel: Codable {
    let payload: [SavePayload]
    let message, devMessage, type: String
    let code: Int

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}

// MARK: - Payload
struct SavePayload: Codable {
    let userID: Int
    let cuisineID: String

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case cuisineID = "cuisine_id"
    }
}
