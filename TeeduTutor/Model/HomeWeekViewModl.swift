//
//  HomeWeekViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 06/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

struct HomeWeekViewModl: Codable {
    let payload: [HomeWeekModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(homeModel: HomeWeekModl?) {
        self.payload = homeModel?.payload
        self.message = homeModel?.message
        self.devMessage = homeModel?.devMessage
        self.type = homeModel?.type
        self.code = homeModel?.code
    }
}

struct HomeMonthViewModl: Codable {
    let payload: [HomeMonthModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(homeModel: HomeMonthModl?) {
        self.payload = homeModel?.payload
        self.message = homeModel?.message
        self.devMessage = homeModel?.devMessage
        self.type = homeModel?.type
        self.code = homeModel?.code
    }
}


// MARK: - HomeTodayViewModl
struct HomeTodayViewModl: Codable {
    let payload: [HomeTodayModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(homeModel: HomeTodayModl?) {
        self.payload = homeModel?.payload
        self.message = homeModel?.message
        self.devMessage = homeModel?.devMessage
        self.type = homeModel?.type
        self.code = homeModel?.code
    }
}

// MARK: - StudentListViewModl
struct StudentListViewModl: Codable {
    let payload: [StudentListModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(studentListModel: StudentListModl?) {
        self.payload = studentListModel?.payload
        self.message = studentListModel?.message
        self.devMessage = studentListModel?.devMessage
        self.type = studentListModel?.type
        self.code = studentListModel?.code
    }
}

// MARK: - RescheduleViewModl
struct RescheduleViewModl: Codable {
    let payload, message, devMessage, type: String?
    let code: Int?

    init(rescheduleModel: RescheduleModl?) {
        self.payload = rescheduleModel?.payload
        self.message = rescheduleModel?.message
        self.devMessage = rescheduleModel?.devMessage
        self.type = rescheduleModel?.type
        self.code = rescheduleModel?.code
    }
}
