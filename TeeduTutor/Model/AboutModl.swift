//
//  AboutModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 10/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - AboutModl
struct AboutModl: Codable {
    let payload: Payload?
    let status: Bool?
    let message: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let content: String?
    }

}

// MARK: - FAQModl
struct FAQModl: Codable {
    let payload: [Payload]?
    let status: Bool?
    let message: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let faqID: String?
        let question, answer: String?

        enum CodingKeys: String, CodingKey {
            case faqID = "faq_id"
            case question, answer
        }
    }
    
}
