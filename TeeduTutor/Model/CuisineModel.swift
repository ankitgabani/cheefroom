// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let cuisineModel = try? newJSONDecoder().decode(CuisineModel.self, from: jsonData)

import Foundation

// MARK: - CuisineModel
struct CuisineModel: Codable {
    let payload: CuisinePayload
    let message, devMessage, type: String
    let code: Int

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}

// MARK: - Payload
struct CuisinePayload: Codable {
    let alaphabets: [Alaphabet]
}

// MARK: - Alaphabet
struct Alaphabet: Codable {
    let alphabet: String
    var cuisines: [Cuisine]
}

// MARK: - Cuisine
struct Cuisine: Codable {
    var cuisineID, cuisineTitle, selected: String

    enum CodingKeys: String, CodingKey {
        case cuisineID = "cuisine_id"
        case cuisineTitle = "cuisine_title"
        case selected
    }
}
