//
//  NotificationViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 10/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - NotificationModl
struct NotificationViewModl: Codable {
    let payload: [NotificationModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(viewModl: NotificationModl) {
        self.payload = viewModl.payload
        self.devMessage = viewModl.devMessage
        self.message = viewModl.message
        self.type = viewModl.type
        self.code = viewModl.code
    }
}
