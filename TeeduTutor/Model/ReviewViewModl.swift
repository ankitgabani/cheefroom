//
//  ReviewViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 12/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - ReviewModl
struct ReviewViewModl: Codable {
    let payload: [ReviewModl.Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    init(viewModl: ReviewModl) {
        self.payload = viewModl.payload
        self.message = viewModl.message
        self.devMessage = viewModl.devMessage
        self.type = viewModl.type
        self.code = viewModl.code
    }
}  
