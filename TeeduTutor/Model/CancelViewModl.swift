//
//  CancelViewModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 06/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

// MARK: - CancelViewModl
struct CancelViewModl: Codable {
    let payload, message, devMessage, type: String?
    let code: Int?

    init(cancel: CancelModl) {
        self.payload = cancel.payload
        self.devMessage = cancel.devMessage
        self.message = cancel.message
        self.type = cancel.type
        self.code = cancel.code
        
    }
}
