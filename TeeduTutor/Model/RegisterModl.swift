//
//  RegisterModl.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 02/04/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

struct AddBankModl: Codable {
    let payload: Payload?
    let message, devMessage, type: String?
    let code: Int?

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
    
    // MARK: - Payload
    struct Payload: Codable {
        let name: String?
        let bankname: String?
        let accountno: String?
        let ifsc: String?


        enum CodingKeys: String, CodingKey {
            case name
            case bankname = "bank_name"
            case accountno = "account_no"
            case ifsc
        }
    }
}


// MARK: - RegisterModl
struct RegisterModl: Codable {
    let payload: Payload?
    let data: DataClass?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let mobileNumber: String?
        let otp: Int?
    }
    
    enum CodingKeys: String, CodingKey {
        case payload, data, message
        case devMessage = "dev_message"
        case type, code
    }
    
    // MARK: - Payload
    struct Payload: Codable {
        let id: String?
        let fullName, email: String?
        let countryCode: String?
        let mobile, password, rememberToken: String?
        let city: String?
        let gender, userType, placeWhereToTeach, courseSelectedID: String?
        let subjectSelectedID, profileImage, bio, apiToken: String?
        let deviceType, deviceToken, status, signUpType: String?
        let createdAt, updatedAt: String?

        enum CodingKeys: String, CodingKey {
            case id
            case fullName = "full_name"
            case email, countryCode, mobile, password
            case rememberToken = "remember_token"
            case city, gender
            case userType = "user_type"
            case placeWhereToTeach = "place_where_to_teach"
            case courseSelectedID = "course_selected_id"
            case subjectSelectedID = "subject_selected_id"
            case profileImage = "profile_image"
            case bio
            case apiToken = "api_token"
            case deviceType = "device_type"
            case deviceToken = "device_token"
            case status
            case signUpType = "signUp_type"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }
}


// MARK: - CityModl
struct CityModl: Codable {
    let predictions: [Prediction]?
    let status: String?
    
    // MARK: - Prediction
    struct Prediction: Codable {
        let predictionDescription, id: String?
        let matchedSubstrings: [MatchedSubstring]?
        let placeID, reference: String?
        let structuredFormatting: StructuredFormatting?
        let terms: [Term]?
        let types: [String]?

        enum CodingKeys: String, CodingKey {
            case predictionDescription = "description"
            case id
            case matchedSubstrings = "matched_substrings"
            case placeID = "place_id"
            case reference
            case structuredFormatting = "structured_formatting"
            case terms, types
        }
    }
    
    // MARK: - MatchedSubstring
    struct MatchedSubstring: Codable {
        let length, offset: Int?
    }
    
    // MARK: - StructuredFormatting
    struct StructuredFormatting: Codable {
        let mainText: String?
        let mainTextMatchedSubstrings: [MatchedSubstring]?
        let secondaryText: String?

        enum CodingKeys: String, CodingKey {
            case mainText = "main_text"
            case mainTextMatchedSubstrings = "main_text_matched_substrings"
            case secondaryText = "secondary_text"
        }
    }
    
    // MARK: - Term
    struct Term: Codable {
        let offset: Int?
        let value: String?
    }
}


// MARK: - CityLatLongModl
struct CityLatLongModl: Codable {
    //let htmlAttributions: Any?
    let result: Result?
    let status: String?

    enum CodingKeys: String, CodingKey {
        //case htmlAttributions = "html_attributions"
        case result, status
    }
    
    // MARK: - Result
    struct Result: Codable {
        let addressComponents: [AddressComponent]?
        let adrAddress, formattedAddress: String?
        let geometry: Geometry?
        let icon: String?
        let id, name: String?
        let photos: [Photo]?
        let placeID, reference, scope: String?
        let types: [String]?
        let url: String?
        let utcOffset: Int?
        let website: String?

        enum CodingKeys: String, CodingKey {
            case addressComponents = "address_components"
            case adrAddress = "adr_address"
            case formattedAddress = "formatted_address"
            case geometry, icon, id, name, photos
            case placeID = "place_id"
            case reference, scope, types, url
            case utcOffset = "utc_offset"
            case website
        }
    }
    
    // MARK: - AddressComponent
    struct AddressComponent: Codable {
        let longName, shortName: String?
        let types: [String]?

        enum CodingKeys: String, CodingKey {
            case longName = "long_name"
            case shortName = "short_name"
            case types
        }
    }
    struct Geometry: Codable {
        let location: Location?
        let viewport: Viewport?
    }
    
    struct Location: Codable {
        let lat, lng: Double?
    }
    
    // MARK: - Viewport
    struct Viewport: Codable {
        let northeast, southwest: Location?
    }

    // MARK: - Photo
    struct Photo: Codable {
        let height: Int?
        let htmlAttributions: [String]?
        let photoReference: String?
        let width: Int?

        enum CodingKeys: String, CodingKey {
            case height
            case htmlAttributions = "html_attributions"
            case photoReference = "photo_reference"
            case width
        }
    }
}



//struct CityModl: Codable {
//    let payload: [Payload]?
//    let message, devMessage, type: String?
//    let code: Int?
//
//    // MARK: - Payload
//    struct Payload: Codable {
//        let id: Int?
//        let name: String?
//        let phonecode: Int?
//    }
//
//    enum CodingKeys: String, CodingKey {
//        case payload, message
//        case devMessage = "dev_message"
//        case type, code
//    }
//}


// MARK: - VerifyOtp
struct VerifyOtpMod: Codable {
    let payload: Payload?
    let status: Bool?
    let message: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id: String?
        let fullName, email: String?
        let countryCode: String?
        let mobile, password, rememberToken: String?
        let city: String?
        let latitude, longitude: String?
        let gender, userType: String?
        let placeWhereToTeach: String?
        let courseSelectedID, subjectSelectedID, profileImage, bio: String?
        let apiToken, deviceType, deviceToken, status: String?
        let signUpType, createdAt, updatedAt: String?
        let bank_details: bank_details?
        
        enum CodingKeys: String, CodingKey {
            case id
            case fullName = "full_name"
            case latitude, longitude
            case email, countryCode, mobile, password
            case rememberToken = "remember_token"
            case city, gender
            case userType = "user_type"
            case placeWhereToTeach = "place_where_to_teach"
            case courseSelectedID = "course_selected_id"
            case subjectSelectedID = "subject_selected_id"
            case profileImage = "profile_image"
            case bio
            case apiToken = "api_token"
            case deviceType = "device_type"
            case deviceToken = "device_token"
            case status
            case signUpType = "signUp_type"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case bank_details = "bank_details"
        }
    }
    
    struct bank_details: Codable {
        let name: String?
        var id: String
        var tutor_id: String?
        var bank_name: String?
        var account_no: String?
        var ifsc: String?
        var created_at: String?
        var updated_at: String?

        enum CodingKeyss: String, CodingKey {
            case name = "name"
            case id = "id"
            case tutor_id = "tutor_id"
            case bank_name = "bank_name"
            case account_no = "account_no"
            case ifsc = "ifsc"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
    }
   
}


// MARK: - WhereTeachModl
struct WhereTeachModl: Codable {
    let payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id: String?
        let tuitionType: String?

        enum CodingKeys: String, CodingKey {
            case id
            case tuitionType = "tuition_type"
        }
    }

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}


// MARK: - ClassesModl
struct ClassesModl: Codable {
    let payload: [Payload]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let id: Int?
        let name: String?
    }

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}


// MARK: - SubjectsModl
struct SubjectsModl: Codable {
    let payload: [[Payload]]?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let subjectID: Int?
        let subjectName: String?
        let classID: Int?

        enum CodingKeys: String, CodingKey {
            case subjectID = "subject_id"
            case subjectName = "subject_name"
            case classID = "class_id"
        }
    }


    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}

// MARK: - BioDataModl
struct BioDataModl: Codable {
    let payload: Payload?
    let message, devMessage, type: String?
    let code: Int?
    
    // MARK: - Payload
    struct Payload: Codable {
        let placeWhereToTeach, courseSelectedID, subjectSelectedID, bio: String?

        enum CodingKeys: String, CodingKey {
            case placeWhereToTeach = "place_where_to_teach"
            case courseSelectedID = "course_selected_id"
            case subjectSelectedID = "subject_selected_id"
            case bio
        }
    }

    enum CodingKeys: String, CodingKey {
        case payload, message
        case devMessage = "dev_message"
        case type, code
    }
}
